/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.o2m.wockhard;

public final class R {
    public static final class anim {
        public static final int slide_in_left=0x7f040000;
        public static final int slide_in_right=0x7f040001;
        public static final int slide_out_left=0x7f040002;
        public static final int slide_out_right=0x7f040003;
    }
    public static final class array {
        public static final int Assessment_fun_array=0x7f090003;
        public static final int Assessment_fun_array_L2_L3=0x7f090004;
        public static final int Milestones_array=0x7f090002;
        public static final int add_sl_category_array=0x7f090006;
        public static final int inbox_submenu_array=0x7f090000;
        public static final int menu_array=0x7f090001;
        public static final int menu_title_array=0x7f090007;
        public static final int sl_category_array=0x7f090005;
        public static final int wocktube_array=0x7f090008;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int avtar_bg=0x7f05000d;
        public static final int casestudy_bg=0x7f050006;
        public static final int classroom_bg=0x7f05000e;
        public static final int default_frame_color=0x7f050000;
        public static final int default_frame_off_color=0x7f050001;
        public static final int default_transparent_color=0x7f050002;
        public static final int epoll_bg=0x7f050005;
        public static final int faq_bg=0x7f05000a;
        public static final int inbox_bg=0x7f050008;
        public static final int my_assment_bg=0x7f050004;
        public static final int my_learning_bg=0x7f050003;
        public static final int performance_bg=0x7f05000c;
        public static final int shared_learning_bg=0x7f050007;
        public static final int training_path_bg=0x7f05000b;
        public static final int wocktube_bg=0x7f050009;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
        public static final int default_image_size=0x7f060002;
    }
    public static final class drawable {
        public static final int arrow=0x7f020000;
        public static final int ass_100x70=0x7f020001;
        public static final int ass_150x105=0x7f020002;
        public static final int ass_200x140=0x7f020003;
        public static final int back_arrow=0x7f020004;
        public static final int book_home=0x7f020005;
        public static final int bronze=0x7f020006;
        public static final int camera=0x7f020007;
        public static final int casestudy_icon=0x7f020008;
        public static final int dislike=0x7f020009;
        public static final int divider=0x7f02000a;
        public static final int drawer_shadow=0x7f02000b;
        public static final int earth=0x7f02000c;
        public static final int epoll_icon=0x7f02000d;
        public static final int faqs_icon=0x7f02000e;
        public static final int fav=0x7f02000f;
        public static final int flatlogin_logo=0x7f020010;
        public static final int flatlogo=0x7f020011;
        public static final int gold=0x7f020012;
        public static final int hmenu_avatar=0x7f020013;
        public static final int hmenu_case_study=0x7f020014;
        public static final int hmenu_classroom=0x7f020015;
        public static final int hmenu_epoll=0x7f020016;
        public static final int hmenu_faqs=0x7f020017;
        public static final int hmenu_inbox=0x7f020018;
        public static final int hmenu_library=0x7f020019;
        public static final int hmenu_my_assessments=0x7f02001a;
        public static final int hmenu_mylearning=0x7f02001b;
        public static final int hmenu_performance_wall=0x7f02001c;
        public static final int hmenu_setting=0x7f02001d;
        public static final int hmenu_shared_learning=0x7f02001e;
        public static final int hmenu_training_path=0x7f02001f;
        public static final int hmenu_wbook=0x7f020020;
        public static final int hmenu_wocktube=0x7f020021;
        public static final int home=0x7f020022;
        public static final int home_assessment=0x7f020023;
        public static final int home_casestudy=0x7f020024;
        public static final int home_epoll=0x7f020025;
        public static final int home_icon_assessment=0x7f020026;
        public static final int home_icon_faq=0x7f020027;
        public static final int home_icon_messages=0x7f020028;
        public static final int home_icon_milestone=0x7f020029;
        public static final int home_icon_performance=0x7f02002a;
        public static final int home_icon_sl=0x7f02002b;
        public static final int home_icon_wocktube=0x7f02002c;
        public static final int home_icon_wocktube1=0x7f02002d;
        public static final int home_logo=0x7f02002e;
        public static final int home_mylearning=0x7f02002f;
        public static final int home_right_arrow=0x7f020030;
        public static final int home_sharedlearning=0x7f020031;
        public static final int home_text=0x7f020032;
        public static final int home_title_bg=0x7f020033;
        public static final int home_wocktube=0x7f020034;
        public static final int homepagebanner=0x7f020035;
        public static final int ic_action_attach=0x7f020036;
        public static final int ic_action_attachment=0x7f020037;
        public static final int ic_action_call=0x7f020038;
        public static final int ic_action_copy=0x7f020039;
        public static final int ic_action_cut=0x7f02003a;
        public static final int ic_action_delete=0x7f02003b;
        public static final int ic_action_done=0x7f02003c;
        public static final int ic_action_edit=0x7f02003d;
        public static final int ic_action_important=0x7f02003e;
        public static final int ic_action_locate=0x7f02003f;
        public static final int ic_action_mail=0x7f020040;
        public static final int ic_action_mail_add=0x7f020041;
        public static final int ic_action_microphone=0x7f020042;
        public static final int ic_action_overflow=0x7f020043;
        public static final int ic_action_paste=0x7f020044;
        public static final int ic_action_photo=0x7f020045;
        public static final int ic_action_refresh=0x7f020046;
        public static final int ic_action_search=0x7f020047;
        public static final int ic_action_select_all=0x7f020048;
        public static final int ic_action_send=0x7f020049;
        public static final int ic_action_share=0x7f02004a;
        public static final int ic_action_star=0x7f02004b;
        public static final int ic_action_user=0x7f02004c;
        public static final int ic_action_user_add=0x7f02004d;
        public static final int ic_action_video=0x7f02004e;
        public static final int ic_empty=0x7f02004f;
        public static final int ic_error=0x7f020050;
        public static final int ic_launcher=0x7f020051;
        public static final int ic_stub=0x7f020052;
        public static final int inbox_100x70=0x7f020053;
        public static final int jupiter=0x7f020054;
        public static final int layout_bg=0x7f020055;
        public static final int left=0x7f020056;
        public static final int like=0x7f020057;
        public static final int liketoggle=0x7f020058;
        public static final int line=0x7f020059;
        public static final int loading1=0x7f02005a;
        public static final int login_212x47=0x7f02005b;
        public static final int login_bt=0x7f02005c;
        public static final int login_logo=0x7f02005d;
        public static final int login_logo_old=0x7f02005e;
        public static final int loginlogo=0x7f02005f;
        public static final int logintextfield=0x7f020060;
        public static final int mars=0x7f020061;
        public static final int menu_assessment=0x7f020062;
        public static final int menu_case_study=0x7f020063;
        public static final int menu_custom_button=0x7f020064;
        public static final int menu_epoll=0x7f020065;
        public static final int menu_faq=0x7f020066;
        public static final int menu_icon=0x7f020067;
        public static final int menu_icon_old=0x7f020068;
        public static final int menu_icon_pressed=0x7f020069;
        public static final int menu_inbox=0x7f02006a;
        public static final int menu_milestone=0x7f02006b;
        public static final int menu_mylearning=0x7f02006c;
        public static final int menu_performance_wall=0x7f02006d;
        public static final int menu_shared_learning=0x7f02006e;
        public static final int menu_wocktube=0x7f02006f;
        public static final int mercury=0x7f020070;
        public static final int message_archive=0x7f020071;
        public static final int message_delete=0x7f020072;
        public static final int message_fav=0x7f020073;
        public static final int message_inbox=0x7f020074;
        public static final int messages_icon=0x7f020075;
        public static final int milestone_icon=0x7f020076;
        public static final int milestone_next_circle=0x7f020077;
        public static final int msg_archive=0x7f020078;
        public static final int msg_deleted=0x7f020079;
        public static final int msg_important=0x7f02007a;
        public static final int msg_inbox=0x7f02007b;
        public static final int neptune=0x7f02007c;
        public static final int next=0x7f02007d;
        public static final int notification_bg=0x7f02007e;
        public static final int performancewall_icon=0x7f02007f;
        public static final int post_comment=0x7f020080;
        public static final int px_transparent=0x7f020081;
        public static final int read_more=0x7f020082;
        public static final int redline=0x7f020083;
        public static final int right=0x7f020084;
        public static final int saturn=0x7f020085;
        public static final int sharedlearning_icon=0x7f020086;
        public static final int silver=0x7f020087;
        public static final int splash=0x7f020088;
        public static final int uranus=0x7f020089;
        public static final int venus=0x7f02008a;
        public static final int video_thumbnail=0x7f02008b;
        public static final int video_thumbnail1=0x7f02008c;
        public static final int wockhardt_new_logo_home=0x7f02008d;
        public static final int wockhardt_new_logo_home11=0x7f02008e;
        public static final int wockhardt_new_logo_home_old=0x7f02008f;
        public static final int wockhardt_new_logo_login=0x7f020090;
        public static final int wocktube_icon=0x7f020091;
    }
    public static final class id {
        public static final int VideoView=0x7f0b002a;
        public static final int action_settings=0x7f0b00aa;
        public static final int addSlBtn=0x7f0b0085;
        public static final int addSlOptionSpinner=0x7f0b0040;
        public static final int assementCourseTitle=0x7f0b0021;
        public static final int assementMarksTitle=0x7f0b0022;
        public static final int assementStatusTitle=0x7f0b0023;
        public static final int assesmentFunList=0x7f0b0051;
        public static final int assesmentList=0x7f0b0050;
        public static final int assesmentPanelBg=0x7f0b001f;
        public static final int assesmentTitle=0x7f0b0020;
        public static final int assessmentBtn=0x7f0b0007;
        public static final int assessmentGrpTitle=0x7f0b0024;
        public static final int assessmentName=0x7f0b005e;
        public static final int assessmentOptionList=0x7f0b0054;
        public static final int assessmentQuestion=0x7f0b0053;
        public static final int assfunTitle=0x7f0b0025;
        public static final int bottombar=0x7f0b0001;
        public static final int btn_Login=0x7f0b0019;
        public static final int btn_like=0x7f0b0039;
        public static final int btn_sendforapproval=0x7f0b004f;
        public static final int caCertificateName=0x7f0b0075;
        public static final int caExpiry=0x7f0b0077;
        public static final int caList=0x7f0b0079;
        public static final int caStatus=0x7f0b0076;
        public static final int closeBtn=0x7f0b0095;
        public static final int completedAssessmentList=0x7f0b005d;
        public static final int completedTitle=0x7f0b005c;
        public static final int content_frame=0x7f0b0012;
        public static final int correctAnsDetail=0x7f0b0057;
        public static final int correctAnsTitle=0x7f0b0056;
        public static final int csAssignedDate=0x7f0b0062;
        public static final int csAttchment=0x7f0b0061;
        public static final int csBtn=0x7f0b0009;
        public static final int csCommDesc=0x7f0b0069;
        public static final int csCommentBy=0x7f0b0036;
        public static final int csCommentDesc=0x7f0b0035;
        public static final int csCommentLikeCount=0x7f0b0038;
        public static final int csCommentList=0x7f0b006b;
        public static final int csCommentdate=0x7f0b0037;
        public static final int csCreatedName=0x7f0b0065;
        public static final int csDescWebView=0x7f0b0064;
        public static final int csPostComm=0x7f0b006a;
        public static final int csRateIt=0x7f0b0066;
        public static final int csTitle=0x7f0b0060;
        public static final int csViewAllComm=0x7f0b0068;
        public static final int cslkcnt=0x7f0b0067;
        public static final int dialog_ratingbar=0x7f0b009b;
        public static final int divisionSpinner=0x7f0b000c;
        public static final int drawer_layout=0x7f0b0011;
        public static final int ePollList=0x7f0b006d;
        public static final int ePollQuestion=0x7f0b006c;
        public static final int ePollResultList=0x7f0b006f;
        public static final int ePollResultQuestion=0x7f0b006e;
        public static final int editTxt_PWD=0x7f0b0018;
        public static final int editTxt_UsrName=0x7f0b0017;
        public static final int epollBtn=0x7f0b000a;
        public static final int epollResultansPercent=0x7f0b003c;
        public static final int epollResultansTitle=0x7f0b003b;
        public static final int ernextBtn=0x7f0b0070;
        public static final int et_slVideoLink=0x7f0b0048;
        public static final int et_sldesc=0x7f0b0046;
        public static final int et_slsubject=0x7f0b0045;
        public static final int faqAns=0x7f0b003e;
        public static final int faqGrpTitle=0x7f0b003f;
        public static final int faqList=0x7f0b0071;
        public static final int faqQue=0x7f0b003d;
        public static final int faqsBtn=0x7f0b0004;
        public static final int gvMenu=0x7f0b001c;
        public static final int homeBtn=0x7f0b0010;
        public static final int image=0x7f0b0084;
        public static final int image1=0x7f0b004a;
        public static final int image2=0x7f0b004b;
        public static final int image3=0x7f0b004c;
        public static final int image4=0x7f0b004d;
        public static final int image5=0x7f0b004e;
        public static final int imageContailer=0x7f0b0049;
        public static final int imageView=0x7f0b008a;
        public static final int imageView1=0x7f0b000e;
        public static final int imageView_icon=0x7f0b003a;
        public static final int image_frame=0x7f0b008e;
        public static final int imagrParent=0x7f0b0088;
        public static final int imgLogo=0x7f0b001d;
        public static final int inboxBtn=0x7f0b0002;
        public static final int inboxList=0x7f0b0073;
        public static final int ivHeader=0x7f0b001a;
        public static final int iv_optIcon=0x7f0b0026;
        public static final int klBtn=0x7f0b0078;
        public static final int leftBtn=0x7f0b0089;
        public static final int left_drawer=0x7f0b0013;
        public static final int levelSpinner=0x7f0b000d;
        public static final int linear_Layout=0x7f0b0014;
        public static final int loginbg=0x7f0b0016;
        public static final int mdmsArchieve=0x7f0b007c;
        public static final int mdmsImp=0x7f0b007d;
        public static final int mdmsdDelete=0x7f0b007b;
        public static final int mdmsgBody=0x7f0b0081;
        public static final int mdmsgBy=0x7f0b007f;
        public static final int mdmsgDate=0x7f0b0080;
        public static final int mdmsgTitle=0x7f0b007e;
        public static final int menu_icon=0x7f0b000f;
        public static final int messageTitle=0x7f0b0072;
        public static final int milestoneBtn=0x7f0b0005;
        public static final int milestoneList=0x7f0b0074;
        public static final int miscAnsTitle=0x7f0b0093;
        public static final int mlTitle=0x7f0b0092;
        public static final int msgBy=0x7f0b0090;
        public static final int msgDaTE=0x7f0b0091;
        public static final int msgDetailTitle=0x7f0b007a;
        public static final int msgTitle=0x7f0b008f;
        public static final int msgbottomBar=0x7f0b0058;
        public static final int myLearningBtn=0x7f0b000b;
        public static final int my_horizontal_image_scroller=0x7f0b0029;
        public static final int my_horizontal_image_scroller1=0x7f0b0028;
        public static final int my_horizontal_image_scroller2=0x7f0b001e;
        public static final int mybrowser=0x7f0b0082;
        public static final int nextBtn=0x7f0b0055;
        public static final int optionSpinner=0x7f0b0094;
        public static final int pendingAssessmentList=0x7f0b005b;
        public static final int pendingTitle=0x7f0b005a;
        public static final int performaceBtn=0x7f0b0006;
        public static final int performanceList=0x7f0b0083;
        public static final int pwChildHq=0x7f0b0098;
        public static final int pwChildTitle=0x7f0b0097;
        public static final int pwGrpTitle=0x7f0b0099;
        public static final int questionNo=0x7f0b0059;
        public static final int radioGrp=0x7f0b0041;
        public static final int radio_image=0x7f0b0044;
        public static final int radio_text=0x7f0b0042;
        public static final int radio_video=0x7f0b0043;
        public static final int rank_dialog_button=0x7f0b009c;
        public static final int rank_dialog_text1=0x7f0b009a;
        public static final int ratingBar=0x7f0b0063;
        public static final int rightBtn=0x7f0b008b;
        public static final int saveBtn=0x7f0b0096;
        public static final int setting=0x7f0b001b;
        public static final int sharedLearningBtn=0x7f0b0008;
        public static final int slBy=0x7f0b009f;
        public static final int slCategorySpinner=0x7f0b0086;
        public static final int slCommentBy=0x7f0b00a5;
        public static final int slCommentDesc=0x7f0b00a4;
        public static final int slCommentdate=0x7f0b00a6;
        public static final int slComments=0x7f0b00a2;
        public static final int slDesc=0x7f0b009e;
        public static final int slLikes=0x7f0b00a1;
        public static final int slList=0x7f0b0087;
        public static final int slTitle=0x7f0b009d;
        public static final int sl_btn_like=0x7f0b00a0;
        public static final int slcommentList=0x7f0b0034;
        public static final int sldBy=0x7f0b002f;
        public static final int sldCommDesc=0x7f0b0032;
        public static final int sldDesc=0x7f0b002e;
        public static final int sldLikes=0x7f0b0030;
        public static final int sldPlayVideo=0x7f0b002c;
        public static final int sldPostComm=0x7f0b0033;
        public static final int sldTitle=0x7f0b002d;
        public static final int sldVideoLink=0x7f0b002b;
        public static final int sld_btn_like=0x7f0b0031;
        public static final int sldate=0x7f0b00a3;
        public static final int thmbnail=0x7f0b00a8;
        public static final int timeLeft=0x7f0b005f;
        public static final int topLinearLayout=0x7f0b0052;
        public static final int topbar=0x7f0b0000;
        public static final int tv_optName=0x7f0b0027;
        public static final int tv_slVideoLinkTitle=0x7f0b0047;
        public static final int txtView_build=0x7f0b0015;
        public static final int wTSynopsis=0x7f0b00a9;
        public static final int wTTitle=0x7f0b00a7;
        public static final int wocktubeBtn=0x7f0b0003;
        public static final int wocktubeList=0x7f0b008d;
        public static final int wocktubeSpinner=0x7f0b008c;
    }
    public static final class layout {
        public static final int activity_home=0x7f030000;
        public static final int activity_home1=0x7f030001;
        public static final int activity_home2=0x7f030002;
        public static final int activity_home3=0x7f030003;
        public static final int activity_inbox=0x7f030004;
        public static final int activity_login=0x7f030005;
        public static final int activity_menu=0x7f030006;
        public static final int activity_splash=0x7f030007;
        public static final int activity_test=0x7f030008;
        public static final int assesment_list_item=0x7f030009;
        public static final int assessment_child_list_item=0x7f03000a;
        public static final int assessment_grp_list_item=0x7f03000b;
        public static final int assessment_pending_child_list_item=0x7f03000c;
        public static final int assessmentfun_list_item=0x7f03000d;
        public static final int backupofactivity_home1=0x7f03000e;
        public static final int bkup_single_view_gv=0x7f03000f;
        public static final int copy_of_fragment_sl_details=0x7f030010;
        public static final int cs_comment_list_item=0x7f030011;
        public static final int drawer_list=0x7f030012;
        public static final int drawer_list_item=0x7f030013;
        public static final int epoll_ans_item=0x7f030014;
        public static final int epoll_item=0x7f030015;
        public static final int epoll_result_list_item=0x7f030016;
        public static final int faq_child_list_item=0x7f030017;
        public static final int faq_grp_list_item=0x7f030018;
        public static final int fragment_add_shared_learning=0x7f030019;
        public static final int fragment_assesment=0x7f03001a;
        public static final int fragment_assesment_fun=0x7f03001b;
        public static final int fragment_assessment_ans=0x7f03001c;
        public static final int fragment_assessment_detail=0x7f03001d;
        public static final int fragment_assessment_que=0x7f03001e;
        public static final int fragment_casestudy=0x7f03001f;
        public static final int fragment_casestudy_comment_list=0x7f030020;
        public static final int fragment_certificate_assessment_detail=0x7f030021;
        public static final int fragment_epoll=0x7f030022;
        public static final int fragment_epoll_result=0x7f030023;
        public static final int fragment_faq=0x7f030024;
        public static final int fragment_inbox=0x7f030025;
        public static final int fragment_milestone=0x7f030026;
        public static final int fragment_milestore_curriculum=0x7f030027;
        public static final int fragment_misc_assessment_que=0x7f030028;
        public static final int fragment_misc_assessment_que_ans=0x7f030029;
        public static final int fragment_msg_details=0x7f03002a;
        public static final int fragment_offline_web=0x7f03002b;
        public static final int fragment_performance_wall=0x7f03002c;
        public static final int fragment_planet=0x7f03002d;
        public static final int fragment_shared_learning=0x7f03002e;
        public static final int fragment_sl_details=0x7f03002f;
        public static final int fragment_test=0x7f030030;
        public static final int fragment_wocktube=0x7f030031;
        public static final int horizontal_image_scroller_item=0x7f030032;
        public static final int inbox_list_item=0x7f030033;
        public static final int milestone_list_item=0x7f030034;
        public static final int misc_ans_list_item=0x7f030035;
        public static final int my_dialog_layout=0x7f030036;
        public static final int performance_wall_child_list_item=0x7f030037;
        public static final int performance_wall_grp_list_item=0x7f030038;
        public static final int rating_dialog=0x7f030039;
        public static final int shared_learn_list_item=0x7f03003a;
        public static final int sl_comment_list_item=0x7f03003b;
        public static final int wocktube_list_item=0x7f03003c;
    }
    public static final class menu {
        public static final int login=0x7f0a0000;
    }
    public static final class string {
        public static final int action_settings=0x7f080001;
        public static final int action_websearch=0x7f080009;
        public static final int app_name=0x7f080000;
        public static final int app_not_available=0x7f08000a;
        public static final int btnLogin=0x7f080004;
        public static final int comment=0x7f080014;
        public static final int drawer_close=0x7f080008;
        public static final int drawer_open=0x7f080007;
        public static final int gray_gradient_1=0x7f08000b;
        public static final int gray_gradient_2=0x7f08000c;
        public static final int gray_gradient_3=0x7f08000d;
        public static final int gray_gradient_home=0x7f08000e;
        public static final int gray_gradient_per_chld=0x7f080010;
        public static final int gray_gradient_per_grp=0x7f08000f;
        public static final int onForgotBtnClick=0x7f080006;
        public static final int onLoginBtnClick=0x7f080005;
        public static final int pswd_hint=0x7f080003;
        public static final int red_gradient_1=0x7f080011;
        public static final int red_gradient_2=0x7f080012;
        public static final int red_gradient_3=0x7f080013;
        public static final int uname_hint=0x7f080002;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070001;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070003;
        public static final int Divider=0x7f070004;
        public static final int FullHeightDialog=0x7f070000;
        public static final int MyTheme=0x7f070002;
    }
}
