package com.o2m.wockhard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.o2m.wockhard.sharedlearning.SharedLearningActivity;

public class HomeActivity extends Activity implements OnClickListener, OnItemSelectedListener{

	ImageButton inboxBtn, faqBtn, milestonesBtn, pwBtn, wocktubeBtn;
	Button ePollBtn, shareLearningBtn , caseStudyBtn, assessmentBtn, mylearningBtn; 
	Spinner divSpinner, levelSpinner;
	ArrayList<String> lvlList, divList;
	HashMap<String, String> divmap;
	URLParams params;

	 public enum Screen {
	     ePoll, CaseStudy, SharedLearning, Assessment,  Inbox,
	     Wocktube, FAQ, Milestone, PerformanceWall, MyLearning, Wow, Feedback,Training,Avatar,Library, INTERACTIVE
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
	
		inboxBtn = (ImageButton) findViewById(R.id.inboxBtn);
		inboxBtn.setOnClickListener(this);
		
		faqBtn = (ImageButton) findViewById(R.id.faqsBtn);
		faqBtn.setOnClickListener(this);
		
		shareLearningBtn =  (Button) findViewById(R.id.sharedLearningBtn);
		shareLearningBtn.setOnClickListener(this);
		
		milestonesBtn =  (ImageButton) findViewById(R.id.milestoneBtn);
		milestonesBtn.setOnClickListener(this);
		
		pwBtn =  (ImageButton) findViewById(R.id.performaceBtn);
		pwBtn.setOnClickListener(this);
		
		ePollBtn =  (Button) findViewById(R.id.epollBtn);
		ePollBtn.setOnClickListener(this);
		
		 
		 wocktubeBtn =  (ImageButton) findViewById(R.id.wocktubeBtn);
		 wocktubeBtn.setOnClickListener(this);
		 
		 caseStudyBtn =  (Button) findViewById(R.id.csBtn);
		 caseStudyBtn.setOnClickListener(this);
		 
		 assessmentBtn =  (Button) findViewById(R.id.assessmentBtn);
		 assessmentBtn.setOnClickListener(this);
		 
		 mylearningBtn  =  (Button) findViewById(R.id.myLearningBtn);
		 mylearningBtn.setOnClickListener(this);

		 
			//URLParams.getInstance().setLoginResponse(dataArray.getJSONObject(0));
		 params = URLParams.getInstance();
		 String roleId = params.getRole_id();
		 String divCode = params.getDiv_code();
		 divSpinner = (Spinner) findViewById(R.id.divisionSpinner);
		 levelSpinner = (Spinner) findViewById(R.id.levelSpinner);
		 
			if (roleId.equals("1") ||  roleId.equals("6") || roleId.equals("3"))
			{
				 
				 divSpinner.setOnItemSelectedListener(this);
				 
			
				 levelSpinner.setOnItemSelectedListener(this);
				 
				 divmap = params.getDivMap();
				 divList =  new ArrayList<String>(divmap.keySet());
				 ArrayAdapter<String> divAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, divList);
				 divSpinner.setAdapter(divAdapter);
				 
				 ArrayList<String> levelList =  new ArrayList<String>();
				 levelList.add("L1");
				 levelList.add("L2");
				 levelList.add("L3");
				 
				  lvlList =  new ArrayList<String>(levelList);
				 ArrayAdapter<String> lvlAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, lvlList);
				 levelSpinner.setAdapter(lvlAdapter);
				 
				 int divPosition = 0, lvlPosition = 0;
				 
				 String divName = HomeActivity.getKeyFromValue(divmap, params.getDiv_code());
				 
				 divPosition = divList.indexOf(divName);
				 lvlPosition = lvlList.indexOf(params.getLevel_code());
				 
				 divSpinner.setSelection(divPosition);
				 levelSpinner.setSelection(lvlPosition);
			}
			else
			{
				divSpinner.setVisibility(View.INVISIBLE);
				levelSpinner.setVisibility(View.INVISIBLE);
			}
				
		 

		 
		 
		 
	
	}
	
	public static String getKeyFromValue(Map hm, Object value) {
	    for (Object o : hm.keySet()) {
	      if (hm.get(o).equals(value)) {
	        return (String)o;
	      }
	    }
	    return null;
	  }
	
	@Override
	public void onClick(View view) {
		
		if(view == inboxBtn)
		{
			Intent inboxIntent = new Intent(this, InboxActivity.class);
			startActivity(inboxIntent);
		}
		else if(view == shareLearningBtn)
		{
			Intent inboxIntent = new Intent(this, SharedLearningActivity.class);
			startActivity(inboxIntent);
		}
		else if(view == milestonesBtn)
		{
			Intent inboxIntent = new Intent(this, BaseActivity.class);
			inboxIntent.putExtra("SCREEN", Screen.Milestone.ordinal());
			startActivity(inboxIntent);
		}
		else if(view == pwBtn)
		{
			Intent inboxIntent = new Intent(this, BaseActivity.class);
			inboxIntent.putExtra("SCREEN", Screen.PerformanceWall.ordinal());
			startActivity(inboxIntent);
		}
		else if(view == faqBtn)
		{
			Intent inboxIntent = new Intent(this, BaseActivity.class);
			inboxIntent.putExtra("SCREEN", Screen.FAQ.ordinal());
			startActivity(inboxIntent);
		}
		else if(view == ePollBtn)
		{
			Intent inboxIntent = new Intent(this, BaseActivity.class);
			inboxIntent.putExtra("SCREEN", Screen.ePoll.ordinal());
			startActivity(inboxIntent);
		}
		else if(view == wocktubeBtn)
		{
			Intent wockTubeIntent = new Intent(this, BaseActivity.class);
			wockTubeIntent.putExtra("SCREEN", Screen.Wocktube.ordinal());
			startActivity(wockTubeIntent);
		}
		else if(view == caseStudyBtn)
		{
			Intent wockTubeIntent = new Intent(this, BaseActivity.class);
			wockTubeIntent.putExtra("SCREEN", Screen.CaseStudy.ordinal());
			startActivity(wockTubeIntent);
		}
		else if(view == assessmentBtn)
		{
			Intent assessmentIntent = new Intent(this, BaseActivity.class);
			assessmentIntent.putExtra("SCREEN", Screen.Assessment.ordinal());
			startActivity(assessmentIntent);
		}
		else if(view == mylearningBtn)
		{
//			Intent myLearningIntent = new Intent(this, LoadWebActivity.class);
//			startActivity(myLearningIntent);
			
			Intent assessmentIntent = new Intent(this, BaseActivity.class);
			assessmentIntent.putExtra("SCREEN", Screen.MyLearning.ordinal());
			startActivity(assessmentIntent);
			
		}
		else
		{
			Toast.makeText(this, "Under development", Toast.LENGTH_LONG).show();
		}
		
	}
	
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		
		//SharedPreferences sharedpreferences = getSharedPreferences("MyPREFERENCES", this.MODE_PRIVATE);
		
		if(parent.getId() == R.id.divisionSpinner)
		{
			params.setDiv_code(divmap.get(divList.get(pos)));
		}
		else if(parent.getId() == R.id.levelSpinner)
		{
			params.setLevel_code(lvlList.get(pos));
		}
		
	}
	
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		
		
	}
}
