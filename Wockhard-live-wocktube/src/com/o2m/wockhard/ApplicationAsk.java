package com.o2m.wockhard;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class ApplicationAsk extends AsyncTask<Integer, Object, Boolean>
{
	private Context activity;

	protected ApplicationService applicationService;

	private static final String FIELD_VERSION_CHECK = "versionCheck";

	public ProgressDialog customProgressDialog;

//	public CustomAlertDialog customAlertDialog;

	long downloadReference;

	DownloadManager downloadManager;

	//public CustomProgressDialog customSyncDialog;

	/**
	 * @return the applicationService
	 */
	public ApplicationService getApplicationService()
	{
		return applicationService;
	}

	/**
	 * @param applicationService the applicationService to set
	 */
	public void setApplicationService(ApplicationService applicationService)
	{
		this.applicationService = applicationService;
	}

	protected ProgressDialog progressDialog;

	private String message;

	public String error;

	protected boolean flag = true;

	public static boolean isShowServerMsg = true;

//	private static IParser parserInstance;

	/*public ApplicationAsk(Context context, ApplicationService applicationService)
	{
		this.context = context;
		this.applicationService = applicationService;
	}*/

	public ApplicationAsk(Context activity, ApplicationService applicationService)
	{
		this.activity = activity;
		this.applicationService = applicationService;
	}

	public ApplicationAsk(Context context)
	{
		this.activity = context;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		progressDialog = new ProgressDialog(activity); //taking object for progress dialog
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(false);
		progressDialog.show(); 
		/*
		customProgressDialog = new CustomProgressDialog(activity, "Please Wait", "Loading...",
				ProgressDialog.STYLE_SPINNER);
		customProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener()
		{

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
			{
				if (keyCode == KeyEvent.KEYCODE_MENU)
				{
					MobiculeLogger.verbose("SyncTask", "** MENU Pressed progressDialog ");
					return true;
				}
				return false;
			}
		});
		customProgressDialog.setCancelable(false);
		customProgressDialog.show();
		*/
	}

	@Override
	protected void onProgressUpdate(Object... values)
	{
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(Boolean result)
	{

		super.onPostExecute(result);
		//	progressDialog.dismiss();
		try
		{
			if (progressDialog != null)
			{
				progressDialog.dismiss();
			}
			
			applicationService.postExecute();
			/*
			if (parserResponse())
			{
				if (result)
				{
					MobiculeLogger.verbose("in application ask on post execute", "in if condition");
					applicationService.postExecute();
				}
				else
				{
					if (message == null || message.equals(""))
					{
						message = "Please try after some time";
					}
					showErrorDialog(message);

				}
			}
			*/
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	protected Boolean doInBackground(Integer... params)
	{
		try
		{
			applicationService.execute();

		}
		catch (Throwable e)
		{
			
			message = e.getMessage();
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public void showErrorDialog(String msg)
	{

		/*
		customAlertDialog = new CustomAlertDialog(activity, new OnCustomAlertDialogClickListener()
		{

			@Override
			public void positiveButtonOnClick(Context context)
			{
				customAlertDialog.cancel();
			}

			@Override
			public void negativeButtonOnClick(Context context)
			{

			}
		});
		customAlertDialog.setTitle("Error Occured");
		customAlertDialog.setCancelable(true);
		customAlertDialog.setMessage(msg);
		customAlertDialog.changeIcon(R.drawable.error);
		customAlertDialog.displayDialog(true, "Ok", false, null);
		customAlertDialog.show();
		*/
	}
/*
	
	*/
	/*
	private void resetParserInstance()
	{
		parserInstance = null;
		Parser.setPreviousInstance(null);
	}

	private void setPositiveButton(String positiveButton, AlertDialog.Builder submitCompleteDialog)
	{
		submitCompleteDialog.setMessage(parserInstance.getResponse().getMessage());
		submitCompleteDialog.setPositiveButton(positiveButton, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, int which)
			{
				dialog.dismiss();
				downloadAndInstall(parserInstance.getResponse().getData().toString());
				resetParserInstance();
			}
		});
	}
/*
	public void downloadAndInstall(String url)
	{
		try
		{
			downloadManager = (DownloadManager) activity.getSystemService(Activity.DOWNLOAD_SERVICE);
			Uri Download_Uri = Uri.parse(url);
			DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
			request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
					| DownloadManager.Request.NETWORK_MOBILE);
			request.setAllowedOverRoaming(false);
			request.setDestinationInExternalFilesDir(activity, Environment.DIRECTORY_DOWNLOADS,
					Download_Uri.getLastPathSegment());
			downloadReference = downloadManager.enqueue(request);
			DownloadReceiver.downloadReference = downloadReference;
			((Activity) activity).finish();
		}
		catch (Exception e)
		{
			Toast.makeText(activity, "Please enable your Download Manager", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	*/
}
