package com.o2m.wockhard;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.twotoasters.android.horizontalimagescroller.image.ImageToLoad;
import com.twotoasters.android.horizontalimagescroller.image.ImageToLoadDrawableResource;
import com.twotoasters.android.horizontalimagescroller.image.ImageToLoadUrl;
import com.twotoasters.android.horizontalimagescroller.widget.HorizontalImageScroller;
import com.twotoasters.android.horizontalimagescroller.widget.HorizontalImageScrollerAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TestActivity extends Activity implements OnClickListener {

	private String TAG = "LoginActivity";
	
	private Button loginBtn, forgotPasswordBtn;
	private EditText userNameEditText, passwordEditText;
	
	String divCode, roleId;
	
	@Override
	public void onClick(View arg0) {
		
		if(arg0 == loginBtn)
		{
			//new LoginTask().execute("8081","1");
			validateLogin(userNameEditText.getText().toString(), passwordEditText.getText().toString());
			//Toast.makeText(this, "login", Toast.LENGTH_LONG).show();
		}
		else if(arg0 == forgotPasswordBtn)
		{
			//Toast.makeText(this, "Not yet implemented", Toast.LENGTH_LONG).show();
			Intent homeIntent = new Intent(this, HomeActivity.class);
			startActivity(homeIntent);
			
		}
			
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		
		HorizontalImageScroller testScroller = (HorizontalImageScroller) findViewById(R.id.my_horizontal_image_scroller2);
        
        
        ArrayList<ImageToLoad> images1 = new ArrayList<ImageToLoad>();
        for (int i=0; i <  5; i++)
        {
       	 /*JSONObject imgObj = imageArray.getJSONObject(i);
       	 String imageTitle = imgObj.getString("img_title");
       	 String url = imgObj.getString("img_file");
       	 Log.v(TAG, "Title : " + imageTitle + "" + " \nimageLink : " + url);*/
       	 ImageToLoadUrl downloadImg = new ImageToLoadUrl("http://www.diporental.com/wp-content/gallery/listcar/toyota-innova2012.jpg");
            downloadImg.setCanCacheFile(true);
            images1.add(downloadImg); 
            // substitute some pretty picture you can stand to see 20 times in a list
           // images1.add(new ImageToLoadDrawableResource(R.drawable.px_transparent)); // plug in some of your own drawables
        }
        HorizontalImageScrollerAdapter adapter1 = new HorizontalImageScrollerAdapter(this, images1);
        adapter1.setImageSize(200);
        testScroller.setAdapter(adapter1);   
		
		
	}

	void login() {
	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	/*
	 private class LoginTask extends AsyncTask<String, String, String> {

		  private String resp;

		  @Override
		  protected String doInBackground(String... params) {
			 
			  JSONObject loginObj = new JSONObject();
			 // JSONObject inboxObj = new JSONObject();
	    		try {
	    			loginObj.put("p_username", params[0]);
	    			loginObj.put("p_pwd", params[1]);
	    			
	    			
	    		} catch (JSONException e) {
	    			
	    			e.printStackTrace();
	    		}
	    		
	    		String loginURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
	    		//String loginURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetDivision";
	    		//String inboxURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetInboxMessage_new";
	    		//application/x-www-form-urlencoded
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(loginURL, loginObj.toString());
		  
		   return resp;
		  }

		 
		  @Override
		  protected void onPostExecute(String result) {
		   
			  Log.v(TAG," onPostExecute Login Response : " + result);
			  
			  try {
				JSONObject loginResponseObj = new JSONObject(result);
				JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
				String status = loginResponseObj1.getString("Status");
				
				if(status.equalsIgnoreCase("success"))
				{
				
				}
				else
				{
					
				}
				
				
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			  
			  
			  
		  }

		  
		 
		 }
*/
	 private void validateLogin(final String username, final String password)
	 {
		 new ApplicationAsk(this, new ApplicationService() {
			
			 private String resp;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute Login Response : " + resp);
				  
				  try {
					  
					  SharedPreferences sharedPref = TestActivity.this.getPreferences(Context.MODE_PRIVATE);
					  SharedPreferences.Editor editor = sharedPref.edit();
					  editor.putString("uname", username);
					  editor.putString("upwd", password);
					  editor.commit();
					  JSONObject loginResponseObj = new JSONObject(resp);
						JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
						String status = loginResponseObj1.getString("Status");
						
						if(status.equalsIgnoreCase("success"))
						{

							JSONObject dataObj = loginResponseObj1.getJSONObject("data");
							JSONArray dataArray = dataObj.getJSONArray("Table");
							
							URLParams.getInstance().setLoginResponse(dataArray.getJSONObject(0));
							roleId=URLParams.getInstance().getRole_id();
							divCode=URLParams.getInstance().getDiv_code();
							
							Log.v(TAG, "roleId : " + roleId + "divCode : " + divCode);
							
							if (roleId.equals("1") ||  roleId.equals("6") ) {
								
								fetchDivCode();
								
							} 
							else if(roleId.equals("3"))
							{
								fetchDivCodeForL4L5();
							}
							else {
								
								Intent homeIntent = new Intent(TestActivity.this, HomeActivity.class);
								startActivity(homeIntent);
							}
													
							//fetchUserList();
						}
						else
						{
							Toast.makeText(TestActivity.this, "Error :"+loginResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
						}
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
				
			}
			
			

			@Override
			public void execute() {
				JSONObject loginObj = new JSONObject();
	    		try {
	    			loginObj.put("p_username", username);
	    			loginObj.put("p_pwd", password);
	    			//loginObj.put("", "");
	    		} catch (JSONException e) {
	    			
	    			e.printStackTrace();
	    		}
	    		
	    		//URLGenerator.getLoginUrl();
	    		
	    		String loginURL = URLGenerator.getLoginUrl();//"http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
	    		//String loginURL = "http://training.wockhardt.com/mobile/service1.asmx/validate_sql_user";
	    		
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(loginURL, loginObj.toString());
		  
		  // return resp;
				
			}
		}).execute();
	 }
	 
	 protected void fetchDivCodeForL4L5()
	 {
		 new ApplicationAsk(this, new ApplicationService() {
				
			 private String resp;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute fetchDivCodeForL4L5 Response : " + resp);
				  
				  try {
					  
					  
					  JSONObject divl4l5ResponseObj = new JSONObject(resp);
						JSONObject divl4l5ResponseObj1 = new JSONObject(divl4l5ResponseObj.getString("d"));
						String status = divl4l5ResponseObj1.getString("Status");
						
						if(status.equalsIgnoreCase("success"))
						{

							JSONObject dataObj = divl4l5ResponseObj1.getJSONObject("data");
							JSONArray divJsonArray = dataObj.getJSONArray("Table");
							
							//URLParams.getInstance().setLoginResponse(dataArray.getJSONObject(0));
							//save this json array					
							URLParams.getInstance().setDivMap(convertJsonArrayToMap(divJsonArray));
							Intent homeIntent = new Intent(TestActivity.this, HomeActivity.class);
							startActivity(homeIntent);
							
						}
						else
						{
							Toast.makeText(TestActivity.this, "Error :"+divl4l5ResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
						}
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
				
			}
						@Override
			public void execute() {
				JSONObject divcodObj = new JSONObject();
	    		try {
	    			divcodObj.put("p_employee_id", URLParams.getInstance().getEmployee_id());
	    		
	    		} catch (JSONException e) {
	    			
	    			e.printStackTrace();
	    		}
	    		
	    			    		
	    		String divisionl4l5URL = URLGenerator.getDivision_L5_L4_User();//"http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
	    			    		
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(divisionl4l5URL, divcodObj.toString());
		 
				
			}
		}).execute();
		
	}

	private void fetchDivCode() {
			
		new ApplicationAsk(this, new ApplicationService() {
			
			 private String resp;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute fetchDivCodeForL4L5 Response : " + resp);
				  
				  try {
					  
					  
					  JSONObject divl4l5ResponseObj = new JSONObject(resp);
						JSONObject divl4l5ResponseObj1 = new JSONObject(divl4l5ResponseObj.getString("d"));
						String status = divl4l5ResponseObj1.getString("Status");
						
						if(status.equalsIgnoreCase("success"))
						{

							JSONObject dataObj = divl4l5ResponseObj1.getJSONObject("data");
							JSONArray divJsonArray = dataObj.getJSONArray("Table");
							
							//URLParams.getInstance().setLoginResponse(dataArray.getJSONObject(0));
							//save this json array					
							URLParams.getInstance().setDivMap(convertJsonArrayToMap(divJsonArray));
							Intent homeIntent = new Intent(TestActivity.this, HomeActivity.class);
							startActivity(homeIntent);
							
						}
						else
						{
							Toast.makeText(TestActivity.this, "Error :"+divl4l5ResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
						}
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
				
			}
						@Override
			public void execute() {
				JSONObject divcodObj = new JSONObject();
	    			    			    		
	    		String divisionURL = URLGenerator.getAllDivision();//"http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
	    			    		
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(divisionURL, divcodObj.toString());
		 
				
			}
		}).execute();
		
		}
	
	private HashMap<String, String> convertJsonArrayToMap(JSONArray divJsonArray)
	{
		HashMap<String, String> divMap = new HashMap<String, String>();
		try {
			
		for (int i = 0; i < divJsonArray.length(); i++) {
			
			JSONObject divJsonObj;
			
				
				divJsonObj = divJsonArray.getJSONObject(i);
				divMap.put(divJsonObj.getString("division"), divJsonObj.getString("div_code"));
				
			
			
		}
		
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		return divMap;
	}
	 
	 /*private void fetchUserList()
	 {
		 new ApplicationAsk(this, new ApplicationService() {
				
			 private String resp;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute Login Response : " + resp);
				  
				  try {
					  
					  
					  JSONObject loginResponseObj = new JSONObject(resp);
						JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
						String status = loginResponseObj1.getString("Status");
						
						if(status.equalsIgnoreCase("success"))
						{

							JSONObject dataObj = loginResponseObj1.getJSONObject("data");
							JSONArray dataArray = dataObj.getJSONArray("Table");
							
							URLParams.getInstance().setUserResponse(dataArray.getJSONObject(0));
							
							Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
							startActivity(homeIntent);
						}
						else
						{
							Toast.makeText(LoginActivity.this, "Error :"+loginResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
						}
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void execute() {
				JSONObject userObj = new JSONObject();
	    		try {
	    			
	    			URLParams params = URLParams.getInstance();
	    			userObj.put("p_employee_id", params.getEmployee_id());
	    			userObj.put("p_div_code", params.getDiv_code());
	    			userObj.put("p_level_code", params.getLevel_code());
	    			//p_level_code
	    			//loginObj.put("", "");
	    		} catch (JSONException e) {
	    			
	    			e.printStackTrace();
	    		}
	    		
	    		//URLGenerator.getLoginUrl();
	    		
	    		String loginURL = URLGenerator.getUserListForEmployeeUrl();//"http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
	    		//String loginURL = "http://training.wockhardt.com/mobile/service1.asmx/validate_sql_user";
	    		
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(loginURL, userObj.toString());
		  
		  // return resp;
				
			}
		}).execute();
	 }
*/

}
