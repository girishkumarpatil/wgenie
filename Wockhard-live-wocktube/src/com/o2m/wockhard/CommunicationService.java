package com.o2m.wockhard;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

import android.util.Log;



public class CommunicationService 
{

	
	private BasicHttpParams httpParams;
	private DefaultHttpClient client;
	private String TAG = "CommunicationService";
	private HttpPost request;
	private BasicHttpResponse httpresponse;
	private BufferedReader in;

	public String sendSyncRequest(String postUrl, String postData)
	{
		
		Log.i(TAG, "postUrl : " + postUrl);
		Log.i(TAG, "postData : " + postData);
		StringBuffer sb = new StringBuffer("");
		
		try
		{
		httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
		HttpConnectionParams.setSoTimeout(httpParams, 60000);
		client = new DefaultHttpClient(httpParams);
		
		
		request = new HttpPost(postUrl);
		request.addHeader("Content-type", "application/json");
		//request.addHeader("Content-type", "application/x-www-form-urlencoded");
		
		request.setEntity(new StringEntity(postData));
		
		httpresponse = (BasicHttpResponse) client.execute(request);
		
		
		in = new BufferedReader(new InputStreamReader(httpresponse.getEntity().getContent()));

		String line = "";

		while ((line = in.readLine()) != null)
		{
			sb.append(line);
		}
		in.close();

		}
		catch (UnsupportedEncodingException e)
		{
			Log.e(TAG, " sendRequest Exception 1 " + e.getMessage());
		}
		catch (ClientProtocolException e)
		{
			Log.e(TAG, " sendRequest Exception 2 " + e.getMessage());
		}
		catch (IllegalStateException e)
		{
			Log.e(TAG, " sendRequest Exception 3 " + e.getMessage());
		}
		catch (IOException e)
		{
			Log.e(TAG, " sendRequest Exception 4 " + e.getMessage());
		}
		
		
		String response = sb.toString();
		//response = response.trim();
		// response = response.substring(1, response.length()-1);
		// response = response.replace("\\", "");
		Log.i(TAG, "response from server   :::::::     " + response);
		
		
		
		
		return response;
	}
	
	
	public String sendMultiPartRequest(String multipartUrl,
			MultipartEntityBuilder builder, int timeout,
			boolean isLogEnabled) {

		int responseCode = 0;
		int invalidFileCount = 0;
		//Response responseObject = new Response(FAILURE, UNKNOWN_ERROR, false,
		//		"", 0);
		URL url = null;
		String serverResponseMessage = "";
		HttpURLConnection urlConnection = null;

		HashMap<String, String> headerFields = null;

		HashMap<String, Object> dataPart = null;
		HashMap<String, String> fileData = null;

		if (isLogEnabled) {
			Log.v("multipartUrl :", multipartUrl);
			Log.v("timeout :", "" + timeout);

		}
		try {
//			if (multipartRequestHeader != null) {
//			//	headerFields = multipartRequestHeader.getHeaderFields();
//				dataPart = multipartRequestHeader.getDataParams();
//				fileData = multipartRequestHeader.getFileData();
//			}

			//MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

//			if (dataPart != null && fileData != null) {
//				if (dataPart.size() != 0) {
//					for (String dataKey : dataPart.keySet()) {
//						String data = dataPart.get(dataKey).toString();
//						builder.addPart(dataKey, new StringBody(data,
//								ContentType.TEXT_PLAIN));
//						if (isLogEnabled) {
//							Log.v("Multipart data key :" + dataKey,
//											"Multipart data Value : " + data);
//						}
//					}
//				}
//				if (fileData.size() != 0) {
//					for (String fileName : fileData.keySet()) {
//						String filePath = fileData.get(fileName);
//						if (filePath.length() != 0 || filePath.equals("")) {
//							File multipartFile = new File(filePath);
//							if (multipartFile.exists()) {
//								FileBody fileBody = new FileBody(multipartFile);
//								builder.addPart(fileName, fileBody);
//
//								if (isLogEnabled) {
//									Log.v(
//											"Multipart fileName : " + fileName,
//											"Multipart filePath : " + filePath);
//								}
//							} else {
//								invalidFileCount++;
//								Log.v("",filePath + " is invalid.");
//							}
//						}
//					}
//				}
//				if (invalidFileCount == fileData.size()) {
//					//return new Response(FAILURE, INVALID_FILE_PATH_SPECIFIED,
//					//		false, "", 0);
//					return "INVALID_FILE_PATH_SPECIFIED";
//							
//				}

				HttpEntity entity = builder.build();
				Log.v(TAG, "HttpEntity : "+entity.toString());

				url = new URL(multipartUrl);
				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setConnectTimeout(timeout);
				urlConnection.setReadTimeout(timeout);
				urlConnection.setDoOutput(true);
				urlConnection.setRequestMethod(NetworkConstants.POST);
//				if (headerFields != null) {
//					if (headerFields.size() != 0) {
//						for (String headerKey : headerFields.keySet()) {
//							if (!headerKey.equals(NetworkConstants.CONTENT_TYPE)) {
//								urlConnection.setRequestProperty(headerKey,
//										headerFields.get(headerKey));
//
//								if (isLogEnabled) {
//									Log.v(NetworkConstants.HEADER_KEY, headerKey);
//									Log.v(NetworkConstants.HEADER_VALUE,
//											headerFields.get(headerKey));
//								}
//							}
//
//						}
//					}
//				}
				urlConnection.addRequestProperty(entity.getContentType()
						.getName(), entity.getContentType().getValue());
				urlConnection.addRequestProperty(NetworkConstants.CONTENT_LENGTH,
						entity.getContentLength() + "");
				if (isLogEnabled) {
					Log.v(NetworkConstants.CONTENT_LENGTH,
							"" + entity.getContentLength());
					Log.v(NetworkConstants.CONTENT_TYPE, ""
							+ entity.getContentType().getValue());
				}
				OutputStream outputStream = urlConnection.getOutputStream();
				entity.writeTo(urlConnection.getOutputStream());
				outputStream.close();
				urlConnection.connect();
				responseCode = urlConnection.getResponseCode();

				if (responseCode == HttpURLConnection.HTTP_OK
						|| responseCode == HttpURLConnection.HTTP_CREATED || responseCode==HttpURLConnection.HTTP_NOT_AUTHORITATIVE) {
					InputStream inputStream = urlConnection.getInputStream();
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(inputStream));
					String inputLine = "";
					StringBuilder stringBuilder = new StringBuilder("");
					while ((inputLine = bufferedReader.readLine()) != null) {
						stringBuilder.append(inputLine);
					}
					serverResponseMessage = stringBuilder.toString();

					if (inputStream != null) {
						inputStream.close();
					}
					if (bufferedReader != null) {
						bufferedReader.close();
					}

					if (isLogEnabled) {
						Log.v("Server Response : ",
								serverResponseMessage);
					}
					
					return serverResponseMessage;
					
				} else {
					return "NETWORK_FAIL";
				}
			
		} catch (HttpHostConnectException httpHostConnectException) {
			// This Exception happens when you attempt to open a TCP connection
			// from an IP address where there is nothing currently listening for
			// connection.

			//responseObject = getResponse("", responseCode, NO_INTERNET_ACCESS,
					//FAILURE, getResponseHeader(urlConnection));

			httpHostConnectException.printStackTrace();
			return "NO_INTERNET_ACCESS";
		} catch (UnknownHostException unknownHostException) {
			// This Exception occurs when you are trying to connect to a remote
			// host using Host name but IP address of that Host can not be
			// resolved.

//			responseObject = getResponse("", responseCode, NO_INTERNET_ACCESS,
//					FAILURE, getResponseHeader(urlConnection));

			unknownHostException.printStackTrace();
			return "NO_INTERNET_ACCESS";
		} catch (ConnectTimeoutException connectTimeoutException) {

			connectTimeoutException.printStackTrace();
			return "TIMEOUT_ERROR";
		} catch (SocketTimeoutException socketTimeoutException) {

			socketTimeoutException.printStackTrace();
			return "SOCKET_TIMEOUT";
		} catch (ConnectException connectException) {

			connectException.printStackTrace();
			return "NO_INTERNET_ACCESS";

		} catch (IOException ioException) {

			ioException.printStackTrace();
			return "NETWORK_FAIL";

		} catch (Exception exception) {

			exception.printStackTrace();
			return "NETWORK_FAIL";
		}

	}
	
	public String sendMultiPartRequestTest()
	{
	   final String BOUNDARY =  "+++++";
	   final String LOG_TAG = "FileTransfer";
	   final String LINE_START = "--";
	   final String LINE_END = "\r\n";
		
		try {
			URL url = new URL("");
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.setUseCaches(false);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
			
			
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
