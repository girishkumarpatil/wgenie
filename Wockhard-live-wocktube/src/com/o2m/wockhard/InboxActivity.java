package com.o2m.wockhard;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.wockhard.inbox.MessageListFragment;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class InboxActivity extends Activity {

	private String TAG = "InboxActivity";
	 private DrawerLayout mDrawerLayout;
	    private ListView mDrawerList;
	    private String[] mPlanetTitles;
		private int i, selectedPosition;
		 enum MessageType {
			    Inbox, Important, Archive, Deleted 
			};
		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox);
	
       
		 mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	        mDrawerList = (ListView) findViewById(R.id.left_drawer);
	        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
	        mPlanetTitles = getResources().getStringArray(R.array.inbox_submenu_array);
	        // set up the drawer's list view with items and click listener
	        MsgNavigationDrawerListAdapter msgMenuAdapter = new MsgNavigationDrawerListAdapter(this, mPlanetTitles);
	        mDrawerList.setAdapter(msgMenuAdapter);
/*//	        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
//	                R.layout.drawer_list_item, mPlanetTitles));
*/	        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
	        selectItem(MessageType.Inbox.ordinal());

		
	}
	
	public void menuButton(View v)
	{
		if (i == 0)
		{
			mDrawerLayout.openDrawer(mDrawerList);
			i = 1;
		}
		else
		{
			i = 0;
			mDrawerLayout.closeDrawer(mDrawerList);
		}

	}
	
	public void homeButton(View v)
	{
		Intent homeIntent = new Intent(this, GridHomeActivity.class);
		startActivity(homeIntent);
	}
	
	 private class DrawerItemClickListener implements ListView.OnItemClickListener {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            selectItem(position);
	        }
	    }
	 
	 private void selectItem(final int position) {
		 selectedPosition = position;
	        switch (position) {
			case 0:
				this.loadInbox();
				break;
			case 1:
				this.loadImportant();
				break;
			case 2:
				this.loadArchieves();
				break;
			case 3:
				this.loadDeleted();
				break;

			default:
				break;
			}
	      
	        
	       
	    }
	 
	 private void loadInbox()
	 {
		  new ApplicationAsk(this, new ApplicationService() {
				
				 private String resp;
				 private JSONArray msgArray;
				
				@Override
				public void postExecute() {
					Log.v(TAG," onPostExecute inbox Response : " + resp);
					  
					  try {
						  
						  
						JSONObject loginResponseObj = new JSONObject(resp);
						//Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("Status"));
					//	Log.v(TAG," onPostExecute message : " +  loginResponseObj.getString("message"));
						
						Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("d"));
						
						JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  loginResponseObj1.getString("Status"));
						
						JSONObject dataObj = loginResponseObj1.getJSONObject("data");
						 msgArray = dataObj.getJSONArray("Messages");
						 if(msgArray == null)
							{
								Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_SHORT).show();
								return;
							}
							if(msgArray.length() <= 0)
							{
								Toast.makeText(InboxActivity.this, "No Message to Display", Toast.LENGTH_SHORT).show();
								return;
							}
							
							Fragment fragment = new MessageListFragment(msgArray, "Inbox");  
							 /* FragmentManager fragmentManager = getFragmentManager();
						        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();*/
						        
						        FragmentTransaction ft = getFragmentManager().beginTransaction();
								//ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,R.anim.slide_in_left, R.anim.slide_out_right) ;
						        ft.addToBackStack("messageList");
								ft.replace(R.id.content_frame, fragment, "messageList");
								
								ft.commit();
						        

						        // update selected item and title, then close the drawer
						        mDrawerList.setItemChecked(selectedPosition, true);
						        setTitle(mPlanetTitles[selectedPosition]);
						        mDrawerLayout.closeDrawer(mDrawerList);
						
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
					  
					  
					  
					  

					
				}
				
				@Override
				public void execute() {
					JSONObject inboxRequestObj = new JSONObject();
			        try {
						/*inboxRequestObj.putOpt("p_div_code", "E");
						inboxRequestObj.putOpt("p_level_code", "L1");
						inboxRequestObj.putOpt("p_zone_code", "z1");
						inboxRequestObj.putOpt("p_region_code", "r1");
						inboxRequestObj.putOpt("p_user_id", "2");
						inboxRequestObj.putOpt("p_msg_flag", "0");*/
			        	
			        	URLParams paramas = URLParams.getInstance();
			        	
			        	inboxRequestObj.putOpt("p_div_code", paramas.getDiv_code());
						inboxRequestObj.putOpt("p_level_code", paramas.getLevel_code());
						inboxRequestObj.putOpt("p_zone_code", paramas.getZone_code());
						inboxRequestObj.putOpt("p_region_code", paramas.getRegion_code());
						inboxRequestObj.putOpt("p_user_id", paramas.getUser_id());
						inboxRequestObj.putOpt("p_msg_flag", "0");
			        	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		//String inboxURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetInboxMessage_new";
			        
			        String inboxURL = URLGenerator.getInboxMessages();
			        
		    		CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(inboxURL, inboxRequestObj.toString());
			  
			  // return resp;
					
				}
			}).execute();
	 }
	
	 
	 private void loadImportant()
	 {
		  new ApplicationAsk(this, new ApplicationService() {
				
				 private String resp;
				 private JSONArray msgArray;
				
				@Override
				public void postExecute() {
					Log.v(TAG," onPostExecute inbox Response : " + resp);
					  
					  try {
						  
						  
						JSONObject loginResponseObj = new JSONObject(resp);
						//Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("Status"));
					//	Log.v(TAG," onPostExecute message : " +  loginResponseObj.getString("message"));
						
						Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("d"));
						
						JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  loginResponseObj1.getString("Status"));
						
						JSONObject dataObj = loginResponseObj1.getJSONObject("data");
						 msgArray = dataObj.getJSONArray("Table");
						 if(msgArray == null)
							{
								Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_SHORT).show();
								return;
							}
							if(msgArray.length() <= 0)
							{
								Toast.makeText(InboxActivity.this, "No Message to Display", Toast.LENGTH_SHORT).show();
								return;
							}
							Fragment fragment = new MessageListFragment(msgArray, "Important");  
							  FragmentManager fragmentManager = getFragmentManager();
						        FragmentTransaction ft = fragmentManager.beginTransaction();
						        ft.addToBackStack("Important");
						        ft.replace(R.id.content_frame, fragment).commit();

						        // update selected item and title, then close the drawer
						        mDrawerList.setItemChecked(selectedPosition, true);
						        setTitle(mPlanetTitles[selectedPosition]);
						        mDrawerLayout.closeDrawer(mDrawerList);
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
					  
					  
					  
					  

					
				}
				
				@Override
				public void execute() {
					JSONObject inboxRequestObj = new JSONObject();
			        try {
						/*inboxRequestObj.putOpt("p_div_code", "E");
						inboxRequestObj.putOpt("p_level_code", "L1");
						inboxRequestObj.putOpt("p_zone_code", "z1");
						inboxRequestObj.putOpt("p_region_code", "r1");
						inboxRequestObj.putOpt("p_user_id", "2");
						inboxRequestObj.putOpt("p_msg_flag", "1");*/
			        	
			        	URLParams paramas = URLParams.getInstance();
			        	
			        	inboxRequestObj.putOpt("p_div_code", paramas.getDiv_code());
						inboxRequestObj.putOpt("p_level_code", paramas.getLevel_code());
						inboxRequestObj.putOpt("p_zone_code", paramas.getZone_code());
						inboxRequestObj.putOpt("p_region_code", paramas.getRegion_code());
						inboxRequestObj.putOpt("p_user_id", paramas.getUser_id());
						inboxRequestObj.putOpt("p_msg_flag", "1");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		/*String inboxURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetInboxMessage_new";*/
			        String inboxURL = URLGenerator.getOtherMessages();
		    		CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(inboxURL, inboxRequestObj.toString());
			  
			  // return resp;
					
				}
			}).execute();
	 }
	 
	 private void loadArchieves()
	 {
		  new ApplicationAsk(this, new ApplicationService() {
				
				 private String resp;
				 private JSONArray msgArray;
				
				@Override
				public void postExecute() {
					Log.v(TAG," onPostExecute inbox Response : " + resp);
					  
					  try {
						  
						  
						JSONObject loginResponseObj = new JSONObject(resp);
						//Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("Status"));
					//	Log.v(TAG," onPostExecute message : " +  loginResponseObj.getString("message"));
						
						Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("d"));
						
						JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  loginResponseObj1.getString("Status"));
						
						JSONObject dataObj = loginResponseObj1.getJSONObject("data");
						 msgArray = dataObj.getJSONArray("Table");
						 if(msgArray == null)
							{
								Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_SHORT).show();
								return;
							}
							if(msgArray.length() <= 0)
							{
								Toast.makeText(InboxActivity.this, "No Message to Display", Toast.LENGTH_SHORT).show();
								return;
							}
							
							Fragment fragment = new MessageListFragment(msgArray, "Archieved");  
							  FragmentManager fragmentManager = getFragmentManager();
							  
						         FragmentTransaction ft = fragmentManager.beginTransaction();
						         ft.addToBackStack("Archieved");
						        ft.replace(R.id.content_frame, fragment).commit();

						        // update selected item and title, then close the drawer
						        mDrawerList.setItemChecked(selectedPosition, true);
						        setTitle(mPlanetTitles[selectedPosition]);
						        mDrawerLayout.closeDrawer(mDrawerList);

						
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
					  
					  
					  
					  
					
				}
				
				@Override
				public void execute() {
					JSONObject inboxRequestObj = new JSONObject();
			        try {
						/*inboxRequestObj.putOpt("p_div_code", "E");
						inboxRequestObj.putOpt("p_level_code", "L1");
						inboxRequestObj.putOpt("p_zone_code", "z1");
						inboxRequestObj.putOpt("p_region_code", "r1");
						inboxRequestObj.putOpt("p_user_id", "2");
						inboxRequestObj.putOpt("p_msg_flag", "2");*/
			        	URLParams paramas = URLParams.getInstance();       	
			        	inboxRequestObj.putOpt("p_div_code", paramas.getDiv_code());
						inboxRequestObj.putOpt("p_level_code", paramas.getLevel_code());
						inboxRequestObj.putOpt("p_zone_code", paramas.getZone_code());
						inboxRequestObj.putOpt("p_region_code", paramas.getRegion_code());
						inboxRequestObj.putOpt("p_user_id", paramas.getUser_id());
						inboxRequestObj.putOpt("p_msg_flag", "3");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		/*String inboxURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetInboxMessage_new";*/
			        String inboxURL = URLGenerator.getOtherMessages();
		    		CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(inboxURL, inboxRequestObj.toString());
			  
			  // return resp;
					
				}
			}).execute();
	 }
	 
	 private void loadDeleted()
	 {
		  new ApplicationAsk(this, new ApplicationService() {
				
				 private String resp;
				 private JSONArray msgArray;
				
				@Override
				public void postExecute() {
					Log.v(TAG," onPostExecute inbox Response : " + resp);
					  
					  try {
						  
						  
						JSONObject loginResponseObj = new JSONObject(resp);
						//Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("Status"));
					//	Log.v(TAG," onPostExecute message : " +  loginResponseObj.getString("message"));
						
						Log.v(TAG," onPostExecute Status : " + loginResponseObj.getString("d"));
						
						JSONObject loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  loginResponseObj1.getString("Status"));
						
						JSONObject dataObj = loginResponseObj1.getJSONObject("data");
						 msgArray = dataObj.getJSONArray("Table");
						
						if(msgArray == null)
						{
							Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_SHORT).show();
							return;
						}
						if(msgArray.length() <= 0)
						{
							Toast.makeText(InboxActivity.this, "No Message to Display", Toast.LENGTH_SHORT).show();
							return;
						}
						 Fragment fragment = new MessageListFragment(msgArray, "Deleted");  
						  FragmentManager fragmentManager = getFragmentManager();
					        FragmentTransaction ft = fragmentManager.beginTransaction();
					        ft.addToBackStack("Deleted");
					        ft.replace(R.id.content_frame, fragment).commit();

					        // update selected item and title, then close the drawer
					        mDrawerList.setItemChecked(selectedPosition, true);
					        setTitle(mPlanetTitles[selectedPosition]);
					        mDrawerLayout.closeDrawer(mDrawerList);
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(InboxActivity.this, "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
					  
					  
					  
					 

					
				}
				
				@Override
				public void execute() {
					JSONObject inboxRequestObj = new JSONObject();
			        try {
						/*inboxRequestObj.putOpt("p_div_code", "E");
						inboxRequestObj.putOpt("p_level_code", "L1");
						inboxRequestObj.putOpt("p_zone_code", "z1");
						inboxRequestObj.putOpt("p_region_code", "r1");
						inboxRequestObj.putOpt("p_user_id", "2");
						inboxRequestObj.putOpt("p_msg_flag", "3");*/
URLParams paramas = URLParams.getInstance();
			        	
			        	inboxRequestObj.putOpt("p_div_code", paramas.getDiv_code());
						inboxRequestObj.putOpt("p_level_code", paramas.getLevel_code());
						inboxRequestObj.putOpt("p_zone_code", paramas.getZone_code());
						inboxRequestObj.putOpt("p_region_code", paramas.getRegion_code());
						inboxRequestObj.putOpt("p_user_id", paramas.getUser_id());
						inboxRequestObj.putOpt("p_msg_flag", "2");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		/*String inboxURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetInboxMessage_new";*/
			        String inboxURL = URLGenerator.getOtherMessages();
		    		CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(inboxURL, inboxRequestObj.toString());
			  
			  // return resp;
					
				}
			}).execute();
	 }
	 
	 @Override
		public boolean onKeyDown(int keyCode, KeyEvent event)
		{
			if (keyCode == KeyEvent.KEYCODE_BACK)
			{

				if (getFragmentManager().getBackStackEntryCount() == 1)
				{
					this.finish();
					//return false;
				}
				else
				{

					getFragmentManager().popBackStack();
					removeCurrentFragment();
					return false;
				}

			}
			return super.onKeyDown(keyCode, event);
		}
	 
		public void removeCurrentFragment()
		{
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			Fragment currentFrag = getFragmentManager().findFragmentById(R.id.content_frame);

			if (currentFrag != null)
				transaction.remove(currentFrag);

			transaction.commit();

		}
}
