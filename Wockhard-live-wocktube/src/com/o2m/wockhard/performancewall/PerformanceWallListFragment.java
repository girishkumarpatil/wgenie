package com.o2m.wockhard.performancewall;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.o2m.wockhard.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PerformanceWallListFragment extends Fragment implements OnItemClickListener {

	private ExpandableListView performanceWallListView;
	private JSONArray performanceList;
	
	 public PerformanceWallListFragment(JSONArray _performanceList) {
         // Empty constructor required for fragment subclasses
		 this.performanceList = _performanceList;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.fragment_performance_wall, container, false);
         performanceWallListView = (ExpandableListView) rootView.findViewById(R.id.performanceList);
        
         
         PerformanceWallAdapter inboxMsgAdapter = new PerformanceWallAdapter(this.getActivity(), performanceList);
         performanceWallListView.setAdapter(inboxMsgAdapter);
        
         setGroupIndicatorToRight();
        // performanceWallListView.setIndicatorBounds(200, 200);
         //shredLearnListView.setOnItemClickListener(this);
         performanceWallListView.expandGroup(0, true);
         return rootView;
     }

     private void setGroupIndicatorToRight() {
         /* Get the screen width */
         DisplayMetrics dm = new DisplayMetrics();
         getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
         int width = dm.widthPixels;
  
         //performanceWallListView.setIndicatorBounds(width - getDipsFromPixel(35), width
         //        - getDipsFromPixel(5));

         if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
        	 performanceWallListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                     - getDipsFromPixel(5));
        	} else {
        		performanceWallListView.setIndicatorBoundsRelative(width - getDipsFromPixel(35), width
                        - getDipsFromPixel(5));
        	   //mListView.setIndicatorBoundsRelative(myLeft, myRight);
        	}
         
     }
  
     // Convert pixel to dip
     public int getDipsFromPixel(float pixels) {
         // Get the screen's density scale
         final float scale = getResources().getDisplayMetrics().density;
         // Convert the dps to pixels, based on density scale
         return (int) (pixels * scale + 0.5f);
     }
    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("MessageListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		 /*
		try {
			JSONObject msgObj = (JSONObject) sharedLearnList.get(position);
			
			Fragment fragment = new SharedLearningDetailsFragment(msgObj);

	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().addToBackStack("vg");
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		
	}

	
	
}
