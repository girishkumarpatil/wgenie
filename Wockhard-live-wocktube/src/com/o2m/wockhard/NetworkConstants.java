package com.o2m.wockhard;

public class NetworkConstants {
	public static final String NO_SERVER_ACCESS = "Server Not Found";
	public static final String NO_INTERNET_ACCESS = "Connection could not be established either because of device's or server's network connectivity";
	public static final String NETWORK_FAIL = "Unknown Error Occurred";
	public static final String TIMEOUT_ERROR = "Connection Timeout";
	public static final String SOCKET_TIMEOUT = "Socket Timeout";
	public static final String INVALID_URL = "Invalid Url";
	public static final String FAILURE = "FAILURE";
	public static final String SUCCESS = "SUCCESS";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_LENGTH = "Content-length";
	public static final int STATUS_CODE_ZERO = 0;

	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String PUT = "PUT";
	public static final String PATCH = "PATCH";
	public static final String DELETE = "DELETE";

	//
	public static final String INVALID_REQUEST_BODY = "Invalid Request Body Configuration";
	public static final String INVALID_REQUEST_HEADER = "Invalid Request Header Configuration";
	public static final String INVALID_POST_REQUEST = "Invalid Post Request";
	public static final String INVALID_MULTIPART_REQUEST = "Invalid Multipart Request";
	public static final String INVALID_CONTENT_TYPE = "Configure Content Type";
	public static final String INVALID_SSL_PINING_REQUEST = "Disable SSL Pinning Mode For http Request.";
	public static final String INVALID_CERTIFICATE_NAMES = "Please Specify Valid Certificate Names.";
	public static final String UNKNOWN_ERROR = "Unknown Error Occurred";
	public static final String INVALID_FILE_PATH_SPECIFIED = "Specified File Paths are Invalid ";

	public static final String BAD_REQUEST = "Bad Request";
	public static final String UNAUTHORIZED = "Unauthorized";
	public static final String FORBIDDEN = "Forbidden";
	public static final String METHOD_NOT_ALLOWED = "Method Not Allowed";
	public static final String NOT_ACCEPTABLE = "Not Acceptable";
	public static final String REQUEST_TIMEOUT = "Request Timeout";
	public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
	public static final String NOT_IMPLEMENTED = "Not Implemented";
	public static final String BAD_GATEWAY = "Bad Gateway";
	public static final String SERVICE_UNAVAILABLE = "Service Unavailable";
	public static final String GATEWAY_TIMEOUT = "Gateway Timeout";

	public static final String UNEXPECTED_HTTPMETHOD = "Unexpected Http Method";

	public static final String INVALID_REQUEST_METHOD = "Please Specify Valid Http Method Name";

	public static final String URL = "Url : ";
	public static final String MULTIPART_URL = "multipartUrl :";
	public static final String TIMEOUT_VALUE = "Timeout value : ";
	public static final String HTTPREQUEST = "HttpRequest : ";
	public static final String REQUESTBODY = "RequestBody : ";
	public static final String REQUESTDIGEST = "Request Digest : ";

	public static final String SERVER_RESPONSE_MESSAGE = "Server Response Message : ";

	public static final String HEADER_KEY = "Header Key : ";
	public static final String HEADER_VALUE = "Header Value : ";
	public static final String COMPRESSED_HEADER_KEY = "compressed";
	public static final String COMPRESSED_ENABLE_VALUE = "true";
	public static final String DIGEST_HEADER_KEY = "digest";
	public static final String DEVICE_DIGEST = "Device Digest";
	public static final String SERVER_DIGEST = "Server Digest";

	public static final String INVALID_DIGEST_MESSAGE = "Response Digest Mismatch";

	public static final String DATA = "data";
	public static final String MESSAGE = "message";
	public static final String STATUS = "status";
	public static final String FILE_EXISTS = "File already Exists !!";
	public static final String COMPRESSION_ENABLED = "Compression Enabled";
}
