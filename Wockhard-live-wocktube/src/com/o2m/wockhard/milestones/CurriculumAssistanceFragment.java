package com.o2m.wockhard.milestones;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CurriculumAssistanceFragment extends Fragment implements OnItemClickListener {

	private ExpandableListView caListView;
	private JSONArray caList;
	
	 public CurriculumAssistanceFragment(JSONArray _caList) {
         // Empty constructor required for fragment subclasses
		 this.caList = _caList;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.fragment_milestore_curriculum, container, false);
         caListView = (ExpandableListView) rootView.findViewById(R.id.caList);
         TextView certiName = (TextView) rootView.findViewById(R.id.caCertificateName);
         TextView status = (TextView) rootView.findViewById(R.id.caStatus);
         TextView expiryDate = (TextView) rootView.findViewById(R.id.caExpiry);
         Button gradeBtn = (Button) rootView.findViewById(R.id.klBtn);
         
         CurriculumAssessmentAdapter faqAdapter = new CurriculumAssessmentAdapter(this.getActivity(), caList);
         caListView.setAdapter(faqAdapter);
        
         this.setGroupIndicatorToRight();
         //shredLearnListView.setOnItemClickListener(this);
         
         try {
 			 
        	JSONObject caJsonObj = caList.getJSONObject(0);
        	String cstatus = caJsonObj.getString("status");
 			certiName.setText(caJsonObj.getString("cert_name"));
 			status.setText(cstatus);
 			expiryDate.setText(caJsonObj.getString("Expire on"));
 			
 			if(cstatus.equalsIgnoreCase("completed"))
 			{
 				String certificate = caJsonObj.getString("Certificate");
 				
 				if(certificate.equalsIgnoreCase("Bronze"))
 				{
 					gradeBtn.setBackgroundResource(R.drawable.bronze);
 				}
 				else if(certificate.equalsIgnoreCase("Silver"))
 				{
 					gradeBtn.setBackgroundResource(R.drawable.silver);
 				}
 				else if(certificate.equalsIgnoreCase("Gold"))
 				{
 					gradeBtn.setBackgroundResource(R.drawable.gold);
 				}
 				else
 				{
 					gradeBtn.setVisibility(View.GONE);
 				}
 				
 			}
 			else
 			{
 				gradeBtn.setVisibility(View.GONE);
 			}
 			
         }
         catch(Exception e)
         {}
         
         
         return rootView;
     }

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("MessageListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		 /*
		try {
			JSONObject msgObj = (JSONObject) sharedLearnList.get(position);
			
			Fragment fragment = new SharedLearningDetailsFragment(msgObj);

	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().addToBackStack("vg");
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		
	}

	 private void setGroupIndicatorToRight() {
         /* Get the screen width */
         DisplayMetrics dm = new DisplayMetrics();
         getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
         int width = dm.widthPixels;
  
         //performanceWallListView.setIndicatorBounds(width - getDipsFromPixel(35), width
         //        - getDipsFromPixel(5));

         if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
        	 caListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                     - getDipsFromPixel(5));
        	} else {
        		caListView.setIndicatorBoundsRelative(width - getDipsFromPixel(35), width
                        - getDipsFromPixel(5));
        	   //mListView.setIndicatorBoundsRelative(myLeft, myRight);
        	}
         
     }
  
     // Convert pixel to dip
     public int getDipsFromPixel(float pixels) {
         // Get the screen's density scale
         final float scale = getResources().getDisplayMetrics().density;
         // Convert the dps to pixels, based on density scale
         return (int) (pixels * scale + 0.5f);
     }
	
}
