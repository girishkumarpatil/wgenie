package com.o2m.wockhard.milestones;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class CurriculumAssessmentAdapter extends BaseExpandableListAdapter 
{

	Context context;

	ArrayList<String> arrayMessages;

	JSONObject caJsonObj;
	String[] grpTitle = {"Courses Assigned","Assesments", "Wocktube"};
	
	JSONArray caJsonArray, coursesJsonArray, assesmenJsonArray, wocktubeJsonArray;
	String unreadCount;

	//private ChannelDetails channelDetails;

	//public MessageAdapter(Context context, ArrayList<String> _arrayMessages)
	public CurriculumAssessmentAdapter(Context context, JSONArray _arrayFaq)
	{

		this.context = context;
		this.caJsonArray = _arrayFaq;
		try {
			caJsonObj = caJsonArray.getJSONObject(0);
			coursesJsonArray = caJsonObj.getJSONArray("Courses");
			wocktubeJsonArray = caJsonObj.getJSONArray("wocktube");
			assesmenJsonArray = caJsonObj.getJSONArray("Assessments");
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	private static class ChildViewHolder
	{

		public TextView txtView_que;
		public TextView txtView_ans;
		
	}
	
	private static class GroupViewHolder
	{

		public TextView txtView_grpTitle;
		
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return grpTitle.length;
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		/*
		 * try { JSONObject performanceObj = (JSONObject)
		 * faqJsonArray.get(groupPosition); String noOfEmp =
		 * performanceObj.getString("no_of_emp"); int noEmp =
		 * Integer.parseInt(noOfEmp); return noEmp; } catch
		 * (org.json.JSONException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		switch (groupPosition) {
		case 0:
			if (coursesJsonArray != null)
				return coursesJsonArray.length();
			else
				return 0;
		case 1:
			if (assesmenJsonArray != null)
				return assesmenJsonArray.length();
			else
				return 0;
		case 2:
			if (wocktubeJsonArray != null)
				return wocktubeJsonArray.length();
			else
				return 0;

		default:
			break;
		}

		return 2;
	}
	
	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
				
		return grpTitle[groupPosition];
	}

	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}



	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		GroupViewHolder holder = new GroupViewHolder();
		if (view == null)
		{

			holder = new GroupViewHolder();
			view = inflater.inflate(R.layout.performance_wall_grp_list_item, parent, false);
			holder.txtView_grpTitle = (TextView) view.findViewById(R.id.pwGrpTitle);
		
			view.setTag(holder);
		}
		else
		{
			holder = (GroupViewHolder) view.getTag();
		}
						
		
		holder.txtView_grpTitle.setText("" +grpTitle[groupPosition]);
		if(!isExpanded)
		{if(groupPosition %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		else
		{view.setBackgroundColor(Color.WHITE);}
		}
				
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ChildViewHolder holder = new ChildViewHolder();
		if (view == null)
		{

			holder = new ChildViewHolder();
			view = inflater.inflate(R.layout.performance_wall_child_list_item, parent, false);
			holder.txtView_que = (TextView) view.findViewById(R.id.pwChildTitle);
			holder.txtView_ans = (TextView) view.findViewById(R.id.pwChildHq);
		
			view.setTag(holder);
		}
		else
		{
			holder = (ChildViewHolder) view.getTag();
		}
		
try {
			if(groupPosition == 0)
			{
				JSONObject courseJsonObj = (JSONObject) coursesJsonArray.get(childPosition);
				//course Status
				holder.txtView_que.setText(""+courseJsonObj.getString("course"));
				holder.txtView_ans.setText(""+courseJsonObj.getString("Status"));
			}
			else if(groupPosition == 1)
			{
				JSONObject assesmentJsonObj = (JSONObject) assesmenJsonArray.get(childPosition);
				holder.txtView_que.setText(""+assesmentJsonObj.getString("assesment"));
				holder.txtView_ans.setText(""+assesmentJsonObj.getString("Status"));
			}
			else if(groupPosition == 2)
			{
				JSONObject wocktubeJsonObj = (JSONObject) wocktubeJsonArray.get(childPosition);
				holder.txtView_que.setText(""+wocktubeJsonObj.getString("wocktube"));
				holder.txtView_ans.setText(""+wocktubeJsonObj.getString("Status"));
			}
				
		
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String getName(int childPosition, JSONObject performanceObj) throws org.json.JSONException {
		switch (childPosition) {
		case 0:

			return performanceObj.getString("emp1");
		case 1:

			return performanceObj.getString("emp2");
		case 2:

			return performanceObj.getString("emp3");
		case 3:

			return performanceObj.getString("emp4");
		case 4:

			return performanceObj.getString("emp5");
		case 5:

			return performanceObj.getString("emp6");
		case 6:

			return performanceObj.getString("emp7");
		case 7:

			return performanceObj.getString("emp8");
		case 8:

			return performanceObj.getString("emp9");
		case 9:

			return performanceObj.getString("emp10");

		default:
			break;
		}
		return "";
	}
	
	public String getHeadqtr(int childPosition, JSONObject performanceObj) throws org.json.JSONException {
		switch (childPosition) {
		case 0:

			return performanceObj.getString("hq1");
		case 1:

			return performanceObj.getString("hq2");
		case 2:

			return performanceObj.getString("hq3");
		case 3:

			return performanceObj.getString("hq4");
		case 4:

			return performanceObj.getString("hq5");
		case 5:

			return performanceObj.getString("hq6");
		case 6:

			return performanceObj.getString("hq7");
		case 7:

			return performanceObj.getString("hq8");
		case 8:

			return performanceObj.getString("hq9");
		case 9:

			return performanceObj.getString("hq10");

		default:
			break;
		}
		return "";
	}
	
	

}
