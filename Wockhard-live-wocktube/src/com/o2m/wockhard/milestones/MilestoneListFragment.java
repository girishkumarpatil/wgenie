package com.o2m.wockhard.milestones;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.BaseActivity;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.o2m.wockhard.faqs.FaqListFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MilestoneListFragment extends Fragment implements OnItemClickListener {

	protected static final String TAG = "MilestoneListFragment";
	private ListView milestoneListView;
	private JSONArray msgList;
	 private String[] mMilestoneTitles;
	 private URLParams urlParameter  = URLParams.getInstance();
	 
	 public MilestoneListFragment() {
         // Empty constructor required for fragment subclasses
		 
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.fragment_milestone, container, false);
         milestoneListView = (ListView) rootView.findViewById(R.id.milestoneList);
         mMilestoneTitles = getResources().getStringArray(R.array.Milestones_array);
         MilestoneAdapter inboxMsgAdapter = new MilestoneAdapter(this.getActivity(), mMilestoneTitles);
         milestoneListView.setAdapter(inboxMsgAdapter);
         //mPlanetTitles = getResources().getStringArray(R.array.planets_array);
	        // set up the drawer's list view with items and click listener
       //  msgListView.setAdapter(new ArrayAdapter<String>(this.getActivity(),R.layout.drawer_list_item, mPlanetTitles));
         
         milestoneListView.setOnItemClickListener(this);
         /*
         int i = getArguments().getInt(ARG_PLANET_NUMBER);
         String planet = getResources().getStringArray(R.array.planets_array)[i];

         int imageId = getResources().getIdentifier(planet.toLowerCase(Locale.getDefault()),
                         "drawable", getActivity().getPackageName());
         ((ImageView) rootView.findViewById(R.id.image)).setImageResource(imageId);
         getActivity().setTitle(planet);
         */
         return rootView;
     }

	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("MessageListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		
		
		if(position == 0)
		{
			this.loadCurriculumAssistance();
		}
		else if(position == 1)
		{
			this.loadIndividualAssessment();
		}
		else if(position == 2)
		{
			//this.loadIndividualAssessment();
			//
			this.loadCourseAssessment();
		}
		else if(position == 3)
		{
			
			//misc
			this.loadMISCAssessment();
		}
		else if(position == 4)
		{
		//	this.loadIndividualAssessment();
			//wocktube
			this.loadWocktubeAssessment();
		}
		/*try {
			JSONObject msgObj = (JSONObject) msgList.get(position);
			
			Fragment fragment = new MessageDetailsFragment(msgObj);

	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().addToBackStack("vg");
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		

		
	}

	
	private void loadCurriculumAssistance()
	{
		 new ApplicationAsk(getActivity(), new ApplicationService() {
				
			 private String resp;
			 private JSONArray faqArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute inbox Response : " + resp);
				  
				  try {
					  
					  
					JSONObject caResponseObj = new JSONObject(resp);
					
					Log.v(TAG," onPostExecute Status : " + caResponseObj.getString("d"));
					
					JSONObject caResponseObj1 = new JSONObject(caResponseObj.getString("d"));
					
					Log.v(TAG," onPostExecute message : " +  caResponseObj1.getString("Status"));
					
					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table");
					
					if(faqArray.length() > 0)
					{
						 Fragment fragment = new CurriculumAssistanceFragment(faqArray);  
						 FragmentManager fragmentManager = getFragmentManager();
						 FragmentTransaction ft = fragmentManager.beginTransaction();
						 ft.addToBackStack("CurriculumAssistanceFragment");
					     ft.replace(R.id.content_frame, fragment).commit();
						
					}
					else
					{
						Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_LONG).show();
					}
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				  
				 

//			        // update selected item and title, then close the drawer
//			        mDrawerList.setItemChecked(screenPosition, true);
//			        setTitle(mMenuTitles[screenPosition]);
//			        mDrawerLayout.closeDrawer(mDrawerList);
				
			}
			
			@Override
			public void execute() {
				JSONObject caRequestObj = new JSONObject();
		        try {
					caRequestObj.putOpt("p_div_code", urlParameter.getDiv_code());
					caRequestObj.putOpt("p_level_code", urlParameter.getLevel_code());
					caRequestObj.putOpt("p_doj", urlParameter.getDoj());
					caRequestObj.putOpt("p_employee_id", urlParameter.getEmployee_id());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String caURL = "http://wockhardt.oh22media.com/myservice/service1.asmx//App_MilestoneAllCertificate";
		        String caURL = URLGenerator.getCurriculumAssistance();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(caURL, caRequestObj.toString());
		  
		 
				
			}
		}).execute();
	}
	
	private void loadIndividualAssessment()
	{
		 new ApplicationAsk(getActivity(), new ApplicationService() {
				
			 private String resp;
			 private JSONArray faqArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute inbox Response : " + resp);
				  
				  try {
					  
					  
					JSONObject iaResponseObj = new JSONObject(resp);
					Log.v(TAG," onPostExecute Status : " + iaResponseObj.getString("d"));
					JSONObject iaResponseObj1 = new JSONObject(iaResponseObj.getString("d"));
					JSONObject dataObj = iaResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table1");
					
					if(faqArray.length() > 0)
					{
						String[] mTitle = new String[faqArray.length()] ; 
						for(int index = 0 ; index < faqArray.length(); index++)
						{
							JSONObject iaJObj = faqArray.getJSONObject(index);
							mTitle[index] = iaJObj.getString("Assessment");
						}
						
						 Fragment fragment = new AssessmentListFragment("Individual", mTitle);  
						  FragmentManager fragmentManager = getFragmentManager();
					      FragmentTransaction ft = fragmentManager.beginTransaction();
					      ft.addToBackStack("IndividualAssesment");
					      ft.replace(R.id.content_frame, fragment).commit();
						
					}
					else
					{
						Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_LONG).show();
					}
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				  
				 

				
			}
			
			@Override
			public void execute() {
				JSONObject iARequestObj = new JSONObject();
		        try {
					/*iARequestObj.putOpt("p_div_code", "E");
					iARequestObj.putOpt("p_desg_level", "L1");
					iARequestObj.putOpt("p_zone_code", "OWP");
					iARequestObj.putOpt("p_region_code", "OWPPU");
					iARequestObj.putOpt("p_old_user_id", "41");*/
		        	
		        	iARequestObj.putOpt("p_div_code", urlParameter.getDiv_code());
					iARequestObj.putOpt("p_desg_level", urlParameter.getLevel_code());
					iARequestObj.putOpt("p_zone_code", urlParameter.getZone_code());
					iARequestObj.putOpt("p_region_code", urlParameter.getRegion_code());
					iARequestObj.putOpt("p_old_user_id", urlParameter.getUser_id());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String iAURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/Milestone_PendingIndividualAsst";
		        String iAURL = URLGenerator.getPendingAssistance();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(iAURL, iARequestObj.toString());
		  
		 
				
			}
		}).execute();
	}
	
	private void loadCourseAssessment()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			
			 private String resp;
			 private JSONArray faqArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute inbox Response : " + resp);
				  
				  try {
					  
					  
					JSONObject iaResponseObj = new JSONObject(resp);
					Log.v(TAG," onPostExecute Status : " + iaResponseObj.getString("d"));
					JSONObject iaResponseObj1 = new JSONObject(iaResponseObj.getString("d"));
					JSONObject dataObj = iaResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table1");
					
					if(faqArray.length() > 0)
					{
						String[] mTitle = new String[faqArray.length()] ; 
						for(int index = 0 ; index < faqArray.length(); index++)
						{
							JSONObject iaJObj = faqArray.getJSONObject(index);
							mTitle[index] = iaJObj.getString("Assessment");
						}
						
						 Fragment fragment = new AssessmentListFragment("Course Assesment", mTitle);  
						  FragmentManager fragmentManager = getFragmentManager();
					      FragmentTransaction ft = fragmentManager.beginTransaction();
					      ft.addToBackStack("CourseAssesment");
					      ft.replace(R.id.content_frame, fragment).commit();
						
					}
					else
					{
						Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_LONG).show();
					}
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				  
				 

				
			}
			
			@Override
			public void execute() {
				JSONObject cARequestObj = new JSONObject();
		        try {
					/*cARequestObj.putOpt("p_div_code", "E");
					cARequestObj.putOpt("p_desg_level", "L1");
					cARequestObj.putOpt("p_zone_code", "OWP");
					cARequestObj.putOpt("p_region_code", "OWPPU");
					cARequestObj.putOpt("p_old_user_id", "41");
					cARequestObj.putOpt("p_user_id", "2");*/
		        	
		        	cARequestObj.putOpt("p_div_code", urlParameter.getDiv_code());
					cARequestObj.putOpt("p_desg_level", urlParameter.getLevel_code());
					cARequestObj.putOpt("p_zone_code", urlParameter.getZone_code());
					cARequestObj.putOpt("p_region_code", urlParameter.getRegion_code());
					cARequestObj.putOpt("p_old_user_id", urlParameter.getUser_id());
					cARequestObj.putOpt("p_user_id", urlParameter.getUser_id());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String cAURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/Milestone_PendingCourseAsst";
		        String cAURL = URLGenerator.getMilestone_PendingCourseAsst();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(cAURL, cARequestObj.toString());
		  
		 
				
			}
		}).execute();
	}
	
	private void loadMISCAssessment()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			
			 private String resp;
			 private JSONArray faqArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute Response : " + resp);
				  
				  try {
					  
					  
					JSONObject iaResponseObj = new JSONObject(resp);
					Log.v(TAG," onPostExecute Status : " + iaResponseObj.getString("d"));
					JSONObject iaResponseObj1 = new JSONObject(iaResponseObj.getString("d"));
					JSONObject dataObj = iaResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table1");
					
					if(faqArray.length() > 0)
					{
						String[] mTitle = new String[faqArray.length()] ; 
						for(int index = 0 ; index < faqArray.length(); index++)
						{
							JSONObject iaJObj = faqArray.getJSONObject(index);
							mTitle[index] = iaJObj.getString("Assessment");
						}
						
						 Fragment fragment = new AssessmentListFragment("MISC Assesment", mTitle);  
						  FragmentManager fragmentManager = getFragmentManager();
					      FragmentTransaction ft = fragmentManager.beginTransaction();
					      ft.addToBackStack("MISCAssesment");
					      ft.replace(R.id.content_frame, fragment).commit();
						
					}
					else
					{
						Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_LONG).show();
					}
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				  
				 

				
			}
			
			@Override
			public void execute() {
				JSONObject miscRequestObj = new JSONObject();
		        try {
					//miscRequestObj.putOpt("p_employee_id", "8081");
		        	miscRequestObj.putOpt("p_employee_id", urlParameter.getEmployee_id());
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String miscURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/Milestone_PendingMISCAsst";
		        String miscURL = URLGenerator.getMilestone_PendingMISCAsst();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(miscURL, miscRequestObj.toString());
		  
		 
				
			}
		}).execute();
		
	}
	
	private void loadWocktubeAssessment()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			
			 private String resp;
			 private JSONArray faqArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute inbox Response : " + resp);
				  
				  try {
					  
					  
					JSONObject iaResponseObj = new JSONObject(resp);
					Log.v(TAG," onPostExecute Status : " + iaResponseObj.getString("d"));
					JSONObject iaResponseObj1 = new JSONObject(iaResponseObj.getString("d"));
					JSONObject dataObj = iaResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table1");
					
					if(faqArray.length() > 0)
					{
						String[] mTitle = new String[faqArray.length()] ; 
						for(int index = 0 ; index < faqArray.length(); index++)
						{
							JSONObject iaJObj = faqArray.getJSONObject(index);
							mTitle[index] = iaJObj.getString("Assessment");
						}
						
						 Fragment fragment = new AssessmentListFragment("Wocktube Assesment", mTitle);  
						  FragmentManager fragmentManager = getFragmentManager();
					      FragmentTransaction ft = fragmentManager.beginTransaction();
					      ft.addToBackStack("WocktubeAssesment");
					      ft.replace(R.id.content_frame, fragment).commit();
						
					}
					else
					{
						Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_LONG).show();
					}
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				  
				 

				
			}
			
			@Override
			public void execute() {
				JSONObject wARequestObj = new JSONObject();
		        try {
					/*wARequestObj.putOpt("p_div_code", "E");
					wARequestObj.putOpt("p_desg_level", "L1");
					wARequestObj.putOpt("p_zone_code", "OWP");
					wARequestObj.putOpt("p_region_code", "OWPPU");
					wARequestObj.putOpt("p_old_user_id", "41");
					wARequestObj.putOpt("p_user_id", "2");*/
					
		        	wARequestObj.putOpt("p_div_code", urlParameter.getDiv_code());
		        	wARequestObj.putOpt("p_desg_level", urlParameter.getLevel_code());
		        	wARequestObj.putOpt("p_zone_code", urlParameter.getZone_code());
		        	wARequestObj.putOpt("p_region_code", urlParameter.getRegion_code());
		        	wARequestObj.putOpt("p_old_user_id", urlParameter.getUser_id());
		        	wARequestObj.putOpt("p_user_id", urlParameter.getUser_id());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String wAURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/Milestone_PendingWocktubeAsst";
		        String wAURL = URLGenerator.getMilestone_PendingWocktubeAsst();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(wAURL, wARequestObj.toString());
		  
		 
				
			}
		}).execute();
	}
	
	
}
