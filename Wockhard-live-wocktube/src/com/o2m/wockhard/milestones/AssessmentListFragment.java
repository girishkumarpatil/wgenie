package com.o2m.wockhard.milestones;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.BaseActivity;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.faqs.FaqListFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AssessmentListFragment extends Fragment implements OnItemClickListener {

	protected static final String TAG = "AssessmentListFragment";
	private ListView assessmentListView;
	private JSONArray msgList;
	 private String[] assesmentTitles;
	 private String assesmentType;
	 
	 public AssessmentListFragment(String _assesmentType, String[] _assesmentTitles) {
         
		 assesmentTitles = _assesmentTitles;
		 assesmentType = _assesmentType;
		 
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         
    	 View rootView = inflater.inflate(R.layout.fragment_assesment, container, false);
         assessmentListView = (ListView) rootView.findViewById(R.id.assesmentList);
         TextView titleTxtView= (TextView) rootView.findViewById(R.id.assesmentTitle);
         titleTxtView.setText(assesmentType);
        // assesmentTitles = getResources().getStringArray(R.array.Milestones_array);
         AssesmentAdapter assmntAdapter = new AssesmentAdapter(this.getActivity(), assesmentTitles);
         assessmentListView.setAdapter(assmntAdapter);
         assessmentListView.setOnItemClickListener(this);
      
         return rootView;
     }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

//	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
//		
//		Log.v(TAG, "position"+ position);
//		
//		Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
//		
//		if(position == 0)
//		{
//			this.loadCurriculumAssistance();
//		}
//		else if(position == 1)
//		{
//			
//		}
//		
//
//		
//	}
//
	
	
	
	
}
