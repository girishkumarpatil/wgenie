package com.o2m.wockhard;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.o2m.wockhard.HomeActivity.Screen;
import com.o2m.wockhard.jshandler.JsHandler;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LoadWebPageFragment extends Fragment {

	private WebView myBrowser;
	private int screenName;

	
	private String asstId; 
	private String attempts; 
	private String duration;
	private String marks;
	private String asstDivId;
	private String passPercent;
	
	
	public LoadWebPageFragment() {

	}
	
	public LoadWebPageFragment(String asstId, String asstDivId, String attempts, String duration, String marks, String passPercent) {

		
		this.asstId = asstId;
		this.attempts = attempts;
		this.duration = duration;
		this.marks = marks;
		this.asstDivId = asstDivId;
		this.passPercent = passPercent;
		
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_offline_web , container, false);

		myBrowser = (WebView) rootView.findViewById(R.id.mybrowser);
		myBrowser.setBackgroundColor(Color.TRANSPARENT);
		//myBrowser.getSettings().setc
		//myBrowser.getSettings().setJavaScriptEnabled(true); 
		myBrowser.getSettings().setJavaScriptEnabled(true); // enable javascript
		
		myBrowser.getSettings().setDomStorageEnabled(true);
	        String databasePath = this.getActivity().getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath(); 
	        myBrowser.getSettings().setDatabasePath(databasePath);
		if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) 
			myBrowser.getSettings().setAllowUniversalAccessFromFileURLs(true);
		
		JsHandler _jsHandler = new JsHandler(this.getActivity(), myBrowser);		
		myBrowser.addJavascriptInterface(_jsHandler, "JsHandler");
		
		myBrowser.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
		                //Required functionality here
		                return super.onJsAlert(view, url, message, result);
		       }});
		
		
		this.loadRespectiveHtml();

		return rootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		myBrowser.onPause();
		super.onPause();
	}
	

	public void setScreenName(int screen) {
		this.screenName = screen;
	}

	private void loadRespectiveHtml() {

		 URLParams params = URLParams.getInstance();
		 String levelCode = params.getLevel_code();
		 String loginLevelCode = params.getLogin_level_code();
		
		if(this.screenName == Screen.MyLearning.ordinal())
		{
			myBrowser.loadUrl("file:///android_asset/www/index.html");
			//myBrowser.loadUrl("file:///android_asset/wocktube/index.html");

		}
		else if(this.screenName == Screen.Wow.ordinal())
		{
			myBrowser.loadUrl("file:///android_asset/feedback/feedback_wow.html");
			
		}
		else if(this.screenName == Screen.Feedback.ordinal())
		{
			myBrowser.loadUrl("file:///android_asset/feedback/feedback_wow.html");
		}
		else if(this.screenName == Screen.Training.ordinal())
		{
			if(loginLevelCode.equalsIgnoreCase("L1") || loginLevelCode.equalsIgnoreCase("L2") || loginLevelCode.equalsIgnoreCase("L3") )
				myBrowser.loadUrl("file:///android_asset/training/training_path_user.html");
			else if(loginLevelCode.equalsIgnoreCase("L4") || loginLevelCode.equalsIgnoreCase("L5") )
				myBrowser.loadUrl("file:///android_asset/training/training_path_admin.html");
		}
		else if(this.screenName == Screen.Avatar.ordinal())
		{
			if(loginLevelCode.equalsIgnoreCase("L1") || loginLevelCode.equalsIgnoreCase("L2") || loginLevelCode.equalsIgnoreCase("L3") )
				myBrowser.loadUrl("file:///android_asset/avatar/avatar_quarter_user.html");
			else if(loginLevelCode.equalsIgnoreCase("L4") || loginLevelCode.equalsIgnoreCase("L5") )
				myBrowser.loadUrl("file:///android_asset/avatar/avatar_quarter.html");
		}
		else if(this.screenName == Screen.Wocktube.ordinal())
		{
			
			myBrowser.loadUrl("file:///android_asset/wocktube/index.html");

		}
		else if(this.screenName == Screen.Library.ordinal())
		{
			
			myBrowser.loadUrl("file:///android_asset/library/library.html");
			//http://training.wockhardt.com/my_library/50fbfbbb-3edb-46bd-ba60-6ba6f17ab101.pdf
			//myBrowser.loadUrl("http://training.wockhardt.com/my_library/50fbfbbb-3edb-46bd-ba60-6ba6f17ab101.pdf");
		}
		else if(this.screenName == Screen.INTERACTIVE.ordinal())
		{
			
			myBrowser.loadUrl("file:///android_asset/interactive/index.html");
			//myBrowser.loadUrl("http://oh22works.com/wgenie/assessment/index.html");
		
		}
		else if(this.screenName == Screen.PerformanceWall.ordinal())
		{
			
			myBrowser.loadUrl("file:///android_asset/interactive/leaderboard.html");
			//myBrowser.loadUrl("http://oh22works.com/wgenie/assessment/index.html");
		
		}

		
		myBrowser.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			     
				   URLParams params = URLParams.getInstance();
					 //  loggedIn("4570", "6224", "B", "L1", "r1", "z1", "815330"); 
					 //  Function name loggedIn(UserId, oldUserId, divId, levelCode, regionCode, zoneCode, employeeId)
					   
					   String UserId = params.getUser_id();
					   //String UserId = params.get;
					   String oldUserId = params.getOld_user_id();
					   String divId = params.getDiv_code();
					   String levelCode = params.getLevel_code();
					   String loginLevelCode = params.getLogin_level_code();
					   String regionCode = params.getRegion_code();
					   String zoneCode = params.getZone_code();
					   String employeeId = params.getEmployee_id();
					   String doj = params.getDoj();
					   String userName = params.getName();
					   Log.v("WEBACtivity", "JS Call");
					   
					   
					   if(LoadWebPageFragment.this.screenName == Screen.MyLearning.ordinal())
					   {
						   String cat = "#category";
						   view.loadUrl("javascript:loggedIn(\""+UserId+"\", \""+oldUserId+"\", \""+divId+"\",\""+levelCode+"\",\""+regionCode+"\",\""+zoneCode+"\", \""+employeeId+"\")");
						  // view.loadUrl("javascript:getCategory(\""+divId+"\", \""+levelCode+"\", \""+cat+"\",\""+employeeId+"\",\""+regionCode+"\",\""+zoneCode+"\", \""+employeeId+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.Wow.ordinal())
					   {
						   view.loadUrl("javascript:getData(\""+employeeId+"\", \""+levelCode+"\", \""+doj+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.Feedback.ordinal())
					   {
						   view.loadUrl("javascript:getData(\""+employeeId+"\", \""+levelCode+"\", \""+doj+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.Training.ordinal())
					   {
						   
						   if(loginLevelCode.equalsIgnoreCase("L1") || loginLevelCode.equalsIgnoreCase("L2") || loginLevelCode.equalsIgnoreCase("L3") )
							   view.loadUrl("javascript:getTraining(\""+employeeId+"\", \""+divId+"\", \""+levelCode+"\",\""+userName+"\", \""+doj+"\")");
							   
							else if(loginLevelCode.equalsIgnoreCase("L4") || loginLevelCode.equalsIgnoreCase("L5") )
								view.loadUrl("javascript:getData(\""+employeeId+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.Avatar.ordinal())
					   {			   
						   if(loginLevelCode.equalsIgnoreCase("L1") || loginLevelCode.equalsIgnoreCase("L2") || loginLevelCode.equalsIgnoreCase("L3") )
							   view.loadUrl("javascript:loadData(\""+employeeId+"\", \""+divId+"\", \""+levelCode+"\")");
							else if(loginLevelCode.equalsIgnoreCase("L4") || loginLevelCode.equalsIgnoreCase("L5") )
								view.loadUrl("javascript:getData(\""+employeeId+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.Wocktube.ordinal())
					   {
						   String cat = "#category";
						   //view.loadUrl("javascript:loggedIn(\""+UserId+"\", \""+oldUserId+"\", \""+divId+"\",\""+levelCode+"\",\""+regionCode+"\",\""+zoneCode+"\", \""+employeeId+"\")");
						   view.loadUrl("javascript:getCategory(\""+divId+"\", \""+levelCode+"\", \""+cat+"\",\""+employeeId+"\",\""+regionCode+"\",\""+zoneCode+"\", \""+employeeId+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.Library.ordinal())
					   {
						 //getLibrary(divCode, userId, levelCode, empId)
						   view.loadUrl("javascript:getLibrary(\""+divId+"\", \""+UserId+"\", \""+levelCode+"\",\""+employeeId+"\")");
					   }
					   else if(LoadWebPageFragment.this.screenName == Screen.PerformanceWall.ordinal())
						{
						   /*divId = "E";
						   UserId = "2";
						   levelCode = "L1";
						   employeeId = "8081";*/
						   view.loadUrl("javascript:showLeaderBoard(\""+divId+"\", \""+UserId+"\", \""+levelCode+"\",\""+employeeId+"\")");
						}
					   //INTERACTIVE
					   else if(LoadWebPageFragment.this.screenName == Screen.INTERACTIVE.ordinal())
					   {
			
						   String asstId = LoadWebPageFragment.this.asstId;
						   String attempts = LoadWebPageFragment.this.attempts;
						   String duration = LoadWebPageFragment.this.duration;
						   String marks = LoadWebPageFragment.this.marks;
						   String asstDivId = LoadWebPageFragment.this.asstDivId;
						   String passPercent = LoadWebPageFragment.this.passPercent;
						   
						   Log.v("", "LoadWebPageFragment.this.asstId" + LoadWebPageFragment.this.asstId);
						   Log.v("", "LoadWebPageFragment.this.attempts" + LoadWebPageFragment.this.attempts);
						   Log.v("", "LoadWebPageFragment.this.duration" + LoadWebPageFragment.this.duration);
						   Log.v("", "LoadWebPageFragment.this.marks" + LoadWebPageFragment.this.marks);
						   /*divId = "E";
						   UserId = "2";
						   levelCode = "L1";
						   employeeId = "8081";
						   asstId = "17";
						   asstDivId = "1";
						   attempts = "0";
						   duration = "10";
						   marks = "100";
						   passPercent = "80";
*/						   //view.loadUrl("javascript:showAssesment(\""+divId+"\", \""+UserId+"\", \""+levelCode+"\",\""+employeeId+"\", \""+asstId+"\", \""+asstDivId+"\", \""+attempts+"\", \""+duration+"\", \""+marks+"\", \""+passPercent+"\")");
						   view.loadUrl("javascript:showAssesment(\""+divId+"\", \""+UserId+"\", \""+levelCode+"\",\""+employeeId+"\", \""+asstId+"\", \""+asstDivId+"\", \""+attempts+"\", \""+duration+"\", \""+marks+"\", \""+passPercent+"\")");
						  }
					   
					   
			    }
			});

			
			
				

	}


}
