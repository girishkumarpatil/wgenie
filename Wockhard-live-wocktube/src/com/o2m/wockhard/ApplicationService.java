package com.o2m.wockhard;

public interface ApplicationService
{
	public void execute();

	public void postExecute();
}
