package com.o2m.wockhard.inbox;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class MessageAdapter extends BaseAdapter
{

	Context context;

	ArrayList<String> arrayMessages;

	JSONArray msgJsonArray;
	String unreadCount;

	//private ChannelDetails channelDetails;

	//public MessageAdapter(Context context, ArrayList<String> _arrayMessages)
	public MessageAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.msgJsonArray = _arrayMessages;
	}

	@Override
	public int getCount()
	{

		return msgJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return msgJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();
//			view = inflater.inflate(R.layout.channel_list, parent, false);
			view = inflater.inflate(R.layout.inbox_list_item, parent, false);
			holder.txtView_msgTitle = (TextView) view.findViewById(R.id.msgTitle);
			holder.txtView_msgBy = (TextView)view.findViewById(R.id.msgBy);
			//holder.attachment = (View)view.findViewById(R.id.) ;
			holder.msgDate = (TextView)view.findViewById(R.id.msgDaTE) ;
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
//		holder.txtView_msgTitle.setText("Message Title : " + position);
//		holder.txtView_msgBy.setText("Creator : " + position);
//		holder.msgDate.setText("date : 26 Jan 2015 15:15 "+ position);
		
		
		try {
			JSONObject msgObj = (JSONObject) msgJsonArray.get(position);
			
			holder.txtView_msgTitle.setText("" + msgObj.getString("msg_sub"));
			
			holder.txtView_msgBy.setText("" + msgObj.getString("name"));
			holder.msgDate.setText(""+ msgObj.getString("display_date"));
//			holder.txtView_msgBy.setText("" + msgObj.getString("created_by"));
//			holder.msgDate.setText(""+ msgObj.getString("msg_date"));
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		/*
		
		try
		{
			String channel = arrayMessages.get(position);
			try
			{
				channelDetails = new ChannelDetails(channel);
				ContentReadUnreadStatus contentReadStatus = ContentReadUnreadStatus.getInstance(context);
				unreadCount = contentReadStatus.getUnreadContentCount(channelDetails.getID());

			}
			catch (JSONException e)
			{

				e.printStackTrace();
			}
			if(unreadCount.length() < 1)
			{
				holder.attachment.setVisibility(View.INVISIBLE) ;
				holder.txtView_msgBy.setVisibility(View.INVISIBLE) ;
			}
			else
			{
				holder.attachment.setVisibility(View.VISIBLE) ;
				holder.txtView_msgBy.setVisibility(View.VISIBLE) ;
				holder.txtView_msgBy.setText(unreadCount) ;
			}
			
			String channelName = channelDetails.getName() ;
					
			holder.txtView_msgTitle.setText(channelName);
			holder.txtView_msgTitle.setSelected(true) ;
			holder.msgDate.setImageResource(R.drawable.channel_grid_icon) ;
			
		}
		
		
		catch (Exception e)
		{
			e.printStackTrace();
		}
		*/
		
		if(position %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_msgTitle;
		public TextView txtView_msgBy ;
		public ImageButton attachment ;
		public TextView msgDate ;

	}

}
