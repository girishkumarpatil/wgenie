package com.o2m.wockhard.inbox;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MessageListFragment extends Fragment implements OnItemClickListener {

	private ListView msgListView;
	private JSONArray msgList;
	private String currentMessageCategory = "Inbox";
	private TextView messageTitleTxtV;
	
	 public MessageListFragment(JSONArray _msgList) {
         // Empty constructor required for fragment subclasses
		 this.msgList = _msgList;
     }
	 
	 public MessageListFragment(JSONArray _msgList, String messageCategory) {
         // Empty constructor required for fragment subclasses
		 this.msgList = _msgList;
		 this.currentMessageCategory = messageCategory;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         
    	 View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
         msgListView = (ListView) rootView.findViewById(R.id.inboxList);
         messageTitleTxtV = (TextView) rootView.findViewById(R.id.messageTitle);
         messageTitleTxtV.setText(currentMessageCategory);
         
         MessageAdapter inboxMsgAdapter = new MessageAdapter(this.getActivity(), msgList);
         msgListView.setAdapter(inboxMsgAdapter);
                  
         msgListView.setOnItemClickListener(this);
         
         return rootView;
     }

	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("MessageListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		
		try {
			JSONObject msgObj = (JSONObject) msgList.get(position);
			
			Fragment fragment = new MessageDetailsFragment(msgObj, currentMessageCategory);
			
			FragmentTransaction ft = getFragmentManager().beginTransaction();
		//	ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,R.anim.slide_in_left, R.anim.slide_out_right) ;
			ft.replace(R.id.content_frame, fragment, "message");
			ft.addToBackStack("message");
			ft.commit();

	   /*     FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().addToBackStack("channel");
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, "channel").commit();*/
	        /*
	        android.support.v4.app.Fragment fragment = new ContentListFragment();
			bundle.putStringArrayList("contents", arrayContents);
			bundle.putString("category_id", details.getID());
			android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
			fragment.setArguments(bundle);
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,R.anim.slide_in_left, R.anim.slide_out_right) ;
			ft.replace(R.id.content_home, fragment, "channel");
			ft.addToBackStack("channel");
			ft.commit();
			*/
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}

	
	public String getInboxResponse() 
	{		
		InputStream input = null;

		try {

			input = this.getActivity().getAssets().open("message.json");

		} catch (IOException e) {

			e.printStackTrace();
		}


		byte[] buffer = null;
		int size;
		try {
			size = input.available();
			buffer = new byte[size];
			input.read(buffer);
			input.close();
		} catch (IOException e) {

			e.printStackTrace();
		}


		String jsonStr = new String(buffer);

		return jsonStr;
	}
	
	
}
