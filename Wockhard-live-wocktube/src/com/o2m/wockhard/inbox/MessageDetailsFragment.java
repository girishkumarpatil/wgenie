package com.o2m.wockhard.inbox;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.InboxActivity;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MessageDetailsFragment extends Fragment implements OnClickListener {

	private String TAG = "MessageDetailsFragment";
	private JSONObject msgObj;
//	private ImageButton impBtn, archievedBtn, deleteBtn;
	private Button btn1, btn2, btn3;
	private String msgId;
	private String currentMessageCategory = "Inbox";
	

	enum MessageMarkAs {
	    Inbox, Important, Deleted, Archive 
	};
	
	 public MessageDetailsFragment(JSONObject _msgObj) {
         // Empty constructor required for fragment subclasses
		 this.msgObj = _msgObj;
     }
	 
	 public MessageDetailsFragment(JSONObject _msgObj, String messageCategory) {
         // Empty constructor required for fragment subclasses
		 this.msgObj = _msgObj;
		 this.currentMessageCategory = messageCategory;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.fragment_msg_details, container, false);
        
         /*
         int i = getArguments().getInt(ARG_PLANET_NUMBER);
         String planet = getResources().getStringArray(R.array.planets_array)[i];

         int imageId = getResources().getIdentifier(planet.toLowerCase(Locale.getDefault()),
                         "drawable", getActivity().getPackageName());
         ((ImageView) rootView.findViewById(R.id.image)).setImageResource(imageId);
         getActivity().setTitle(planet);
         
         
         
         
         
         */
         
         TextView msgTitle = (TextView) rootView.findViewById(R.id.mdmsgTitle);
         TextView msgDate = (TextView) rootView.findViewById(R.id.mdmsgDate);
         TextView msgBy = (TextView) rootView.findViewById(R.id.mdmsgBy);
         TextView messageTitleTxtV = (TextView) rootView.findViewById(R.id.msgDetailTitle);
         messageTitleTxtV.setText(currentMessageCategory);
         WebView msgBody = (WebView) rootView.findViewById(R.id.mdmsgBody);
         btn1 = (Button) rootView.findViewById(R.id.mdmsImp);
         btn1.setOnClickListener(this);
         btn2 = (Button) rootView.findViewById(R.id.mdmsArchieve);
         btn2.setOnClickListener(this);
         btn3 = (Button) rootView.findViewById(R.id.mdmsdDelete);
         btn3.setOnClickListener(this);
         configureOptionBtns();
         
         msgBody.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
         
         msgBody.getSettings().setJavaScriptEnabled(true);
         

  try{
         msgTitle.setText(""+msgObj.getString("msg_sub"));
         msgDate.setText(""+msgObj.getString("display_date"));
         msgBy.setText(""+msgObj.getString("name"));
 
         msgId = msgObj.getString("msg_id");
         
         msgBody.loadData(msgObj.getString("msg_desc"), "text/html", "UTF-8");
         //msgBody.setText(""+msgObj.getString("msg_desc"));
     } catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
         return rootView;
     }

     
     private void configureOptionBtns()
     {
    	 if(this.currentMessageCategory.equalsIgnoreCase("inbox"))
    	 {
    		 btn1.setTag(MessageMarkAs.Important.ordinal());
    		 btn1.setBackgroundResource(R.drawable.message_fav);
    		 
    		 btn2.setTag(MessageMarkAs.Archive.ordinal());
    		 btn2.setBackgroundResource(R.drawable.message_archive);
    		 
    		 btn3.setTag(MessageMarkAs.Deleted.ordinal());
    		 btn3.setBackgroundResource(R.drawable.message_delete);
    	 }
    	 else if(this.currentMessageCategory.equalsIgnoreCase("important"))
    	 {
    		 btn1.setTag(MessageMarkAs.Inbox.ordinal());
    		 btn1.setBackgroundResource(R.drawable.message_inbox);
    		 
    		 
    		 btn2.setTag(MessageMarkAs.Archive.ordinal());
    		 btn2.setBackgroundResource(R.drawable.message_archive);
    		 
    		 btn3.setTag(MessageMarkAs.Deleted.ordinal());
    		 btn3.setBackgroundResource(R.drawable.message_delete);
    		 
    	 }
    	 else if(this.currentMessageCategory.equalsIgnoreCase("archieved"))
    	 {
    		 btn1.setTag(MessageMarkAs.Inbox.ordinal());
    		 btn1.setBackgroundResource(R.drawable.message_inbox);
    		 
    		 btn2.setTag(MessageMarkAs.Important.ordinal());
    		 btn2.setBackgroundResource(R.drawable.message_fav);
    		 
    		 btn3.setTag(MessageMarkAs.Deleted.ordinal());
    		 btn3.setBackgroundResource(R.drawable.message_delete);
    	 }
    	 else if(this.currentMessageCategory.equalsIgnoreCase("deleted"))
    	 {
    		 btn1.setTag(MessageMarkAs.Inbox.ordinal());
    		 btn1.setBackgroundResource(R.drawable.message_inbox);
    		 
    		 btn2.setTag(MessageMarkAs.Important.ordinal());
    		 btn2.setBackgroundResource(R.drawable.message_fav);
    		 
    		 btn3.setTag(MessageMarkAs.Archive.ordinal());
    		 btn3.setBackgroundResource(R.drawable.message_archive);
    	 }
     }
     
	@Override
	public void onClick(View v) {
		
		int action = (Integer) v.getTag();
		
		if(action == MessageMarkAs.Inbox.ordinal())
		{
			this.markMsgAs(MessageMarkAs.Inbox.ordinal());
		}
		else if(action == MessageMarkAs.Archive.ordinal())
		{
			this.markMsgAs(MessageMarkAs.Archive.ordinal());
		}
		else if(action == MessageMarkAs.Important.ordinal())
		{
			this.markMsgAs(MessageMarkAs.Important.ordinal());
		}
		else if(action == MessageMarkAs.Deleted.ordinal())
		{
			this.markMsgAs(MessageMarkAs.Deleted.ordinal());
		}
		
		
		
		/*
		
		if(v == impBtn)
		{
			this.markMsgAs(MessageMarkAs.Important.ordinal());
		}
		else if(v == archievedBtn)
		{
			this.markMsgAs(MessageMarkAs.Archive.ordinal());
		}
		else if(v == deleteBtn)
		{
			this.markMsgAs(MessageMarkAs.Deleted.ordinal());
		}
		*/
	}
	
	private void markMsgAs(final int selection)
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			
			 private String resp;
			 private JSONArray msgArray;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute Response : " + resp);
				  
				  try {
					  
					  
					JSONObject updateMsgResponseObj = new JSONObject(resp);
					Log.v(TAG," onPostExecute Status : " + updateMsgResponseObj.getString("d"));
					JSONObject updateMsgResponseObj1 = new JSONObject(updateMsgResponseObj.getString("d"));
					String status = updateMsgResponseObj1.getString("Status");
					Log.v(TAG," onPostExecute message : " +  status);
					
					
					if(status.equalsIgnoreCase("success"))
					{
						Toast.makeText(getActivity(), "Message Updated Successfully", Toast.LENGTH_LONG).show();
						getFragmentManager().popBackStackImmediate();
					}
					else
					{
						Toast.makeText(getActivity(), "Please Try again later", Toast.LENGTH_LONG).show();
					}
				
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  				
			}
			
			@Override
			public void execute() {
				JSONObject updateMsgRequestObj = new JSONObject();
		        try {
		        	
					updateMsgRequestObj.putOpt("p_msg_id", msgId);
					updateMsgRequestObj.putOpt("p_user_id", URLParams.getInstance().getUser_id());
					updateMsgRequestObj.putOpt("p_msg_flag", String.valueOf(selection));
					updateMsgRequestObj.putOpt("p_msg_view", "1");
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String updateMsgURL = URLGenerator.getUpdateMessageStatus();//"http://wockhardt.oh22media.com/myservice/service1.asmx/Update_Message_flag";
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(updateMsgURL, updateMsgRequestObj.toString());
		  
		  // return resp;
				
			}
		}).execute();
	}
	
}
