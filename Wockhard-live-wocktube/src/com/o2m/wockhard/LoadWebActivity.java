package com.o2m.wockhard;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LoadWebActivity extends Activity {
	
	private WebView mWebview ;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        
       // String url = getIntent().getExtras().getString("url");
        
        mWebview  = new WebView(this);

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.getSettings().setDomStorageEnabled(true);
        String databasePath = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath(); 
        mWebview.getSettings().setDatabasePath(databasePath);
//        mWebview.setWebChromeClient(new WebChromeClient() { 
//        public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) { 
//                quotaUpdater.updateQuota(5 * 1024 * 1024); 
//            } 
//        });
        
        final Activity activity = this;

        mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
               
            }
        });

       // mWebview .loadUrl("file:///android_asset/www/index.html");
        //http://training.wockhardt.com/mobile/html/
        mWebview .loadUrl("http://training.wockhardt.com/mobile/html/");
        setContentView(mWebview );
        
        
        mWebview.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			       
				   URLParams params = URLParams.getInstance();
				 //  loggedIn("4570", "6224", "B", "L1", "r1", "z1", "815330"); 
				 //  Function name loggedIn(UserId, oldUserId, divId, levelCode, regionCode, zoneCode, employeeId)
				   
				   String UserId = params.getUser_id();
				   String oldUserId = params.getOld_user_id();
				   String divId = params.getDiv_code();
				   String levelCode = params.getLevel_code();
				   String regionCode = params.getRegion_code();
				   String zoneCode = params.getZone_code();
				   String employeeId = params.getEmployee_id();
				   
				   Log.v("WEBACtivity", "JS Call");
				   view.loadUrl("javascript:loggedIn(\""+UserId+"\", \""+oldUserId+"\", \""+divId+"\",\""+levelCode+"\",\""+regionCode+"\",\""+zoneCode+"\", \""+employeeId+"\")");
					
				 
			    }
			});
        
        

    }


}
