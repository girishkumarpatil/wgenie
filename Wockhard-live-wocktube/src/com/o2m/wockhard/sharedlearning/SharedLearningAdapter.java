package com.o2m.wockhard.sharedlearning;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class SharedLearningAdapter extends BaseAdapter
{

	Context context;

	ArrayList<String> arrayMessages;

	JSONArray sLJsonArray;
	String unreadCount;

	//private ChannelDetails channelDetails;

	//public MessageAdapter(Context context, ArrayList<String> _arrayMessages)
	public SharedLearningAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.sLJsonArray = _arrayMessages;
	}

	@Override
	public int getCount()
	{

		return sLJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return sLJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();
//			view = inflater.inflate(R.layout.channel_list, parent, false);
			view = inflater.inflate(R.layout.shared_learn_list_item, parent, false);
			holder.txtView_slTitle = (TextView) view.findViewById(R.id.slTitle);
			holder.txtView_slDesc = (TextView)view.findViewById(R.id.slDesc);
			holder.txtView_slBy = (TextView)view.findViewById(R.id.slBy);
			holder.txtView_slLikes = (TextView)view.findViewById(R.id.slLikes);
			holder.txtView_slComments = (TextView)view.findViewById(R.id.slComments);
			holder.txtView_sldate = (TextView)view.findViewById(R.id.sldate);
			//holder.attachment = (View)view.findViewById(R.id.) ;
			//holder.msgDate = (TextView)view.findViewById(R.id.msgDaTE) ;
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
		holder.txtView_slTitle.setText("sl Title : " + position);
		holder.txtView_slDesc.setText("desc : " + position);
		holder.txtView_slBy.setText("Girish Patil "+ position);
		holder.txtView_slLikes.setText("Likes "+ position);
		holder.txtView_slComments.setText("Comments "+ position);
		
		
		try {
		
			JSONObject slObj = (JSONObject) sLJsonArray.get(position);
				
			holder.txtView_slTitle.setText(" " + slObj.getString("sl_sub"));
			holder.txtView_slDesc.setText(" " + slObj.getString("sl_desc"));
			holder.txtView_slBy.setText("Posted By : " + slObj.getString("name"));
			holder.txtView_slLikes.setText("Likes " + slObj.getString("like_count"));
			holder.txtView_slComments.setText("Comments "+  slObj.getString("comment_count"));
			holder.txtView_sldate.setText(""+  slObj.getString("display_date"));
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_slTitle;
		public TextView txtView_slDesc;
		public TextView txtView_slBy ;
		public TextView txtView_slLikes ;
		//public ImageButton attachment ;
		public TextView txtView_slComments ;
		public TextView txtView_sldate ;

	}

}
