package com.o2m.wockhard.sharedlearning;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.o2m.wockhard.R;
import com.o2m.wockhard.inbox.MessageDetailsFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SharedLearningListFragment extends Fragment implements
OnItemClickListener, OnItemSelectedListener, OnClickListener {

	private ListView shredLearnListView;
	private JSONArray sharedLearnList;
	JSONArray filteredSharedLearnList;
	private Button addSlBtn;

	public SharedLearningListFragment(JSONArray _msgList) {
		// Empty constructor required for fragment subclasses
		this.sharedLearnList = _msgList;
		this.filteredSharedLearnList = _msgList;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_shared_learning,
				container, false);
		shredLearnListView = (ListView) rootView.findViewById(R.id.slList);

		SharedLearningAdapter inboxMsgAdapter = new SharedLearningAdapter(
				this.getActivity(), sharedLearnList);
		shredLearnListView.setAdapter(inboxMsgAdapter);

		Spinner spinner = (Spinner) rootView
				.findViewById(R.id.slCategorySpinner);
		
		addSlBtn = (Button) rootView.findViewById(R.id.addSlBtn);
		addSlBtn.setOnClickListener(this);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this.getActivity(), R.array.sl_category_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);

		shredLearnListView.setOnItemClickListener(this);
		spinner.setOnItemSelectedListener(this);
		return rootView;
	}

	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {

		Log.v("SharedLearningListFragment", "position" + position);

		//Toast.makeText(this.getActivity(), "" + position, Toast.LENGTH_SHORT).show();

		try {
			JSONObject slObj = (JSONObject) filteredSharedLearnList.get(position);

			Fragment fragment = new SharedLearningDetailsFragment(slObj);

			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack("SharedLearningDetailsFragment");
			ft.replace(R.id.content_frame, fragment).commit();

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {

//		Toast.makeText(getActivity(), "onItemSelected " + pos,
//				Toast.LENGTH_SHORT).show();
		
		Log.d("SharedLearningList", "onItemSelected : " + pos);

		if (pos == 0) {
			SharedLearningAdapter inboxMsgAdapter = new SharedLearningAdapter(
					this.getActivity(), sharedLearnList);
			shredLearnListView.setAdapter(inboxMsgAdapter);
		} else {
			 filteredSharedLearnList = new JSONArray(); 

			for (int i = 0; i < this.sharedLearnList.length(); i++) {

				try {

					JSONObject slObj = (JSONObject) this.sharedLearnList.get(i);
					String category = slObj.getString("category_id");
					if (category.equalsIgnoreCase(String.valueOf(pos))) {
						filteredSharedLearnList.put(slObj);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

				if (filteredSharedLearnList.length() == 0) {
					
					Toast.makeText(getActivity(), "No Data Found ",
							Toast.LENGTH_SHORT).show();
					shredLearnListView.setAdapter(null);
					shredLearnListView.invalidateViews();

				} else {

					SharedLearningAdapter inboxMsgAdapter = new SharedLearningAdapter(
							this.getActivity(), filteredSharedLearnList);
					shredLearnListView.setAdapter(inboxMsgAdapter);
				}
			
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

		Toast.makeText(getActivity(), "onNothingSelected", Toast.LENGTH_SHORT)
		.show();
	}

	@Override
	public void onClick(View v) {

		Fragment fragment = new AddSharedLearning();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("AddSharedLearning");
		ft.replace(R.id.content_frame, fragment, "message");
		
		ft.commit();
		
	}

}
