package com.o2m.wockhard.sharedlearning;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.loadingmanager.callbackdownloadmanager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.twotoasters.android.horizontalimagescroller.widget.HorizontalImageScroller;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SharedLearningDetailsFragment extends Fragment implements OnClickListener,callbackdownloadmanager {

	private JSONObject slObj;
	private Button slPostComm, slLikeBtn, slPlayBtn;
	private EditText slCommDesc; 
	private ListView commentListView;
	private TextView likeCount, slVideoLink;
	private VideoView videoView;
	HorizontalImageScroller scroller;
	ProgressDialog pDialog;
	String videoLinkToBePlayed = "";
	Button leftBtn, rightBtn;
	int currentIndex = 0;
	ImageView img;
	ProgressBar bar;
	int totlImages = 0;
	RelativeLayout imagePanel;
	JSONArray imageArray;
	DisplayImageOptions options;
	private String TAG = "SharedLearningDetailsFragment";
	
	
	 public SharedLearningDetailsFragment(JSONObject _slObj) {
         // Empty constructor required for fragment subclasses
		 this.slObj = _slObj;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         
    	 View rootView = inflater.inflate(R.layout.fragment_sl_details, container, false);
    	 
         TextView slTitle = (TextView) rootView.findViewById(R.id.sldTitle);
         TextView slDesc = (TextView) rootView.findViewById(R.id.sldDesc);
         TextView slBy = (TextView) rootView.findViewById(R.id.sldBy);
         slPostComm = (Button) rootView.findViewById(R.id.sldPostComm);
         slLikeBtn = (Button) rootView.findViewById(R.id.sld_btn_like);
         slCommDesc = (EditText) rootView.findViewById(R.id.sldCommDesc);
         commentListView = (ListView) rootView.findViewById(R.id.slcommentList);
         likeCount = (TextView) rootView.findViewById(R.id.sldLikes);
         slVideoLink = (TextView) rootView.findViewById(R.id.sldVideoLink);
         videoView = (VideoView) rootView.findViewById(R.id.VideoView);
         slPlayBtn = (Button) rootView.findViewById(R.id.sldPlayVideo);
         slPlayBtn.setOnClickListener(this);
         imagePanel = (RelativeLayout) rootView.findViewById(R.id.imagrParent);
         img = (ImageView) rootView.findViewById(R.id.imageView);
 		
 		leftBtn = (Button) rootView.findViewById(R.id.leftBtn);
 		rightBtn = (Button) rootView.findViewById(R.id.rightBtn);
 		
 		leftBtn.setOnClickListener(this);
 		rightBtn.setOnClickListener(this);
 		
 		//bar = (ProgressBar) rootView.findViewById(R.id.bar);
 		
 
 		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this.getActivity()).build();
		ImageLoader.getInstance().init(config);
		 options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.loading1) // resource or drawable
		         .showImageForEmptyUri(R.drawable.ic_empty)
		         .showImageOnFail(R.drawable.ic_error)
		         .resetViewBeforeLoading(false)
		           .cacheInMemory(true).build();
         
         
         // if sl_type : 1 : video
         // if sl_type : 2 : multiple images
         
         
       
//         scroller = (HorizontalImageScroller) rootView.findViewById(R.id.my_horizontal_image_scroller);
//        HorizontalImageScroller testScroller = (HorizontalImageScroller) rootView.findViewById(R.id.my_horizontal_image_scroller3);
//         
//         
//         ArrayList<ImageToLoad> images1 = new ArrayList<ImageToLoad>();
//         for (int i=0; i <  5; i++)
//         {
//        	 /*JSONObject imgObj = imageArray.getJSONObject(i);
//        	 String imageTitle = imgObj.getString("img_title");
//        	 String url = imgObj.getString("img_file");
//        	 Log.v(TAG, "Title : " + imageTitle + "" + " \nimageLink : " + url);*/
//        	 ImageToLoadUrl downloadImg = new ImageToLoadUrl("http://www.diporental.com/wp-content/gallery/listcar/toyota-innova2012.jpg");
//             downloadImg.setCanCacheFile(true);
//             images1.add(downloadImg); 
//             // substitute some pretty picture you can stand to see 20 times in a list
//            images1.add(new ImageToLoadDrawableResource(R.drawable.home_casestudy)); // plug in some of your own drawables
//         }
//         HorizontalImageScrollerAdapter adapter1 = new HorizontalImageScrollerAdapter(this.getActivity(), images1);
//         adapter1.setImageSize(200);
//         testScroller.setAdapter(adapter1);   

         
        // scroller.setAdapter(new Ho)
         
         slPostComm.setOnClickListener(this);
         slLikeBtn.setOnClickListener(this);

  try{
	  
	  slTitle.setText(""+slObj.getString("sl_sub"));
	  slDesc.setText(""+slObj.getString("sl_desc"));
	  slBy.setText(""+slObj.getString("name"));
	  likeCount.setText(""+slObj.getString("like_count"));
	  
	  String slType = slObj.getString("sl_type");
	  
	  if(slType.equals("1"))//video
	  {
		  
		  //scroller.setVisibility(View.GONE);
		  imagePanel.setVisibility(View.GONE);
		  videoView.setVisibility(View.GONE);
		  String videoLink = slObj.getString("video_link");
		  Log.v(TAG, "video Type : " + videoLink);
		  videoLinkToBePlayed = videoLink;
		  slVideoLink.setText(""+videoLink);
		  //videoLink = "http://m.youtube.com/watch?v=l-SBR7p7K-M";
		  //videoLink = "http://www.jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v";
		  //this.loadAndPlayVideoFrom(videoLink);
	  }
	  else if(slType.equals("2"))// images
	  {
		  videoView.setVisibility(View.GONE);
		  slVideoLink.setVisibility(View.GONE);
		  slPlayBtn.setVisibility(View.GONE);
		  imageArray = slObj.getJSONArray("img_file");
		  Log.v(TAG, "image Type" + imageArray);
		  
		  totlImages = imageArray.length();
	 		
	 		this.loadImageForIndex(currentIndex);
		  
		  /*
		  ArrayList<ImageToLoad> images = new ArrayList<ImageToLoad>();
	         for (int i=0; i < imageArray.length() ; i++)
	         {
	        	 JSONObject imgObj = imageArray.getJSONObject(i);
	        	 String imageTitle = imgObj.getString("img_title");
	        	 String url = imgObj.getString("img_file");
	        	 Log.v(TAG, "Title : " + imageTitle + "" + " \nimageLink : " + url);
	        	 ImageToLoadUrl downloadImg = new ImageToLoadUrl(url);
                 downloadImg.setCanCacheFile(true);
	             images.add(downloadImg);
	             // substitute some pretty picture you can stand to see 20 times in a list
	            // images.add(new ImageToLoadDrawableResource(R.drawable.menu_faq)); // plug in some of your own drawables
	             images.add(new ImageToLoadDrawableResource(R.drawable.line)); // plug in some of your own drawables
	         }
	         HorizontalImageScrollerAdapter adapter = new HorizontalImageScrollerAdapter(this.getActivity(), images);
	         adapter.setImageSize(200);
	         scroller.setAdapter(adapter);   
		  */
	  }
	  else
	  {   
		  Log.v(TAG, "SL TYPE OTHER");
		  videoView.setVisibility(View.GONE);
		  //scroller.setVisibility(View.GONE);
		  imagePanel.setVisibility(View.GONE);
		  slVideoLink.setVisibility(View.GONE);
		  slPlayBtn.setVisibility(View.GONE);
	  }
	  
	  
	  
	  if(slObj.getString("like_id").trim().equals(""))
	  {
		 slLikeBtn.setEnabled(true); 
	  }
	  else
	  {
		  slLikeBtn.setEnabled(false);
	  }
	  
	  
	/*  if(slType.equals("1"))
	  {
		  //hide images, show video
	  }
	  else if(slType.equals("2"))
	  {
		  // hide video, show images
	  }
	  else
	  {
		  // hide both video and image scroller
	  }*/
	  
	  loadComments();
     } 
  
   catch (org.json.JSONException e) {
			
			e.printStackTrace();
		}
         
         return rootView;
     }

	private void loadAndPlayVideoFrom(String VideoURL) {
		// Create a progressbar
				pDialog = new ProgressDialog(getActivity());
				// Set progressbar title
				pDialog.setTitle(" Video Streaming ");
				// Set progressbar message
				pDialog.setMessage("Buffering..."+ VideoURL);
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				// Show progressbar
				pDialog.show();
		 
				try {
					// Start the MediaController
					MediaController mediacontroller = new MediaController(
							getActivity());
					mediacontroller.setAnchorView(videoView);
					// Get the URL from String VideoURL
					Uri video = Uri.parse(VideoURL);
					videoView.setMediaController(mediacontroller);
					videoView.setVideoURI(video);
		 
				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
		 
				videoView.requestFocus();
				videoView.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						pDialog.dismiss();
						videoView.start();
					}
				});
		 
			
		
	}

	@Override
	public void onClick(View view) {
		
		if(view == slPostComm)
		{
			if(slCommDesc.getText().toString().trim().equalsIgnoreCase(""))
			{
				Toast.makeText(getActivity(), "Please enter the comment", Toast.LENGTH_LONG).show();
			}
			else
			{
				//Toast.makeText(getActivity(), "Submission not implemented yet", Toast.LENGTH_LONG).show();
				this.submitComment(slCommDesc.getText().toString());
			}
		}
		else if(view == slLikeBtn)
		{
			this.submitLikeSL();
		}
		else if(view == slPlayBtn)
		{
			//Uri video = Uri.parse(videoLinkToBePlayed);
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(videoLinkToBePlayed));
			startActivity(i);
		}
		if(view == leftBtn)
		{
			currentIndex -- ;
			this.loadImageForIndex(currentIndex);
		}
		else if(view == rightBtn)
		{
			currentIndex++ ;
			this.loadImageForIndex(currentIndex);
			
		}
		
	}
	
	
	public void submitLikeSL()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csLikeResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csLikeResponseObj.getString("d"));
						
						JSONObject csLikeResponseObj1 = new JSONObject(csLikeResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csLikeResponseObj1.getString("Status"));
						
					// csF = new CaseStudyFragment(csResponseObj);
					 	if(csLikeResponseObj1.getString("Status").equalsIgnoreCase("Success")){
					 		slCommDesc.setText("");
					 	Toast.makeText(getActivity(), "Like Posted Successfully..", Toast.LENGTH_LONG).show();
					 		slLikeBtn.setEnabled(false);
					 		slLikeBtn.setClickable(false);
					 		
					 		increasetLikeCount();
					 	}
					 	else{
					 	Toast.makeText(getActivity(), "Error during Like posting", Toast.LENGTH_LONG).show();
					 	}
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject csAddLikeRequestObj = new JSONObject();
		        try {
					
		        	URLParams urlParameter = URLParams.getInstance();
					csAddLikeRequestObj.putOpt("p_sl_id",slObj.getString("sl_id"));
					csAddLikeRequestObj.putOpt("p_user_id",urlParameter.getUser_id());
							
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String csURL = URLGenerator.AddLikeCountForSL();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(csURL, csAddLikeRequestObj.toString());
		  
		 
				
			}
		}).execute();

		
	}
	
	private void increasetLikeCount()
	{
		String currentCount = (String) likeCount.getText();
		int cnt = Integer.parseInt(currentCount);
		cnt++;
		likeCount.setText(""+cnt);
	}
	
	
	public void submitComment(final String _comment)
	{
		 //submit comments
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csResponseObj.getString("d"));
						
						JSONObject csResponseObj1 = new JSONObject(csResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csResponseObj1.getString("Status"));
						
					// csF = new CaseStudyFragment(csResponseObj);
					 	if(csResponseObj1.getString("Status").equalsIgnoreCase("Success")){
					 		slCommDesc.setText("");
					 	Toast.makeText(getActivity(), "Comment Posted Successfully..", Toast.LENGTH_LONG).show();
					 	}
					 	else{
					 	Toast.makeText(getActivity(), "Error during Comment posting", Toast.LENGTH_LONG).show();
					 	}
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject csAddcommentRequestObj = new JSONObject();
		        try {
					
		        	URLParams urlParameter = URLParams.getInstance();
					csAddcommentRequestObj.putOpt("p_sl_id",slObj.getString("sl_id"));
					csAddcommentRequestObj.putOpt("p_comment_desc",_comment);
					csAddcommentRequestObj.putOpt("p_user_id",urlParameter.getUser_id());
							
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String csURL = URLGenerator.getAddCommentsForSharedLearning();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(csURL, csAddcommentRequestObj.toString());
		  
		 
				
			}
		}).execute();

		
		
	}
	
	
	private void loadComments()
	{
		//p_sl_id
		
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			 private JSONArray commentArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csCommentResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csCommentResponseObj.getString("d"));
						
						JSONObject csCommentResponseObj1 = new JSONObject(csCommentResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csCommentResponseObj1.getString("Status"));
						
					
					 	if(csCommentResponseObj1.getString("Status").equalsIgnoreCase("Success"))
					 	{
					 		//Toast.makeText(getActivity(), "Comment Posted Successfully..", Toast.LENGTH_LONG).show();
					 		// Comments 
					 		JSONObject dataObj = csCommentResponseObj1.getJSONObject("data");
							commentArray = dataObj.getJSONArray("Table");
							
							
						   SlCommentAdapter commentAdapter = new SlCommentAdapter(getActivity(), commentArray);
						   commentListView.setAdapter(commentAdapter);
							
					 		/*CaseStudyCommentListFragment fragment = new CaseStudyCommentListFragment(commentArray);
					 		
					 	    FragmentTransaction ft = getFragmentManager().beginTransaction();
					 		//ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,R.anim.slide_in_left, R.anim.slide_out_right) ;
							ft.replace(R.id.content_frame, fragment, "CaseStudyFragment");
							ft.addToBackStack("CaseStudyFragment");
							ft.commit();*/
					 	}
					 	else
					 	{
					 		Toast.makeText(getActivity(), "Error during Comment posting", Toast.LENGTH_LONG).show();
					 	}
				} 
				  catch (JSONException e) 
				  {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject fetchSLCommetsRB = new JSONObject();
		        try {
					
					fetchSLCommetsRB.putOpt("p_sl_id",slObj.getString("sl_id"));
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String fetchSlCommetsURL = URLGenerator.getCommentsForSharedLearning();//"http://wockhardt.oh22media.com/myservice/service1.asmx/getCSCommentsForUsers";
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(fetchSlCommetsURL, fetchSLCommetsRB.toString());
		  
		 
				
			}
		}).execute();
		
		
	}
	
	
	
	private void loadImageForIndex(int index)
	{
		
		if(index < 0)
		{
			return;	
		}
		
		if(index > totlImages)
		{
			return;
		}
		
		if(index > 0)
		{
			// show left btn
			leftBtn.setVisibility(View.VISIBLE);
		}
		else
		{
		   //hide left btn
			leftBtn.setVisibility(View.GONE);
		}
		
		if(index < (totlImages - 1))
		{
			rightBtn.setVisibility(View.VISIBLE);
		}
		else
		{
			rightBtn.setVisibility(View.GONE);
		}
		
		
		 JSONObject imgObj;
		try {
			
			 imgObj = imageArray.getJSONObject(index);
			 String imageTitle = imgObj.getString("img_title");
	    	 String imgUrl = imgObj.getString("img_file");
	    	 Log.v(TAG, "Title : " + imageTitle + "" + " \nimageLink : " + imgUrl);
				
			/*img.setTag(imgUrl);
			AhmadDownloadManager ahmadDownload = new AhmadDownloadManager(this.getActivity());
			ahmadDownload.DisplayImage(imgUrl, this.getActivity(), img, bar, this);
			*/
	    	 
	    	 
	    	 
	    	// DisplayImageOptions options = new DisplayImageOptions.Builder().;
	    	 
	    	 ImageLoader loader = ImageLoader.getInstance();
	 		loader.displayImage(imgUrl, img, options);
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
	}

	@Override
	public void result(Bitmap arg0) {
		Log.i("AhmadDownloadCallback", "Not able to download");
		
	}

	
	
}
