package com.o2m.wockhard.sharedlearning;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.HomeActivity;
import com.o2m.wockhard.LoginActivity;
import com.o2m.wockhard.MultipartRequestHeader;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AddSharedLearning extends Fragment implements OnItemSelectedListener, OnClickListener {

	private static final int REQUEST_CAMERA = 3;
	private static final int SELECT_FILE = 4;
	private Spinner addSlView;
	private JSONArray addSl;

	private EditText edSubject;
	private EditText edDescription;
	private EditText edVideoLink;
	private TextView edVideoLinkTitle;
	private LinearLayout imageContainer;
	private ImageView image1, image2, image3, image4, image5, selectedImageView;
	private Button approvalBtn;

	private String category = "1";
	private String slSubject;
	private String slDescription;
	private String slVideoLink;
	private String TAG = "AddSharedLearning";
	RadioGroup radioGrp ;
	protected String userChoosenTask;


	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_shared_learning, container, false);
		addSlView = (Spinner) rootView.findViewById(R.id.addSlOptionSpinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this.getActivity(), R.array.add_sl_category_array, 
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		addSlView.setAdapter(adapter);
		addSlView.setOnItemSelectedListener(this);
		edSubject=(EditText) rootView.findViewById(R.id.et_slsubject);
		edDescription=(EditText) rootView.findViewById(R.id.et_sldesc);
		edVideoLink=(EditText) rootView.findViewById(R.id.et_slVideoLink);
		edVideoLinkTitle =  (TextView) rootView.findViewById(R.id.tv_slVideoLinkTitle);

		radioGrp = (RadioGroup) rootView.findViewById(R.id.radioGrp);
		radioGrp.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(RadioGroup group, int checkedId) {

				if(checkedId == R.id.radio_text) {

					edVideoLink.setVisibility(View.GONE);
					edVideoLinkTitle.setVisibility(View.GONE);
					imageContainer.setVisibility(View.GONE);

				} else if(checkedId == R.id.radio_video) {

					edVideoLink.setVisibility(View.VISIBLE);
					edVideoLinkTitle.setVisibility(View.VISIBLE);
					imageContainer.setVisibility(View.GONE);
				}
				else if(checkedId == R.id.radio_image) {

					edVideoLink.setVisibility(View.GONE);
					edVideoLinkTitle.setVisibility(View.GONE);
					imageContainer.setVisibility(View.VISIBLE);
				}

			}});


		approvalBtn = (Button) rootView.findViewById(R.id.btn_sendforapproval);
		approvalBtn.setOnClickListener(this);

		imageContainer = (LinearLayout) rootView.findViewById(R.id.imageContailer);
		image1 = (ImageView) rootView.findViewById(R.id.image1);
		image1.setDrawingCacheEnabled(true);
		image1.setOnClickListener(this);

		image2 = (ImageView) rootView.findViewById(R.id.image2);
		image2.setDrawingCacheEnabled(true);
		image2.setOnClickListener(this);


		image3 = (ImageView) rootView.findViewById(R.id.image3);
		image3.setDrawingCacheEnabled(true);
		image3.setOnClickListener(this);


		image4 = (ImageView) rootView.findViewById(R.id.image4);
		image4.setDrawingCacheEnabled(true);
		image4.setOnClickListener(this);
		


		image5 = (ImageView) rootView.findViewById(R.id.image5);
		image5.setDrawingCacheEnabled(true);
		image5.setOnClickListener(this);




		return rootView;
	}

	private void validateAddSL(String subject, String desc, final String catId, final int slType,
			String videoLink)
	{
		new ApplicationAsk(this.getActivity(), new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute addSl Response : " + resp);

				try {


					JSONObject addSlResponseObj = new JSONObject(resp);
					JSONObject addSlResponseObj1 = new JSONObject(addSlResponseObj.getString("d"));
					String status = addSlResponseObj1.getString("Status");

					
						if(status.equalsIgnoreCase("success"))
						{
							if(slType != 2)
							Toast.makeText(getActivity(), "Shared Learning Posted", Toast.LENGTH_SHORT).show();
							else 
							Toast.makeText(getActivity(), "Shared Learning Posted, Uploading Images", Toast.LENGTH_SHORT).show();
							
							edSubject.setText("");
							edDescription.setText("");
							edVideoLink.setText("");
							edVideoLinkTitle.setText("");
							
						}
						else
						{
							Toast.makeText(getActivity(), "Error :"+addSlResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
						}
					
					if(slType == 2 )
					{
					Long postID  = addSlResponseObj1.getJSONObject("data").getLong("sl_id");
					submitImages(postID);
					}
					

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject addSl = new JSONObject();
				try {

					URLParams urlParameter  = URLParams.getInstance();
					addSl.put("p_sl_subject", slSubject);
					addSl.put("p_sl_desc", slDescription);
					addSl.put("p_category_id", ""+catId);
					addSl.put("p_created_by", urlParameter.getUser_id());
					addSl.put("p_sl_type", ""+slType);
					addSl.put("p_video_link", slVideoLink);
					addSl.put("p_div_code", urlParameter.getDiv_code());
					//loginObj.put("", "");
				} catch (JSONException e) {

					e.printStackTrace();
				}

				//URLGenerator.getLoginUrl();

				String addSlURL = URLGenerator.getAddSharedLearningUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(addSlURL, addSl.toString());

				// return resp;

			}
		}).execute();
	}
	
	private void submitImages(final Long postID)
	{
		
		new ApplicationAsk(getActivity(), new ApplicationService() {
			private String resp;
			@Override
			public void postExecute() {
				JSONObject addSlResponseObj;
				try {
					image1.setImageDrawable(getResources().getDrawable(R.drawable.camera));
					image2.setImageDrawable(getResources().getDrawable(R.drawable.camera));
					image3.setImageDrawable(getResources().getDrawable(R.drawable.camera));
					image4.setImageDrawable(getResources().getDrawable(R.drawable.camera));
					image5.setImageDrawable(getResources().getDrawable(R.drawable.camera));
					
					addSlResponseObj = new JSONObject(resp);
					JSONObject addSlResponseObj1 = new JSONObject(addSlResponseObj.getString("d"));
					String status = addSlResponseObj1.getString("Status");
						if(status.equalsIgnoreCase("success"))
						{
							Toast.makeText(getActivity(), "Shared Learning Images Uploaded Successfuly", Toast.LENGTH_SHORT).show();
							getActivity().getFragmentManager().popBackStack();
						}
						else
						{
							Toast.makeText(getActivity(), "Error :"+addSlResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
						}
						
						
						
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			
			@Override
			public void execute() {
				if(image1.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
				{
					//MultipartRequestHeader header = new MultipartRequestHeader();
					File f = new File(getActivity().getCacheDir(), "1.jpg");
					try {
						f.createNewFile();
						//Convert bitmap to byte array
						Bitmap bitmap = Bitmap.createBitmap(image1.getDrawingCache());
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
						byte[] bitmapdata = bos.toByteArray();
						//write the bytes in file
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bitmapdata);
						fos.flush();
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Log.v(TAG, ""+f.getPath());
					Log.v(TAG, ""+Integer.parseInt(String.valueOf(f.length()/1024)));
					MultipartEntityBuilder builder = MultipartEntityBuilder.create();
					builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					FileBody fileBody = new FileBody(f);
					builder.addPart("recFile", fileBody);
					builder.addTextBody("postid", String.valueOf(postID));
					String addSlImageURL = URLGenerator.imageUpload();
					CommunicationService commService = new CommunicationService();
					 resp = commService.sendMultiPartRequest(addSlImageURL, builder, 18000, true);
					
				}
				if(image2.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
				{
					//MultipartRequestHeader header = new MultipartRequestHeader();
					File f = new File(getActivity().getCacheDir(), "2.jpg");
					try {
						f.createNewFile();
						//Convert bitmap to byte array
						Bitmap bitmap = Bitmap.createBitmap(image2.getDrawingCache());
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
						byte[] bitmapdata = bos.toByteArray();
						//write the bytes in file
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bitmapdata);
						fos.flush();
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					MultipartEntityBuilder builder = MultipartEntityBuilder.create();
					builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					FileBody fileBody = new FileBody(f);
					builder.addPart("recFile", fileBody);
					builder.addTextBody("postid", String.valueOf(postID));
					String addSlImageURL = URLGenerator.imageUpload();
					CommunicationService commService = new CommunicationService();
					 resp = commService.sendMultiPartRequest(addSlImageURL, builder, 18000, true);
					
				}
				if(image3.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
				{
					//MultipartRequestHeader header = new MultipartRequestHeader();
					File f = new File(getActivity().getCacheDir(), "3.jpg");
					try {
						f.createNewFile();
						//Convert bitmap to byte array
						Bitmap bitmap = Bitmap.createBitmap(image3.getDrawingCache());
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
						byte[] bitmapdata = bos.toByteArray();
						//write the bytes in file
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bitmapdata);
						fos.flush();
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					MultipartEntityBuilder builder = MultipartEntityBuilder.create();
					builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					FileBody fileBody = new FileBody(f);
					builder.addPart("recFile", fileBody);
					builder.addTextBody("postid", String.valueOf(postID));
					String addSlImageURL = URLGenerator.imageUpload();
					CommunicationService commService = new CommunicationService();
					 resp = commService.sendMultiPartRequest(addSlImageURL, builder, 18000, true);
					
				}
				if(image4.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
				{
					//MultipartRequestHeader header = new MultipartRequestHeader();
					File f = new File(getActivity().getCacheDir(), "4.jpg");
					try {
						f.createNewFile();
						//Convert bitmap to byte array
						Bitmap bitmap = Bitmap.createBitmap(image4.getDrawingCache());
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
						byte[] bitmapdata = bos.toByteArray();
						//write the bytes in file
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bitmapdata);
						fos.flush();
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					MultipartEntityBuilder builder = MultipartEntityBuilder.create();
					builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					FileBody fileBody = new FileBody(f);
					builder.addPart("recFile", fileBody);
					builder.addTextBody("postid", String.valueOf(postID));
					String addSlImageURL = URLGenerator.imageUpload();
					CommunicationService commService = new CommunicationService();
					 resp = commService.sendMultiPartRequest(addSlImageURL, builder, 18000, true);
					
				}
				if(image5.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
				{
					//MultipartRequestHeader header = new MultipartRequestHeader();
					File f = new File(getActivity().getCacheDir(), "5.jpg");
					try {
						f.createNewFile();
						//Convert bitmap to byte array
						Bitmap bitmap = Bitmap.createBitmap(image5.getDrawingCache());
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
						byte[] bitmapdata = bos.toByteArray();
						//write the bytes in file
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bitmapdata);
						fos.flush();
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					MultipartEntityBuilder builder = MultipartEntityBuilder.create();
					builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					FileBody fileBody = new FileBody(f);
					builder.addPart("recFile", fileBody);
					builder.addTextBody("postid", String.valueOf(postID));
					String addSlImageURL = URLGenerator.imageUpload();
					CommunicationService commService = new CommunicationService();
					 resp = commService.sendMultiPartRequest(addSlImageURL, builder, 18000, true);
					
				}
						
				
				
				
			}
		}).execute();
	}

	private int onRadioButtonClicked(View view) {
		// TODO Auto-generated method stub
		/*		boolean checked = ((RadioButton) view).isChecked();
		switch (view.getId()) {
		case R.id.radio_text:
			if (checked){
				return 0;
				break;

			}

		case R.id.radio_video:
			if (checked){
				return 1;
			}
			break;
		}
		 */
		if	(view.getId()==R.id.radio_text){
			return 0;
		}
		else{
			return 1;
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		int position = addSlView.getSelectedItemPosition();
		category = ""+ (position + 1) ;//arg0.getItemAtPosition(position).toString();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {

		Log.v(TAG, "ocClick --------");

		//validation required

		if (v == approvalBtn) {
			slSubject = edSubject.getText().toString();
			slDescription = edSubject.getText().toString();
			slVideoLink = edVideoLink.getText().toString();
			if (slSubject.trim().equalsIgnoreCase("")) {
				Toast.makeText(this.getActivity(), "Subject can't be empty",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if (slDescription.trim().equalsIgnoreCase("")) {
				Toast.makeText(this.getActivity(),
						"Description can't be empty", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			if (radioGrp.getCheckedRadioButtonId() == R.id.radio_text) {
				validateAddSL(slSubject, slDescription, category, 0, "");
			} else if (radioGrp.getCheckedRadioButtonId() == R.id.radio_video) {
				if (slVideoLink.trim().equalsIgnoreCase("")) {
					Toast.makeText(this.getActivity(),
							"Video link can't be empty", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				validateAddSL(slSubject, slDescription, category, 1, "");
			}
			else if (radioGrp.getCheckedRadioButtonId() == R.id.radio_image) {
				
				if(image1.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState() || 
						image2.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState() ||
						image3.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState() ||
						image4.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState() || 
						image5.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
				{
					//It means at least one view is camera selected
					validateAddSL(slSubject, slDescription, category, 2, "");
					
				}
				else
				{
					Toast.makeText(this.getActivity(),
							"Please select at least one image", Toast.LENGTH_SHORT)
							.show();
				}
				
				
				
			}
		}
		else if(v == image1 || v == image2 || v == image3 || v == image4 || v == image5)
		{
			selectedImageView = (ImageView) v;
			selectImage();
			
			if(selectedImageView.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.camera).getConstantState())
			{
				Log.v(TAG, "IMAGE CAMERA");
			}
		}
		
	}

	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
		"Remove" , "Cancel" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(items,new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				//boolean result=Utility.checkPermission(AddSharedLearning.this.getActivity());
				if (items[item].equals("Take Photo")) {
					userChoosenTask="Take Photo";
					//if(result)
					cameraIntent();
				} else if (items[item].equals("Choose from Library")) {
					userChoosenTask="Choose from Library";
					//if(result)
					galleryIntent();
				}
				else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
				else if (items[item].equals("Remove")) {
					selectedImageView.setImageDrawable(getResources().getDrawable(R.drawable.camera));
				}



			}


		}).show();
	}

	private void cameraIntent()
	{
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, REQUEST_CAMERA);
	}

	private void galleryIntent()
	{
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);//
		startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_FILE)
				onSelectFromGalleryResult(data);
			else if (requestCode == REQUEST_CAMERA)
				onCaptureImageResult(data);
		}
	}

	@SuppressWarnings("deprecation")
	private void onSelectFromGalleryResult(Intent data) {
		Bitmap bm=null;
		if (data != null) {
			try {
				bm = MediaStore.Images.Media.getBitmap(this.getActivity().getApplicationContext().getContentResolver(), data.getData());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		selectedImageView.setImageBitmap(bm);
		
		if(selectedImageView.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
		{
			Log.v(TAG, "IMAGE CHANGED");
		}
	}

	private void onCaptureImageResult(Intent data) {
		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
		File destination = new File(Environment.getExternalStorageDirectory(),
				System.currentTimeMillis() + ".jpg");
		FileOutputStream fo;
		try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		selectedImageView.setImageBitmap(thumbnail);
	
		if(selectedImageView.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.camera).getConstantState())
		{
			Log.v(TAG, "IMAGE CHANGED");
		}
	}


}
