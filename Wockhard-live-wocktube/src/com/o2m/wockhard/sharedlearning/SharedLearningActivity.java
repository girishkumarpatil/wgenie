package com.o2m.wockhard.sharedlearning;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.wochhard.casestudy.CaseStudyFragment;
import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.BaseActivity;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.GridHomeActivity;
import com.o2m.wockhard.HomeActivity;
import com.o2m.wockhard.InboxActivity;
import com.o2m.wockhard.NavigationDrawerListAdapter;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.o2m.wockhard.epoll.EPollListFragment;
import com.o2m.wockhard.epoll.EPollResultListFragment;
import com.o2m.wockhard.faqs.FaqListFragment;
import com.o2m.wockhard.inbox.MessageListFragment;
import com.o2m.wockhard.milestones.MilestoneListFragment;
import com.o2m.wockhard.performancewall.PerformanceWallListFragment;
import com.o2m.wockhard.wocktube.WocktubeListFragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SharedLearningActivity extends Activity {

	private String TAG = "SharedLearningActivity";
	 private DrawerLayout mDrawerLayout;
	    private ListView mDrawerList;
	    private String[] mMenuTitles;
		private int i;
		int screenPosition;
		private URLParams urlParameter = URLParams.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox);
	
		 mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	        mDrawerList = (ListView) findViewById(R.id.left_drawer);
	        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
	        mMenuTitles = getResources().getStringArray(R.array.menu_array);
	        NavigationDrawerListAdapter customAdapter = new NavigationDrawerListAdapter(this, mMenuTitles);
	        mDrawerList.setAdapter(customAdapter);
	        // set up the drawer's list view with items and click listener
//	        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
//	                R.layout.drawer_list_item, mPlanetTitles));
	        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		init();
	}
	
	private void init()
	{
		new ApplicationAsk(this, new ApplicationService() {
			
			 private String resp;
			 private JSONArray sharedLearnArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute SharedLearning Response : " + resp);
				  
				  try {
					  
					  
					JSONObject sharedLearnResponseObj = new JSONObject(resp);
					Log.v(TAG," onPostExecute Status : " + sharedLearnResponseObj.getString("d"));
					
					JSONObject sharedLearnResponseObj1 = new JSONObject(sharedLearnResponseObj.getString("d"));
					
					Log.v(TAG," onPostExecute message : " +  sharedLearnResponseObj1.getString("Status"));
					
					JSONObject dataObj = sharedLearnResponseObj1.getJSONObject("data");
					 sharedLearnArray = dataObj.getJSONArray("Table");
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(SharedLearningActivity.this, "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				  
				  Fragment fragment = new SharedLearningListFragment(sharedLearnArray);  
				 
				  FragmentTransaction ft = getFragmentManager().beginTransaction();
				  ft.addToBackStack("SharedLearningListFragment");
			      ft.replace(R.id.content_frame, fragment).commit();


				
			}
			
			@Override
			public void execute() {
				JSONObject sharedLearningRequestObj = new JSONObject();
		        try {
					/*sharedLearningRequestObj.putOpt("p_div_code", "T1");
					sharedLearningRequestObj.putOpt("p_category_id", "0");
					sharedLearningRequestObj.putOpt("p_user_id", "2");*/
		        	
		        	URLParams urlParameters = URLParams.getInstance();
		        	sharedLearningRequestObj.putOpt("p_div_code", urlParameters.getDiv_code());
					sharedLearningRequestObj.putOpt("p_category_id", "0");
					sharedLearningRequestObj.putOpt("p_user_id", urlParameters.getUser_id());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String sharedLearningURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetSharedLearningForFrontend";
		        String sharedLearningURL = URLGenerator.getSharedLearning(); 
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(sharedLearningURL, sharedLearningRequestObj.toString());
		  
		  // return resp;
				
			}
		}).execute();
	}
	
	public void menuButton(View v)
	{
		if (i == 0)
		{
			mDrawerLayout.openDrawer(mDrawerList);
			i = 1;
		}
		else
		{
			i = 0;
			mDrawerLayout.closeDrawer(mDrawerList);
		}

	}
	
	public void homeButton(View v)
	{
		Intent homeIntent = new Intent(this, GridHomeActivity.class);
		startActivity(homeIntent);
	}
	
	 private class DrawerItemClickListener implements ListView.OnItemClickListener {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        	screenPosition = position;
	        	selectItem(position);
	        }
	    }
	 
/*	 private void selectItem(final int position) {
	          
	        // update selected item and title, then close the drawer
	        mDrawerList.setItemChecked(position, true);
	        setTitle(mPlanetTitles[position]);
	        mDrawerLayout.closeDrawer(mDrawerList);
	       
	    }*/
	 
		private void selectItem(final int position) {

			// Toast.makeText(this, "---> "+position, Toast.LENGTH_SHORT).show();
			
			if(position >= 0 && position < 8)
				this.clearStack();
			

			switch (position) {
			// case
			case 0:
				this.loadePoll();

				break;
				// ePoll
			case 1:
				this.loadCaseStudy();
				break;
				// wocktube
			case 2:
				this.loadSL();

				break;
				// inbox
			case 3:

				this.loadInbox();
				break;
				// sl
			case 4:
				this.loadWocktube();
				break;
				// faq
			case 5:

				this.loadFaq();
				break;
				// milestone
			case 6:

				this.loadMilestone();
				break;
				// performance wall
			case 7:

				this.loadPerformancewall();
			/*	Fragment fragment = new TestFragment();
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(screenPosition, true);
				setTitle(mMenuTitles[screenPosition]);
				mDrawerLayout.closeDrawer(mDrawerList);*/
				break;

			default:

				Toast.makeText(this, "Invalid configuration ", Toast.LENGTH_SHORT)
				.show();
				break;
			}

		}

		private void loadCaseStudy() {

			new ApplicationAsk(this, new ApplicationService() {

				private String resp;
				private JSONArray faqArray;

				@Override
				public void postExecute() {
					Log.v(TAG, " onPostExecute loadCaseStudy Response : " + resp);

					try {

						JSONObject csResponseObj = new JSONObject(resp);

						Log.v(TAG,
								" onPostExecute Status : "
										+ csResponseObj.getString("d"));

						JSONObject csResponseObj1 = new JSONObject(
								csResponseObj.getString("d"));

						Log.v(TAG,
								" onPostExecute message : "
										+ csResponseObj1.getString("Status"));

						JSONObject dataObj = csResponseObj1.getJSONObject("data");
						faqArray = dataObj.getJSONArray("Table");
						JSONObject caseStudyObj = (JSONObject) faqArray.get(0);
						
						Fragment csF = new CaseStudyFragment(caseStudyObj);
						FragmentTransaction ft = getFragmentManager().beginTransaction();
						ft.addToBackStack("CaseStudyFragment");
						ft.replace(R.id.content_frame, csF).commit();
						
						// getFragmentManager().beginTransaction().commit();
						mDrawerList.setItemChecked(screenPosition, true);
						setTitle(mMenuTitles[screenPosition]);
						mDrawerLayout.closeDrawer(mDrawerList);

					} catch (JSONException e) {

						e.printStackTrace();
						Toast.makeText(SharedLearningActivity.this, "Data Parsing Error",
								Toast.LENGTH_LONG).show();
					}

				}

				@Override
				public void execute() {
					JSONObject csRequestObj = new JSONObject();
					try {

						/*
						 * csRequestObj.putOpt("p_div_code", "E");
						 * csRequestObj.putOpt("p_level_code", "L1");
						 */
						csRequestObj.putOpt("p_employee_id",
								urlParameter.getEmployee_id());
						csRequestObj.putOpt("p_div_code",
								urlParameter.getDiv_code());
						csRequestObj.putOpt("p_level_code",
								urlParameter.getLevel_code());

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// String csURL =
					// "http://wockhardt.oh22media.com/myservice/service1.asmx/getCaseStudyByDivDesg";
					String csURL = URLGenerator.getCaseStudy();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(csURL,
							csRequestObj.toString());

				}
			}).execute();

		}

		private void loadePoll() {
			new ApplicationAsk(this, new ApplicationService() {

				private String resp;
				private JSONArray faqArray;

				@Override
				public void postExecute() {
					Log.v(TAG, " onPostExecute inbox Response : " + resp);

					try {

						JSONObject epollResponseObj = new JSONObject(resp);

						Log.v(TAG,
								" onPostExecute Status : "
										+ epollResponseObj.getString("d"));

						JSONObject epollResponseObj1 = new JSONObject(
								epollResponseObj.getString("d"));

						Log.v(TAG,
								" onPostExecute message : "
										+ epollResponseObj1.getString("Status"));

						String epollResponseStatus = epollResponseObj1
								.getString("Status");
						
						if (!epollResponseStatus.equalsIgnoreCase("Success")) {
							Toast.makeText(SharedLearningActivity.this,
									epollResponseObj1.getString("message"),
									Toast.LENGTH_LONG).show();
							return;
						} else {
							JSONObject dataObj = epollResponseObj1
									.getJSONObject("data");
							faqArray = dataObj.getJSONArray("Table");
							if (faqArray == null) {
								Toast.makeText(SharedLearningActivity.this,
										"Data Parsing Error", Toast.LENGTH_SHORT)
										.show();
								return;
							}
							if (faqArray.length() <= 0) {
								Toast.makeText(SharedLearningActivity.this,
										"No data to Display", Toast.LENGTH_SHORT)
										.show();
								return;
							}

							JSONObject ePollResponse = (JSONObject) faqArray.get(0);

							if (ePollResponse.get("status").equals("Pending")) {
								String pollId = ePollResponse.getString("Poll_id");
								JSONArray ePollQueArray = ePollResponse
										.getJSONArray("Questions");
								EPollListFragment fragment = new EPollListFragment(
										ePollQueArray, pollId);

								// JSONArray ePollResultArray =
								// ePollResponse.getJSONArray("Result");
								// EPollResultListFragment fragment = new
								// EPollResultListFragment(ePollResultArray);

								if (ePollResponse.getString("Poll_ans").equals("0")) {
									// 0 : hide ans 1 : shw ans

									fragment.setShowAns(false);
								} else {
									fragment.setShowAns(true);
								}

								FragmentTransaction ft = getFragmentManager().beginTransaction();
								ft.addToBackStack("EPollListFragment");
								
								ft.replace(R.id.content_frame, fragment)
								.commit();
							} else {// Result

								JSONArray ePollResultArray = ePollResponse
										.getJSONArray("Result");
								EPollResultListFragment fragment = new EPollResultListFragment(
										ePollResultArray);

								if (ePollResponse.getString("Poll_ans").equals("0")) {
									// 0 : hide ans 1 : shw ans

									fragment.setShowAns(false);
								} else {
									fragment.setShowAns(true);
								}

								FragmentTransaction ft = getFragmentManager().beginTransaction();
								ft.addToBackStack("EPollResultListFragment");
								ft.replace(R.id.content_frame, fragment).commit();

								/*
								 * String pollId =
								 * ePollResponse.getString("Poll_id"); JSONArray
								 * ePollQueArray =
								 * ePollResponse.getJSONArray("Questions");
								 * EPollListFragment fragment = new
								 * EPollListFragment(ePollQueArray, pollId);
								 * 
								 * // JSONArray ePollResultArray =
								 * ePollResponse.getJSONArray("Result"); //
								 * EPollResultListFragment fragment = new
								 * EPollResultListFragment(ePollResultArray);
								 * 
								 * if(ePollResponse.getString("Poll_ans").equals("0")
								 * ) { //0 : hide ans 1 : shw ans
								 * 
								 * fragment.setShowAns(false); } else {
								 * fragment.setShowAns(true); }
								 * 
								 * 
								 * FragmentManager fragmentManager =
								 * getFragmentManager();
								 * fragmentManager.beginTransaction
								 * ().replace(R.id.content_frame,
								 * fragment).commit();
								 */

							}

						}
					} catch (JSONException e) {

						e.printStackTrace();
						Toast.makeText(SharedLearningActivity.this, "Data Parsing Error",
								Toast.LENGTH_LONG).show();
					}

					// update selected item and title, then close the drawer
					mDrawerList.setItemChecked(screenPosition, true);
					setTitle(mMenuTitles[screenPosition]);
					mDrawerLayout.closeDrawer(mDrawerList);

				}

				@Override
				public void execute() {
					JSONObject ePollRequestObj = new JSONObject();
					try {
						/*
						 * ePollRequestObj.putOpt("p_employee_id", "0");
						 * ePollRequestObj.putOpt("p_div_code", "E");
						 * ePollRequestObj.putOpt("p_level_code", "L1");
						 */

						ePollRequestObj.putOpt("p_employee_id",
								urlParameter.getEmployee_id());
						ePollRequestObj.putOpt("p_div_code",
								urlParameter.getDiv_code());
						ePollRequestObj.putOpt("p_level_code",
								urlParameter.getLevel_code());

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// String ePollURL =
					// "http://wockhardt.oh22media.com/myservice/service1.asmx/GetEpollFrontend";
					String ePollURL = URLGenerator.getEPoll();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(ePollURL,
							ePollRequestObj.toString());

				}
			}).execute();
		}

		private void loadInbox() {
			Intent inboxIntent = new Intent(this, InboxActivity.class);
			startActivity(inboxIntent);
		}

		private void loadSL() {
			Intent inboxIntent = new Intent(this, SharedLearningActivity.class);
			startActivity(inboxIntent);
		}

		private void loadWocktube() {
			new ApplicationAsk(this, new ApplicationService() {

				private String resp;
				private JSONArray faqArray;

				@Override
				public void postExecute() {
					Log.v(TAG, " onPostExecute inbox Response : " + resp);

					try {

						JSONObject wTResponseObj = new JSONObject(resp);

						Log.v(TAG,
								" onPostExecute Status : "
										+ wTResponseObj.getString("d"));

						JSONObject wTResponseObj1 = new JSONObject(
								wTResponseObj.getString("d"));

						Log.v(TAG,
								" onPostExecute message : "
										+ wTResponseObj1.getString("Status"));

						JSONObject dataObj = wTResponseObj1.getJSONObject("data");
						faqArray = dataObj.getJSONArray("Table");
						
						Fragment fragment = new WocktubeListFragment(faqArray);
						FragmentTransaction ft = getFragmentManager().beginTransaction();
						ft.addToBackStack("WocktubeListFragment");
						ft.replace(R.id.content_frame, fragment).commit();

						// update selected item and title, then close the drawer
						mDrawerList.setItemChecked(screenPosition, true);
						setTitle(mMenuTitles[screenPosition]);
						mDrawerLayout.closeDrawer(mDrawerList);

					} catch (JSONException e) {

						e.printStackTrace();
						Toast.makeText(SharedLearningActivity.this, "Data Parsing Error",
								Toast.LENGTH_LONG).show();
					}

				}

				@Override
				public void execute() {
					JSONObject wocktubeRequestObj = new JSONObject();
					try {
						/*
						 * wocktubeRequestObj.putOpt("p_div_code", "T1");
						 * wocktubeRequestObj.putOpt("p_desg_level", "L1");
						 */
						wocktubeRequestObj.putOpt("p_div_code",
								urlParameter.getDiv_code());
						wocktubeRequestObj.putOpt("p_desg_level",
								urlParameter.getLevel_code());

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// String wocktubeURL =
					// "http://wockhardt.oh22media.com/myservice/service1.asmx/GetWocktubeForFrontend";
					String wocktubeURL = URLGenerator.getWockTube();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(wocktubeURL,
							wocktubeRequestObj.toString());

				}
			}).execute();
		}

		private void loadFaq() {
			new ApplicationAsk(this, new ApplicationService() {

				private String resp;
				private JSONArray faqArray;

				@Override
				public void postExecute() {
					Log.v(TAG, " onPostExecute inbox Response : " + resp);

					try {

						JSONObject faqResponseObj = new JSONObject(resp);

						Log.v(TAG,
								" onPostExecute Status : "
										+ faqResponseObj.getString("d"));

						JSONObject faqResponseObj1 = new JSONObject(
								faqResponseObj.getString("d"));

						Log.v(TAG,
								" onPostExecute message : "
										+ faqResponseObj1.getString("Status"));

						JSONObject dataObj = faqResponseObj1.getJSONObject("data");
						faqArray = dataObj.getJSONArray("Table");

					} catch (JSONException e) {

						e.printStackTrace();
						Toast.makeText(SharedLearningActivity.this, "Data Parsing Error",
								Toast.LENGTH_LONG).show();
					}

					Fragment fragment = new FaqListFragment(faqArray);
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.addToBackStack("FaqListFragment");
					ft.replace(R.id.content_frame, fragment).commit();

					// update selected item and title, then close the drawer
					mDrawerList.setItemChecked(screenPosition, true);
					setTitle(mMenuTitles[screenPosition]);
					mDrawerLayout.closeDrawer(mDrawerList);

				}

				@Override
				public void execute() {
					JSONObject faqRequestObj = new JSONObject();
					try {
						// faqRequestObj.putOpt("p_div_code", "T1");
						faqRequestObj.putOpt("p_div_code",
								urlParameter.getDiv_code());

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// String faqURL =
					// "http://wockhardt.oh22media.com/myservice/service1.asmx/GetFAQForFrontendByDivision";
					String faqURL = URLGenerator.getFAQ();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(faqURL,
							faqRequestObj.toString());

				}
			}).execute();
		}

		private void loadMilestone() {
			Fragment fragment = new MilestoneListFragment();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack("MilestoneListFragment");
			ft.replace(R.id.content_frame, fragment).commit();

		}

		private void loadPerformancewall() {
			new ApplicationAsk(this, new ApplicationService() {

				private String resp;
				private JSONArray pwArray;

				@Override
				public void postExecute() {
					Log.v(TAG, " onPostExecute inbox Response : " + resp);

					try {

						JSONObject pwResponseObj = new JSONObject(resp);

						Log.v(TAG,
								" onPostExecute Status : "
										+ pwResponseObj.getString("d"));

						JSONObject pwResponseObj1 = new JSONObject(
								pwResponseObj.getString("d"));

						Log.v(TAG,
								" onPostExecute message : "
										+ pwResponseObj1.getString("Status"));

						JSONObject dataObj = pwResponseObj1.getJSONObject("data");
						pwArray = dataObj.getJSONArray("Table");

					} catch (JSONException e) {

						e.printStackTrace();
						Toast.makeText(SharedLearningActivity.this, "Data Parsing Error",
								Toast.LENGTH_LONG).show();
					}

					Fragment fragment = new PerformanceWallListFragment(pwArray);
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.addToBackStack("PerformanceWallListFragment");
					ft.replace(R.id.content_frame, fragment).commit();

					// update selected item and title, then close the drawer
					mDrawerList.setItemChecked(screenPosition, true);
					setTitle(mMenuTitles[screenPosition]);
					mDrawerLayout.closeDrawer(mDrawerList);

				}

				@Override
				public void execute() {
					JSONObject pwRequestObj = new JSONObject();
					try {
						pwRequestObj.putOpt("p_performance_id", "0");
						pwRequestObj.putOpt("p_div_code", urlParameter.getDiv_code());
						

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// String pwWallURL =
					// "http://wockhardt.oh22media.com/myservice/service1.asmx/App_GetPerformanceWall";
					String pwWallURL = URLGenerator.getPerformanceWall();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(pwWallURL,
							pwRequestObj.toString());

				}
			}).execute();
		}
	 
	 
	 @Override
		public boolean onKeyDown(int keyCode, KeyEvent event)
		{
			if (keyCode == KeyEvent.KEYCODE_BACK)
			{

				if (getFragmentManager().getBackStackEntryCount() == 1)
				{
					this.finish();
					//return false;
				}
				else
				{

					getFragmentManager().popBackStack();
					removeCurrentFragment();
					return false;
				}

			}
			return super.onKeyDown(keyCode, event);
		}
	 
		public void removeCurrentFragment()
		{
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			Fragment currentFrag = getFragmentManager().findFragmentById(R.id.content_frame);

			if (currentFrag != null)
				transaction.remove(currentFrag);

			transaction.commit();

		}
	
		public void clearStack()
		{
			/*
			FragmentManager manager = getFragmentManager();
			
			for(int i = 0 ; i < manager.getBackStackEntryCount(); i++)
			{
				manager.popBackStack();
			}
				*/	
		}

	 
	
}
