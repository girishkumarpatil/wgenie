package com.o2m.wockhard.sharedlearning;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class SlCommentAdapter extends BaseAdapter
{

	Context context;
	JSONArray wTJsonArray;

	public SlCommentAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.wTJsonArray = _arrayMessages;
	}

	@Override
	public int getCount()
	{

		return wTJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return wTJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();

			view = inflater.inflate(R.layout.sl_comment_list_item, parent, false);
			holder.txtView_csCommentTitle = (TextView) view.findViewById(R.id.slCommentBy);
			holder.txtView_csCommentDesc = (TextView)view.findViewById(R.id.slCommentDesc);
			
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
				
		
		
		try {
			JSONObject wTObj = (JSONObject) wTJsonArray.get(position);
			
			holder.txtView_csCommentTitle.setText("" + wTObj.getString("name"));
			holder.txtView_csCommentDesc.setText("" + wTObj.getString("comment_desc"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_csCommentTitle;
		
		public TextView txtView_csCommentDesc;

	}

}
