package com.o2m.wockhard;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class NavigationDrawerListAdapter extends BaseAdapter
{
	Context context;

	String[] fragments;

	public NavigationDrawerListAdapter(Context context, String[] mMenuTitles)
	{
		this.context = context;
		this.fragments = mMenuTitles;

	}

	@Override
	public int getCount()
	{

		return fragments.length;
	}

	@Override
	public Object getItem(int pos)
	{

		return fragments[pos];
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.drawer_list, parent, false);
			holder.Icon = (ImageView) view.findViewById(R.id.imageView_icon);
			//holder.txtView_Fragment = (TextView) view.findViewById(R.id.txt_fragment);
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		try
		{
			//holder.txtView_Fragment.setText(fragments.get(postion).toString());

			if (postion == 0)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_epoll);
				
			}
			else if (postion == 1)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_case_study);
			}
			else if (postion == 2)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_shared_learning);
				
			}
			else if (postion == 3)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_assessment);
				
			}
			else if (postion == 4)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_inbox);
			}
			else if (postion == 5)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_wocktube);
			}
			else if (postion == 6)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_faq);
			}
			else if (postion == 7)
			{
				holder.Icon.setBackgroundResource(R.drawable.menu_milestone);
			}
			else if (postion == 8)
			{
				
				holder.Icon.setBackgroundResource(R.drawable.menu_performance_wall);
			}
			
		}
		catch (Exception e)
		{

		}
		return view;
	}

	private static class ViewHolder
	{
		public ImageView Icon;

		//public TextView txtView_Fragment;

	}
}
