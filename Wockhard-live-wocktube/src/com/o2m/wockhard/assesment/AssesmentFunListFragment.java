package com.o2m.wockhard.assesment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.AssessmentType;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AssesmentFunListFragment extends Fragment implements
		OnItemClickListener {

	protected static final String TAG = "AssesmentFunListFragment";
	private ListView assessmentFunListView;
	private String[] mAssessmentFunTitles;
	private URLParams urlParameter = URLParams.getInstance();

	public AssesmentFunListFragment() {
		// Empty constructor required for fragment subclasses

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_assesment_fun,
				container, false);
		assessmentFunListView = (ListView) rootView
				.findViewById(R.id.assesmentFunList);
		String tempmAssessmentFunTitles[] = getResources().getStringArray(
				R.array.Assessment_fun_array);

		String levelCode = urlParameter.getLevel_code();
		if (levelCode.equalsIgnoreCase("L2")
				|| levelCode.equalsIgnoreCase("L3"))
			mAssessmentFunTitles = getResources().getStringArray(
					R.array.Assessment_fun_array_L2_L3);
		else
			mAssessmentFunTitles = getResources().getStringArray(
					R.array.Assessment_fun_array);
		AssesmentFunAdapter inboxMsgAdapter = new AssesmentFunAdapter(
				this.getActivity(), mAssessmentFunTitles);
		assessmentFunListView.setAdapter(inboxMsgAdapter);
		assessmentFunListView.setOnItemClickListener(this);

		return rootView;
	}

	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {

		Toast.makeText(getActivity(), "Pos : " + position, Toast.LENGTH_SHORT)
				.show();
		if (position == 0) {
			this.loadLatestAssessment();
		} else if (position == 1) {
			this.loadInductionAssessment();
		} else if (position == 2)// certificate
		{
			this.loadCertificateAssessment();
		} else if (position == 3)// course
		{
			this.loadCourseAssessment();
		} else if (position == 4)// individual
		{
			this.loadIndividualAssessment();
		} else if (position == 5)// wocktube
		{
			this.loadWocktubeAssessment();
		} else if (position == 6) // MISC.
		{
			this.loadMISCAssessment();
		} else if (position == 7) // PRE-POST ASSESSMENT.
		{
			this.loadPrePostAssessment();
		}

	}

	private void loadLatestAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject latestResponseObj = new JSONObject(resp);
					JSONObject latestResponseObj1 = new JSONObject(
							latestResponseObj.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ latestResponseObj1.getString("Status"));

					JSONObject dataObj = latestResponseObj1
							.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject prepostAss = dataArray.getJSONObject(0);
						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								prepostAss);
						fragment.setAssessmentType(AssessmentType.INTERACTIVE);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject latestAssRequestObj = new JSONObject();
				try {

					latestAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					latestAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					latestAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					latestAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					latestAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());	
							
					/*latestAssRequestObj.putOpt("p_div_code",
							"E");
					latestAssRequestObj.putOpt("p_desg_level",
							"L1");
					latestAssRequestObj.putOpt("p_employee_id",
							"All");
					latestAssRequestObj.putOpt("p_zone_code",
							"All");
					latestAssRequestObj.putOpt("p_region_code",
							"All");*/
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String latestAssURL = URLGenerator.getLatestAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(latestAssURL,
						latestAssRequestObj.toString());

			}
		}).execute();

	}

	private void loadPrePostAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject prepostResponseObj = new JSONObject(resp);
					JSONObject prepostResponseObj1 = new JSONObject(
							prepostResponseObj.getString("d"));
					Log.v(TAG, " onPostExecute message : "
							+ prepostResponseObj1.getString("Status"));

					JSONObject dataObj = prepostResponseObj1
							.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject prepostAss = dataArray.getJSONObject(0);

						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								prepostAss);
						fragment.setAssessmentType(AssessmentType.PREPOST);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject prepostAssRequestObj = new JSONObject();
				try {

					prepostAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					prepostAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					// sString doj = urlParameter.getDoj();
					String fyear = urlParameter.getYear();// /doj.substring(5,
															// 9);
					// String fyear = doj.s
					// Date date = new Date

					prepostAssRequestObj.putOpt("fyear", fyear);
					prepostAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					prepostAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					prepostAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String prepostAssURL = URLGenerator.getPREPOSTAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(prepostAssURL,
						prepostAssRequestObj.toString());

			}
		}).execute();
	}

	private void loadInductionAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute inbox Response : " + resp);

				try {

					JSONObject caResponseObj = new JSONObject(resp);
					JSONObject caResponseObj1 = new JSONObject(caResponseObj
							.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ caResponseObj1.getString("Status"));

					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject indAss = dataArray.getJSONObject(0);

						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								indAss);
						fragment.setAssessmentType(AssessmentType.Induction);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject inductionAssRequestObj = new JSONObject();
				try {

					inductionAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					inductionAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					inductionAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					inductionAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());
					inductionAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					inductionAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					inductionAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());

					/*
					 * inductionAssRequestObj.putOpt("p_div_code", "E");
					 * inductionAssRequestObj.putOpt("p_desg_level", "L1");
					 * inductionAssRequestObj.putOpt("p_zone_code", "Z1");
					 * inductionAssRequestObj.putOpt("p_region_code", "R1");
					 * inductionAssRequestObj.putOpt("p_employee_id", "8081");
					 * inductionAssRequestObj.putOpt("p_user_id", "2");
					 * inductionAssRequestObj.putOpt("p_old_user_id",
					 * "260356040");
					 */
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String indutionAssURL = URLGenerator.getInductionAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(indutionAssURL,
						inductionAssRequestObj.toString());

			}
		}).execute();

	}

	private void loadIndividualAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject caResponseObj = new JSONObject(resp);
					JSONObject caResponseObj1 = new JSONObject(caResponseObj
							.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ caResponseObj1.getString("Status"));

					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject indAss = dataArray.getJSONObject(0);

						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								indAss);
						fragment.setAssessmentType(AssessmentType.Individual);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject inductionAssRequestObj = new JSONObject();
				try {

					inductionAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					inductionAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					inductionAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					inductionAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());
					inductionAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					inductionAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					inductionAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String indutionAssURL = URLGenerator.getIndividualAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(indutionAssURL,
						inductionAssRequestObj.toString());

			}
		}).execute();

	}

	private void loadCertificateAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject caResponseObj = new JSONObject(resp);
					JSONObject caResponseObj1 = new JSONObject(caResponseObj
							.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ caResponseObj1.getString("Status"));

					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject indAss = dataArray.getJSONObject(0);

						CertificationAssessmentDetailsFragment fragment = new CertificationAssessmentDetailsFragment(
								indAss);
						fragment.setAssessmentType(AssessmentType.Certificate);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject inductionAssRequestObj = new JSONObject();
				try {

					inductionAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					inductionAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					inductionAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					inductionAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());
					inductionAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					inductionAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					inductionAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());
					inductionAssRequestObj.putOpt("doj_div_level",
							urlParameter.getDoj());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String indutionAssURL = URLGenerator
						.getCertificationAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(indutionAssURL,
						inductionAssRequestObj.toString());

			}
		}).execute();

	}

	//

	private void loadCourseAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject caResponseObj = new JSONObject(resp);
					JSONObject caResponseObj1 = new JSONObject(caResponseObj
							.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ caResponseObj1.getString("Status"));

					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject indAss = dataArray.getJSONObject(0);

						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								indAss);
						fragment.setAssessmentType(AssessmentType.Course);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject inductionAssRequestObj = new JSONObject();
				try {

					inductionAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					inductionAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					inductionAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					inductionAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());
					inductionAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					inductionAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					inductionAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String indutionAssURL = URLGenerator.getCourseAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(indutionAssURL,
						inductionAssRequestObj.toString());

			}
		}).execute();

	}

	private void loadWocktubeAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject caResponseObj = new JSONObject(resp);
					JSONObject caResponseObj1 = new JSONObject(caResponseObj
							.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ caResponseObj1.getString("Status"));

					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject indAss = dataArray.getJSONObject(0);

						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								indAss);
						fragment.setAssessmentType(AssessmentType.Wocktube);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject inductionAssRequestObj = new JSONObject();
				try {

					inductionAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					inductionAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					inductionAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					inductionAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());
					inductionAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					inductionAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					inductionAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String indutionAssURL = URLGenerator.getWocktubeAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(indutionAssURL,
						inductionAssRequestObj.toString());

			}
		}).execute();

	}

	private void loadMISCAssessment() {
		new ApplicationAsk(getActivity(), new ApplicationService() {

			private String resp;
			private JSONArray dataArray;

			@Override
			public void postExecute() {

				try {

					JSONObject caResponseObj = new JSONObject(resp);
					JSONObject caResponseObj1 = new JSONObject(caResponseObj
							.getString("d"));
					Log.v(TAG,
							" onPostExecute message : "
									+ caResponseObj1.getString("Status"));

					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");

					if (dataArray.length() > 0) {
						JSONObject indAss = dataArray.getJSONObject(0);

						AssessmentDetailsFragment fragment = new AssessmentDetailsFragment(
								indAss);
						fragment.setAssessmentType(AssessmentType.MISC);
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();
						ft.addToBackStack("AssessmentDetailsFragment");
						ft.replace(R.id.content_frame, fragment).commit();

					} else {
						Toast.makeText(getActivity(), "No Data Found",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject inductionAssRequestObj = new JSONObject();
				try {

					inductionAssRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					inductionAssRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());
					inductionAssRequestObj.putOpt("p_zone_code",
							urlParameter.getZone_code());
					inductionAssRequestObj.putOpt("p_region_code",
							urlParameter.getRegion_code());
					inductionAssRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					inductionAssRequestObj.putOpt("p_user_id",
							urlParameter.getUser_id());
					inductionAssRequestObj.putOpt("p_old_user_id",
							urlParameter.getOld_user_id());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String indutionAssURL = URLGenerator.getMISCAssessment();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(indutionAssURL,
						inductionAssRequestObj.toString());

			}
		}).execute();

	}

}
