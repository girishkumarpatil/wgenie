package com.o2m.wockhard.assesment;



import com.o2m.wockhard.HomeActivity.Screen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.AssessmentType;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.LoadWebPageFragment;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.HomeActivity.Screen;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AssessmentDetailsFragment extends Fragment implements OnChildClickListener {

	protected static final String TAG = "AssessmentDetailsFragment";
	private ExpandableListView pendingAssessmentListView, completedAssessmentListView ;
	private TextView pendingTitle, completedTitle;
	private JSONObject assessmentObj;
	private JSONArray completedAssessmentArray, pendingAssessmentArray;
	AssessmentType assessmentType;
	
	 public AssessmentDetailsFragment(JSONObject _assessmentObj) {
         
		 this.assessmentObj = _assessmentObj;
		 
		 try {
			
			 this.pendingAssessmentArray = _assessmentObj.getJSONArray("Pending");
			 this.completedAssessmentArray = _assessmentObj.getJSONArray("Completed");
			 
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		
		 
     }

	 private void setGroupIndicatorToRight() {
         /* Get the screen width */
         DisplayMetrics dm = new DisplayMetrics();
         getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
         int width = dm.widthPixels;

         if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
        	 pendingAssessmentListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                     - getDipsFromPixel(5));
        	 completedAssessmentListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                     - getDipsFromPixel(5));
        	} else {
        		pendingAssessmentListView.setIndicatorBoundsRelative(width - getDipsFromPixel(35), width
                        - getDipsFromPixel(5));
        		completedAssessmentListView.setIndicatorBoundsRelative(width - getDipsFromPixel(35), width
                        - getDipsFromPixel(5));
        
        	}
         
     }
  
     // Convert pixel to dip
     public int getDipsFromPixel(float pixels) {
         // Get the screen's density scale
         final float scale = getResources().getDisplayMetrics().density;
         // Convert the dps to pixels, based on density scale
         return (int) (pixels * scale + 0.5f);
     }
	 
     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         
    	 View rootView = inflater.inflate(R.layout.fragment_assessment_detail, container, false);
         pendingAssessmentListView = (ExpandableListView) rootView.findViewById(R.id.pendingAssessmentList);
         completedAssessmentListView = (ExpandableListView) rootView.findViewById(R.id.completedAssessmentList);
         
         pendingTitle = (TextView) rootView.findViewById(R.id.pendingTitle);
         completedTitle = (TextView) rootView.findViewById(R.id.completedTitle);
         
         Log.v("", ""+this.pendingAssessmentArray);
         Log.v("", ""+this.completedAssessmentArray);
         pendingTitle.setText("Pending Assessment List ["+this.pendingAssessmentArray.length()+"]");
         completedTitle.setText("Completed Assessment List ["+this.completedAssessmentArray.length()+"]");
         
         if(this.pendingAssessmentArray.length() > 0)
         {
        	 AsessmentDetailsAdapter pendingAdapter = new AsessmentDetailsAdapter(this.getActivity(), pendingAssessmentArray);
        	 pendingAssessmentListView.setAdapter(pendingAdapter);
        	 pendingAdapter.setPendingAssessment(true);
        	 pendingAdapter.setAssessmentType(assessmentType);
        	 pendingAssessmentListView.setOnChildClickListener(this);
         }
         
         if(this.completedAssessmentArray.length() > 0)
         {
        	 AsessmentDetailsAdapter completedAdapter = new AsessmentDetailsAdapter(this.getActivity(), completedAssessmentArray);
        	 completedAssessmentListView.setAdapter(completedAdapter);
        	 completedAssessmentListView.setOnChildClickListener(this);
        	 completedAdapter.setAssessmentType(assessmentType);
         }
         this.setGroupIndicatorToRight();

         
         return rootView;
     }
     
     

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("AssessmentDetailsFragment", "position : "+ position);
		
		if(view == pendingAssessmentListView)
		{
			try {
				JSONObject assessmentObj = this.pendingAssessmentArray.getJSONObject(position);
				
				String access = assessmentObj.getString("asst_access");
				if(access.equalsIgnoreCase("true"))
				//if(true)
				{
					if(this.assessmentType == AssessmentType.INTERACTIVE)
					{
						
						String asstId = assessmentObj.getString("asst_id");
						String attempts = assessmentObj.getString("user_attempt");
						String duration = assessmentObj.getString("time_interval");
						String marks = assessmentObj.getString("user_marks");
						String asstDivId = assessmentObj.getString("asst_div_id");
						String passPercent = assessmentObj.getString("passing_per");
						
						LoadWebPageFragment fragment = new LoadWebPageFragment(asstId, asstDivId, attempts,duration,marks, passPercent);
						fragment.setScreenName(Screen.INTERACTIVE.ordinal());
						FragmentTransaction ft = getFragmentManager().beginTransaction();
						
						ft.addToBackStack("LoadWebPageFragment");
						ft.replace(R.id.content_frame, fragment).commit();
					}
					else
					{
					this.loadAssessmentQuestion(assessmentObj);
					}
				}
				else
				{
					String msg = assessmentObj.getString("d_message");
					Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
				}	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		else if(view == completedAssessmentListView)
		{
			
			
		}
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		 /*
		try {
			JSONObject msgObj = (JSONObject) sharedLearnList.get(position);
			
			Fragment fragment = new SharedLearningDetailsFragment(msgObj);

	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().addToBackStack("vg");
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		
	}

	private void loadAssessmentQuestion(final JSONObject assessmentObj) {
		
		
		new ApplicationAsk(getActivity(), new ApplicationService() {
			
			 private String resp, asst_id;
			 private JSONArray dataArray;
			@Override
			public void postExecute() {
								  
				  try { 
					  
					JSONObject caResponseObj = new JSONObject(resp);	
					JSONObject caResponseObj1 = new JSONObject(caResponseObj.getString("d"));
					Log.v(TAG," onPostExecute message : " +  caResponseObj1.getString("Status"));
					
					JSONObject dataObj = caResponseObj1.getJSONObject("data");
					dataArray = dataObj.getJSONArray("Table");
					
					if(dataArray.length() > 0)
					{
						 JSONObject assessmentQueObj = dataArray.getJSONObject(0);
						
						 if(assessmentType == AssessmentType.MISC)
						 {
							 MiscAssessmentQueListFragment fragment = new MiscAssessmentQueListFragment(assessmentQueObj);  
							 fragment.setAsstId(asst_id);
							 fragment.setOuterAssessmentObj(assessmentObj);
							 fragment.setAssessmentType(assessmentType);
							 FragmentManager fragmentManager = getFragmentManager();
							 FragmentTransaction ft = fragmentManager.beginTransaction();
							 ft.addToBackStack("AssessmentQueListFragment");
						     ft.replace(R.id.content_frame, fragment).commit();
						 }
						 else
						 {
							 
						 AssessmentQueListFragment fragment = new AssessmentQueListFragment(assessmentQueObj);  
						 fragment.setAsstId(asst_id);
						 fragment.setOuterAssessmentObj(assessmentObj);
						 fragment.setAssessmentType(assessmentType);
						 FragmentManager fragmentManager = getFragmentManager();
						 FragmentTransaction ft = fragmentManager.beginTransaction();
						 ft.addToBackStack("AssessmentQueListFragment");
					     ft.replace(R.id.content_frame, fragment).commit();
						 }
					}
					else
					{
						Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_LONG).show();
					}
					
					
					
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}
				  
				  
				 
			}
			
			@Override
			public void execute() {
				JSONObject inductionAssQueRequestObj = new JSONObject();
		        try {
		        	asst_id = assessmentObj.getString("asst_id");
					
					
					/*
		        	inductionAssQueRequestObj.putOpt("p_asst_id", "32");
					inductionAssQueRequestObj.putOpt("p_asst_div_id", "125");
					*/
					
				
	    		
		        String indutionAssQueURL = null;
		        
		        if(assessmentType == AssessmentType.MISC)
				{
		        	inductionAssQueRequestObj.putOpt("asst_id", asst_id);
					inductionAssQueRequestObj.putOpt("group_id", assessmentObj.getString("group_id"));
					indutionAssQueURL = URLGenerator.getMISCQue();
				}
				else
				{
					inductionAssQueRequestObj.putOpt("p_asst_id", asst_id);
					inductionAssQueRequestObj.putOpt("p_asst_div_id", assessmentObj.getString("asst_div_id"));
					indutionAssQueURL = URLGenerator.getInductionAssessmentQuestion();
				}
		        
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(indutionAssQueURL, inductionAssQueRequestObj.toString());
		  
		        } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}).execute();
		
		
		
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View view, int grpPosition,
			int childPosition, long id) {
		
		
		
Log.v("AssessmentDetailsFragment", "position : "+ grpPosition +" - "+childPosition);
		
		if(parent == pendingAssessmentListView || parent == completedAssessmentListView)
		{
			try {
				JSONObject assessmentObj = this.pendingAssessmentArray.getJSONObject(grpPosition);
//				this.loadAssessmentQuestion(assessmentObj);
				
				String access = assessmentObj.getString("asst_access");
				if(access.equalsIgnoreCase("true"))
				//if(true) [ For Testing ]
				{
					if(this.assessmentType == AssessmentType.INTERACTIVE)
					{
						
						String asstId = assessmentObj.getString("asst_id");
						String attempts = assessmentObj.getString("user_attempt");
						String duration = assessmentObj.getString("time_interval");
						String marks = assessmentObj.getString("user_marks");
						String asstDivId = assessmentObj.getString("asst_div_id");
						String passPercent = assessmentObj.getString("passing_per");
						
						LoadWebPageFragment fragment = new LoadWebPageFragment(asstId, asstDivId, attempts,duration,marks, passPercent);
						fragment.setScreenName(Screen.INTERACTIVE.ordinal());
						FragmentTransaction ft = getFragmentManager().beginTransaction();
						
						ft.addToBackStack("LoadWebPageFragment");
						ft.replace(R.id.content_frame, fragment).commit();
					}
					else
					{
						this.loadAssessmentQuestion(assessmentObj);
					}
				}
				else
				{
					String msg = assessmentObj.getString("d_message");
					Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
				}	
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		else if(parent == completedAssessmentListView)
		{
			/*
			try {
				JSONObject assessmentObj = this.completedAssessmentArray.getJSONObject(grpPosition);
				
				
					this.loadAssessmentQuestion(assessmentObj);
					
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
		}
		
		
		return false;
	}

	public void setAssessmentType(AssessmentType assessmentType) {
		this.assessmentType = assessmentType;
	}

	
	
}
