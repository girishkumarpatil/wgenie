package com.o2m.wockhard.assesment;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.AssessmentType;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.o2m.wockhard.wocktube.Utils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AssessmentAnsListFragment extends Fragment implements OnItemClickListener,OnClickListener {

	private String TAG = "AssessmentAnsListFragment";
	private ListView optionListView;
	private TextView questionTxtView, questionNoTxtView, timerTxtView, correctAnsTxtView;
	private Button nextBtn;
	private JSONObject assessmentObj, outerAssessmentObj;
	//private String pollId;
	
	private JSONObject ePollSubmitObj = new JSONObject();
	private JSONArray assessmentDataArray = new JSONArray();
	
	//private JSONArray assessmentAnsArray = new JSONArray();
	
	int totalQuestions, currentQueNo = 0, currentCorrectOption = 0, correctAns = 0;
	double percentage = 0.0;
	ArrayAdapter<String> arrayAdapter;
	ArrayList<String> optionList;
	boolean showAns;
	String asstId;
//	MyCountDownTimer myCountDownTimer;
	AssessmentType assessmentType;
	
	
	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	private JSONArray assessmentQuestionList;
	
	 public AssessmentAnsListFragment(JSONObject _assessmentObj) {
         // Empty constructor required for fragment subclasses
		 this.assessmentObj = _assessmentObj;
		/* totalQuestions = _ePollList.length();
		 this.pollId = _pollId;
		 */
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
    	 
         View rootView = inflater.inflate(R.layout.fragment_assessment_ans, container, false);
         optionListView = (ListView) rootView.findViewById(R.id.assessmentOptionList);
         questionTxtView = (TextView) rootView.findViewById(R.id.assessmentQuestion);
         questionNoTxtView = (TextView) rootView.findViewById(R.id.questionNo);
        // timerTxtView = (TextView) rootView.findViewById(R.id.timeLeft);
         correctAnsTxtView = (TextView) rootView.findViewById(R.id.correctAnsDetail);
         nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
         nextBtn.setOnClickListener(this); 
      //   TextView assessmentNameTxt = (TextView) rootView.findViewById(R.id.assessmentName);
         optionListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
      
         try {
        	
          //  assessmentNameTxt.setText(""+assessmentObj.getString("asst_name"));
			assessmentQuestionList = assessmentObj.getJSONArray("questions");
			totalQuestions = assessmentQuestionList.length();
			int time_interval = Integer.parseInt(assessmentObj.getString("time_interval"));
			setValuesfor(0);
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
         
         return rootView;
     }

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("AssessmentQueListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
	
		
	}

	@Override
	public void onClick(View v) {
		
		currentQueNo++;
		
		/*
		if(optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION)
		{
			Toast.makeText(this.getActivity(), " Please select answer then next ", Toast.LENGTH_SHORT).show();
			return;
		}
		else
		{
			this.captureAns();
		}
			*/
			
		if(currentQueNo < totalQuestions)
		{
			setValuesfor(currentQueNo);
		}
		else
		{
			//Toast.makeText(this.getActivity(), " asssessment finished ", Toast.LENGTH_SHORT).show();
			
			percentage = (correctAns * 100.0)/totalQuestions;
	//		submitePoll();
			
			new AlertDialog.Builder(getActivity())
			.setTitle("Assessment Finished")
			.setMessage("Done with All questions")
			.setPositiveButton("Back To Assessment", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 

					AssessmentAnsListFragment.this.getFragmentManager().popBackStack("AssesmentFunListFragment", 0);//popBackStack();

				}
			})
			
			.setIcon(android.R.drawable.ic_dialog_alert)
			.setCancelable(false)
			.show();
			
			
		}
	}

	private void setValuesfor(int questionNo)
	{
		//Toast.makeText(this.getActivity(), "Question No : "+(questionNo+1), Toast.LENGTH_SHORT).show();
		questionNoTxtView.setText("Question : "+(questionNo+1)+"/"+totalQuestions);
		try {
		if(questionNo !=0 )
		{
		/*String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		ansObj.put("quest_id",""+questionTxtView.getTag());
		ansObj.put("option", (optionListView.getCheckedItemPosition()+1));
		ePollDataArray.put(ansObj);*/
		
		}
		Log.v("EPollListFragment", ""+answersHashMap);
		
		JSONObject questionJsonObj;
		
			questionJsonObj = assessmentQuestionList.getJSONObject(questionNo);
			String question = questionJsonObj.getString("question");
			String questionId = questionJsonObj.getString("quest_id");
			
			String opt1 = questionJsonObj.getString("option1");
			String opt2 = questionJsonObj.getString("option2");
			String opt3 = questionJsonObj.getString("option3");
			String opt4 = questionJsonObj.getString("option4");
			String opt5 = questionJsonObj.getString("option5");
			//String opt6 = questionJsonObj.getString("opt6");
			
			currentCorrectOption = Integer.parseInt(questionJsonObj.getString("correct_option"));
			
			 optionList = new ArrayList<String>();
			if(!opt1.equals(""))
				optionList.add(opt1);
			if(!opt2.equals(""))
				optionList.add(opt2);
			if(!opt3.equals(""))
				optionList.add(opt3);
			if(!opt4.equals(""))
				optionList.add(opt4);
			if(!opt5.equals(""))
				optionList.add(opt5);
			/*if(!opt6.equals(""))
				optionList.add(opt6);*/
			
			//questionTxtView.setText(""+(questionNo+1)+")  "+question);
			questionTxtView.setText(""+question);
			questionTxtView.setTag(questionId);
			String correctAnsfor = questionJsonObj.getString("option"+currentCorrectOption);
			correctAnsTxtView.setText(correctAnsfor);
			//arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_checked, optionList);
			arrayAdapter = new ArrayAdapter(getActivity(), R.layout.epoll_ans_item, optionList);
			
	        optionListView.setAdapter(arrayAdapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void captureAns()
	{
		String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		try {
			int currentSelection = (optionListView.getCheckedItemPosition()+1);
			ansObj.put("QuestionID",""+questionTxtView.getTag());
			ansObj.put("selected_option", currentSelection);
			ansObj.put("CorrectAnswer", currentCorrectOption);
			
			if(currentSelection == currentCorrectOption)
			{
				correctAns++;
			}
			
			assessmentDataArray.put(ansObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	

	public String getAsstId() {
		return asstId;
	}

	public void setAsstId(String asstId) {
		this.asstId = asstId;
	}

	public JSONObject getOuterAssessmentObj() {
		return outerAssessmentObj;
	}

	public void setOuterAssessmentObj(JSONObject outerAssessmentObj) {
		this.outerAssessmentObj = outerAssessmentObj;
	}

	public void setAssessmentType(AssessmentType assessmentType) {
		this.assessmentType = assessmentType;
	}
}
