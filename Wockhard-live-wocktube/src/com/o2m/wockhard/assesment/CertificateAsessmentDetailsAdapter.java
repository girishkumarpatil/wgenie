package com.o2m.wockhard.assesment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.AssessmentType;
import com.o2m.wockhard.R;

public class CertificateAsessmentDetailsAdapter extends BaseExpandableListAdapter 
{

	Context context;

	ArrayList<String> arrayMessages;
	
	JSONArray pendingCertificateArray, assessmentArray;
	String unreadCount;
	AssessmentType assessmentType;
	boolean isPendingAssessment = false;

	
	public CertificateAsessmentDetailsAdapter(Context context, JSONArray _arrayassessment)
	{

		this.context = context;
		this.pendingCertificateArray = _arrayassessment;
	}

	private static class ChildViewHolder
	{

		public TextView txtView_courseTitle;
		//public TextView txtView_courseValue;
		public TextView txtView_marksTitle;
		//public TextView txtView_marksValue;
		public TextView txtView_statusTitle;
		//public TextView txtView_statusValue;
		
	}
	
	private static class GroupViewHolder
	{

		public TextView txtView_grpTitle;
		
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return pendingCertificateArray.length();
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		int assessmentCount = 0;
		try {
			JSONObject pendingCertificateObj = (JSONObject) pendingCertificateArray.get(groupPosition);
			assessmentCount = pendingCertificateObj.getJSONArray("assessment").length();
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		return assessmentCount;
		
	}
	
	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		try {
			
			JSONObject performanceObj = (JSONObject) pendingCertificateArray.get(groupPosition);
			
			return performanceObj;
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}



	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		GroupViewHolder holder = new GroupViewHolder();
		if (view == null)
		{

			holder = new GroupViewHolder();
			view = inflater.inflate(R.layout.assessment_grp_list_item, parent, false);
			holder.txtView_grpTitle = (TextView) view.findViewById(R.id.assessmentGrpTitle);
		
			view.setTag(holder);
		}
		else
		{
			holder = (GroupViewHolder) view.getTag();
		}
		
		try {
			
			JSONObject assessmentObj = (JSONObject) pendingCertificateArray.get(groupPosition);
				
			holder.txtView_grpTitle.setText("" + assessmentObj.getString("certificate_name"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(groupPosition %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		
	
		
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ChildViewHolder holder = new ChildViewHolder();
		if (view == null)
		{

			holder = new ChildViewHolder();
			if (isPendingAssessment) {
				
				view = inflater.inflate(R.layout.assessment_pending_child_list_item, parent, false);
			}
			else
			{
				view = inflater.inflate(R.layout.assessment_child_list_item, parent, false);
			}
			
			holder.txtView_courseTitle = (TextView) view.findViewById(R.id.assementCourseTitle);
			//holder.txtView_courseValue = (TextView) view.findViewById(R.id.assementCourseValue);
			holder.txtView_marksTitle = (TextView) view.findViewById(R.id.assementMarksTitle);
			//holder.txtView_marksValue = (TextView) view.findViewById(R.id.assementMarksValue);
			holder.txtView_statusTitle = (TextView) view.findViewById(R.id.assementStatusTitle);
			//holder.txtView_statusValue = (TextView) view.findViewById(R.id.assementStatusValue);
		
			view.setTag(holder);
		}
		else
		{
			holder = (ChildViewHolder) view.getTag();
		}
		
try {
			
	 //JSONObject assesmentObj = (JSONObject) pendingCertificateArray.get(groupPosition);
	
	
		JSONObject pendingCertificateObj = (JSONObject) pendingCertificateArray.get(groupPosition);
		JSONArray assesmentArray = pendingCertificateObj.getJSONArray("assessment");
		JSONObject assesmentObj = (JSONObject) assesmentArray.get(childPosition);
	
				
			if(assessmentType == AssessmentType.MISC)
			{
				holder.txtView_courseTitle.setText("Detail : "+assesmentObj.getString("Details"));
				holder.txtView_marksTitle.setText("Marks/Attempt : "+assesmentObj.getString("user_marks")+"/"+assesmentObj.getString("user_attempt"));
				holder.txtView_statusTitle.setText("Status : "+assesmentObj.getString("pass_failed"));
			}
			else if(assessmentType == AssessmentType.Individual)
			{
				holder.txtView_courseTitle.setText("Detail : "+assesmentObj.getString("Details"));
				holder.txtView_marksTitle.setText("Marks/Attempt : "+assesmentObj.getString("user_marks")+"/"+assesmentObj.getString("user_attempt"));
				holder.txtView_statusTitle.setText("Status : "+assesmentObj.getString("pass_failed"));
			}
			else
			{		
			
			holder.txtView_courseTitle.setText("Course : "+assesmentObj.getString("Course_name"));
			holder.txtView_marksTitle.setText("Marks/Attempt : "+assesmentObj.getString("user_marks")+"/"+assesmentObj.getString("user_attempt"));
			holder.txtView_statusTitle.setText("Status : "+assesmentObj.getString("pass_failed"));
			}
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	public void setAssessmentType(AssessmentType assessmentType) {
		this.assessmentType = assessmentType;
	}

	public boolean isPendingAssessment() {
		return isPendingAssessment;
	}

	public void setPendingAssessment(boolean isPendingAssessment) {
		this.isPendingAssessment = isPendingAssessment;
	}
	
	

}
