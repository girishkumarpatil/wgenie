package com.o2m.wockhard.assesment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class AssesmentFunAdapter extends BaseAdapter
{

	Context context;

	ArrayList<String> arrayMessages;

	String[] assessmentTitleArray;
	String unreadCount;

	//private ChannelDetails channelDetails;

	//public MessageAdapter(Context context, ArrayList<String> _arrayMessages)
	/*public MilestoneAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.milestoneTitleArray = _arrayMessages;
	}*/

	public AssesmentFunAdapter(Activity activity, String[] assessmentTitles) {
		// TODO Auto-generated constructor stub
		this.context = activity;
		this.assessmentTitleArray = assessmentTitles;
	}

	@Override
	public int getCount()
	{

		return assessmentTitleArray.length;
	}

	@Override
	public Object getItem(int position)
	{

		
			return assessmentTitleArray[position];
		
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();
//			view = inflater.inflate(R.layout.channel_list, parent, false);
			view = inflater.inflate(R.layout.assessmentfun_list_item, parent, false);
			holder.txtView_Title = (TextView) view.findViewById(R.id.assfunTitle);
	
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
		holder.txtView_Title.setText(this.assessmentTitleArray[position]);
		
		if(position %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_Title;
		
	}

}
