package com.o2m.wockhard.assesment;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.AssessmentType;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.o2m.wockhard.wocktube.Utils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AssessmentQueListFragment extends Fragment implements OnItemClickListener,OnClickListener {

	private String TAG = "AssessmentQueListFragment";
	private ListView optionListView;
	private TextView questionTxtView, questionNoTxtView, timerTxtView;
	private Button nextBtn;
	private JSONObject assessmentObj, outerAssessmentObj;
	//private String pollId;
	
	private JSONObject ePollSubmitObj = new JSONObject();
	private JSONArray assessmentDataArray = new JSONArray();
	
	//private JSONArray assessmentAnsArray = new JSONArray();
	
	int totalQuestions, currentQueNo = 0, currentCorrectOption = 0, correctAns = 0;
	double percentage = 0.0;
	ArrayAdapter<String> arrayAdapter;
	ArrayList<String> optionList;
	boolean showAns;
	String asstId;
	MyCountDownTimer myCountDownTimer;
	AssessmentType assessmentType;
	
	
	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	private JSONArray assessmentQuestionList;
	
	 public AssessmentQueListFragment(JSONObject _assessmentObj) {
         // Empty constructor required for fragment subclasses
		 this.assessmentObj = _assessmentObj;
		/* totalQuestions = _ePollList.length();
		 this.pollId = _pollId;
		 */
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
    	 
         View rootView = inflater.inflate(R.layout.fragment_assessment_que, container, false);
         optionListView = (ListView) rootView.findViewById(R.id.assessmentOptionList);
         questionTxtView = (TextView) rootView.findViewById(R.id.assessmentQuestion);
         questionNoTxtView = (TextView) rootView.findViewById(R.id.questionNo);
         timerTxtView = (TextView) rootView.findViewById(R.id.timeLeft);
         nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
         nextBtn.setOnClickListener(this); 
         TextView assessmentNameTxt = (TextView) rootView.findViewById(R.id.assessmentName);
         optionListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
      
         try {
        	
            assessmentNameTxt.setText(""+assessmentObj.getString("asst_name"));
			assessmentQuestionList = assessmentObj.getJSONArray("questions");
			totalQuestions = assessmentQuestionList.length();
			int time_interval = Integer.parseInt(assessmentObj.getString("time_interval"));
			myCountDownTimer = new MyCountDownTimer(time_interval*60*1000, 1000);
			myCountDownTimer.start();
			setValuesfor(0);
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
         
         return rootView;
     }

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("AssessmentQueListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
	
		
	}

	@Override
	public void onClick(View v) {
		
		currentQueNo++;
		
		if(optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION)
		{
			Toast.makeText(this.getActivity(), " Please select answer then next ", Toast.LENGTH_SHORT).show();
			return;
		}
		else
		{
			this.captureAns();
		}
			
		if(currentQueNo < totalQuestions)
		{
			setValuesfor(currentQueNo);
		}
		else
		{
			Toast.makeText(this.getActivity(), " asssessment finished ", Toast.LENGTH_SHORT).show();
			//Log.v(TAG, "correctAns" + correctAns);
			percentage = (correctAns * 100.0)/totalQuestions;
			
			Log.v(TAG, "correctAns : " + correctAns);
			Log.v(TAG, "totalQuestions : " + totalQuestions);
			Log.v(TAG, "percentage : " + percentage);
			
			submitePoll();
			
			
			
			
		}
	}

	private void submitePoll() {
		
			myCountDownTimer.cancel();
			
			
			new ApplicationAsk(getActivity(), new ApplicationService() {
				
				 private String resp;
				 private JSONArray faqArray;
				
				@Override
				public void postExecute() {
										  
					  try {
						  
						  
						JSONObject wTResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + wTResponseObj.getString("d"));
						
						JSONObject wTResponseObj1 = new JSONObject(wTResponseObj.getString("d"));
						
						String status = wTResponseObj1.getString("Status");
						
						if(status.equalsIgnoreCase("success"))
						{
							
							Toast.makeText(getActivity(), "Thank You for attempting Assessment ", Toast.LENGTH_LONG).show();
						    
							String title = "";
							String message = "";
							
							int passing = Integer.parseInt(assessmentObj.getString("pass_percent"));
							
							if(passing > percentage)
							{
								title = "Sorry";
								message = "You have failed to make passing grade!\n Your score is "+Utils.round(percentage, 1)+"%";

							}	
							else
							{
								title = "Congratulations";
								if(percentage >= 100)
								{
									//message = "You have Sucessed to make passing grade!\n Your score is 100%";
									message = "Success! You have made the passing grade.\n Your score is 100%";
								}
								else
								{
									//message = "You have Sucessed to make passing grade!\n Your score is "+Utils.round(percentage, 1)+"%";
									message = "Success! You have made the passing grade.\n Your score is "+Utils.round(percentage, 1)+"%";
								}

							}
														
								new AlertDialog.Builder(getActivity())
							    .setTitle(title)
							    .setMessage(message)
							    .setPositiveButton("Back To Assessment", new DialogInterface.OnClickListener() {
							        public void onClick(DialogInterface dialog, int which) { 
							           
							        	AssessmentQueListFragment.this.getFragmentManager().popBackStack("AssesmentFunListFragment", 0);//popBackStack();
							        	
							        }
							     })
							     .setNegativeButton("View Ans", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) { 

								AssessmentAnsListFragment fragment = new AssessmentAnsListFragment(AssessmentQueListFragment.this.assessmentObj);  
								fragment.setAssessmentType(AssessmentQueListFragment.this.assessmentType);
								FragmentManager fragmentManager = getFragmentManager();
								FragmentTransaction ft = fragmentManager.beginTransaction();
								ft.addToBackStack("AssessmentAnsListFragment");
								ft.replace(R.id.content_frame, fragment).commit();

							}
						})
							    .setIcon(android.R.drawable.ic_dialog_alert)
							    .setCancelable(false)
							     .show();
							
						}
						else
						{
							//failure
							Toast.makeText(getActivity(), "Assessment can't be submitted. Please try later ", Toast.LENGTH_LONG).show(); 
						}
						
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
						}
				
				@Override
				public void execute() {
					JSONObject assessmentSubmitRequest = new JSONObject();
					String asesmentType = null;
			        try {
			        	//ePollSubmitObj.put("employee_id", "8087");
			        	URLParams params = URLParams.getInstance();
			        	ePollSubmitObj.put("employee_id", params.getEmployee_id());
			        	ePollSubmitObj.put("UserId", params.getUser_id());
			        	
						ePollSubmitObj.put("AsstDivID", outerAssessmentObj.getString("asst_div_id"));
						asesmentType = outerAssessmentObj.getString("assessment_type");
						ePollSubmitObj.put("AssignType", asesmentType);
						int attempt = Integer.parseInt(outerAssessmentObj.getString("user_attempt"));
						attempt++;
						ePollSubmitObj.put("Attempt", ""+attempt);
						ePollSubmitObj.put("min", ""+AssessmentQueListFragment.this.myCountDownTimer.getMin());
						ePollSubmitObj.put("sec", ""+AssessmentQueListFragment.this.myCountDownTimer.getSec());
						ePollSubmitObj.put("Marks", "" + (int)percentage);
						
						int passing = Integer.parseInt(assessmentObj.getString("pass_percent"));
						
						if(passing > percentage)
							ePollSubmitObj.put("passed", "0");
						else
							ePollSubmitObj.put("passed", "1");
						
						ePollSubmitObj.put("Answer", assessmentDataArray);
						
										
		    		
			        String assessmentSubmitURL = null;
			        
			        /*if(assessmentType.equalsIgnoreCase("2"))//Individual
			        {
			        	assessmentSubmitURL = URLGenerator.submitIndividualAssessmentResult();
			        }
			        else if(assessmentType.equalsIgnoreCase("3"))//Induction
			        {
			        	assessmentSubmitURL = URLGenerator.submitInductionAssessmentResult();
			        }
			        else if(assessmentType.equalsIgnoreCase("5"))//wocktube
			        {
			        	assessmentSubmitURL = URLGenerator.submitWocktubeAssessmentResult();
			        }
			        else if(assessmentType.equalsIgnoreCase("0"))//Course Assessment
			        {
			        	//
			        	ePollSubmitObj.put("my_learning_asst_id", outerAssessmentObj.getString("my_learning_asst_id"));
			        	assessmentSubmitURL = URLGenerator.submitCourseAssessmentResult();
			        }*/
			        
			        if(assessmentType == AssessmentType.Individual || assessmentType == AssessmentType.PREPOST)//Individual
			        {
			        	
			        	assessmentSubmitURL = URLGenerator.submitIndividualAssessmentResult();
			        }
			        else if(assessmentType == AssessmentType.Induction)//Induction
			        {
			        	ePollSubmitObj.put("ind_asst_ID", outerAssessmentObj.getString("ind_asst_id"));
			        	ePollSubmitObj.put("ind_div_id", outerAssessmentObj.getString("ind_div_id"));
			        	assessmentSubmitURL = URLGenerator.submitInductionAssessmentResult();
			        }
			        else if(assessmentType == AssessmentType.Wocktube)//wocktube
			        {
			        	assessmentSubmitURL = URLGenerator.submitWocktubeAssessmentResult();
			        }
			        else if(assessmentType == AssessmentType.Certificate)//wocktube
			        {
			        	ePollSubmitObj.put("cert_main_id", outerAssessmentObj.getString("cert_main_id"));
			        	ePollSubmitObj.put("my_learning_asst_id", outerAssessmentObj.getString("my_learning_asst_id"));
			        	//ePollSubmitObj.put("p_old_user_id", params.getOld_user_id());
			        	assessmentSubmitRequest.put("p_old_user_id",  params.getOld_user_id());
			        	//p_old_user_id	
			        	assessmentSubmitURL = URLGenerator.submitCertificateAssessmentResult();
			        }
			        else if(assessmentType == AssessmentType.Course)//Course Assessment
			        {
			        	//
			        	ePollSubmitObj.put("my_learning_asst_id", outerAssessmentObj.getString("my_learning_asst_id"));
			        	assessmentSubmitURL = URLGenerator.submitCourseAssessmentResult();
			        }
			        
			        
			        assessmentSubmitRequest.put("json_input", ePollSubmitObj.toString());
			        
			        
			        CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(assessmentSubmitURL, assessmentSubmitRequest.toString());
			        
			        } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 
					
				}
			}).execute();
					
	}

	private void setValuesfor(int questionNo)
	{
		//Toast.makeText(this.getActivity(), "Question No : "+(questionNo+1), Toast.LENGTH_SHORT).show();
		questionNoTxtView.setText("Question : "+(questionNo+1)+"/"+totalQuestions);
		try {
		if(questionNo !=0 )
		{
		/*String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		ansObj.put("quest_id",""+questionTxtView.getTag());
		ansObj.put("option", (optionListView.getCheckedItemPosition()+1));
		ePollDataArray.put(ansObj);*/
		
		}
		Log.v("EPollListFragment", ""+answersHashMap);
		
		JSONObject questionJsonObj;
		
			questionJsonObj = assessmentQuestionList.getJSONObject(questionNo);
			String question = questionJsonObj.getString("question");
			String questionId = questionJsonObj.getString("quest_id");
			
			String opt1 = questionJsonObj.getString("option1");
			String opt2 = questionJsonObj.getString("option2");
			String opt3 = questionJsonObj.getString("option3");
			String opt4 = questionJsonObj.getString("option4");
			String opt5 = questionJsonObj.getString("option5");
			//String opt6 = questionJsonObj.getString("opt6");
			
			currentCorrectOption = Integer.parseInt(questionJsonObj.getString("correct_option"));
			
			 optionList = new ArrayList<String>();
			if(!opt1.equals(""))
				optionList.add(opt1);
			if(!opt2.equals(""))
				optionList.add(opt2);
			if(!opt3.equals(""))
				optionList.add(opt3);
			if(!opt4.equals(""))
				optionList.add(opt4);
			if(!opt5.equals(""))
				optionList.add(opt5);
			/*if(!opt6.equals(""))
				optionList.add(opt6);*/
			
			//questionTxtView.setText(""+(questionNo+1)+")  "+question);
			questionTxtView.setText(""+question);
			questionTxtView.setTag(questionId);
			
			//arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_checked, optionList);
			arrayAdapter = new ArrayAdapter(getActivity(), R.layout.epoll_item, optionList);
			
	        optionListView.setAdapter(arrayAdapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void captureAns()
	{
		String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		try {
			int currentSelection = (optionListView.getCheckedItemPosition()+1);
			ansObj.put("QuestionID",""+questionTxtView.getTag());
			ansObj.put("selected_option", currentSelection);
			ansObj.put("CorrectAnswer", currentCorrectOption);
			
			Log.v(TAG, "B4 correctAns : " + correctAns);
			if(currentSelection == currentCorrectOption)
			{
				correctAns++;
			}
			
			Log.v(TAG, "After correctAns : " + correctAns);
			assessmentDataArray.put(ansObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public class MyCountDownTimer extends CountDownTimer
	{
		long min , sec;

		public MyCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			new AlertDialog.Builder(getActivity())
		    .setTitle("Assessment")
		    .setMessage("Times Up")
		    .setPositiveButton("Back To Assessment", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		           
		        	AssessmentQueListFragment.this.getFragmentManager().popBackStack();
		        	
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert)
		     .show();
			
		}

		@Override
		public void onTick(long milliUntilFinished) {
			
			long totalSecs = milliUntilFinished/1000;
			
			 min = totalSecs/60;
			 sec = totalSecs%60;
			
			timerTxtView.setText("Time Remaining : "+min+":"+sec);
			
		}

		public long getMin() {
			return min;
		}

		public long getSec() {
			return sec;
		}
		
	}

	public String getAsstId() {
		return asstId;
	}

	public void setAsstId(String asstId) {
		this.asstId = asstId;
	}

	public JSONObject getOuterAssessmentObj() {
		return outerAssessmentObj;
	}

	public void setOuterAssessmentObj(JSONObject outerAssessmentObj) {
		this.outerAssessmentObj = outerAssessmentObj;
	}

	public void setAssessmentType(AssessmentType assessmentType) {
		this.assessmentType = assessmentType;
	}
}
