package com.o2m.wockhard.assesment;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.AssessmentType;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.o2m.wockhard.wocktube.Utils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MiscAssessmentQueListFragment extends Fragment implements OnItemClickListener,OnClickListener {

	private String TAG = "MiscAssessmentQueListFragment";
	private ListView optionListView;
	private TextView questionTxtView, questionNoTxtView, timerTxtView;
	private Button nextBtn;
	private JSONObject assessmentObj, outerAssessmentObj;
	private String pollId;
	
	private JSONObject ePollSubmitObj = new JSONObject();
	private JSONArray assessmentDataArray = new JSONArray();
	
	private JSONArray assessmentAnsArray = new JSONArray();
	MiscAnsAdapter miscAnsAdapter;
	
	String question_type = "0";
	
	int totalQuestions, currentQueNo = 0, correctAns = 0;
	double percentage = 0.0;
	ArrayAdapter<String> arrayAdapter;
	ArrayList<String> optionList;
	boolean showAns;
	String asstId, currentCorrectAnswer;
	MyCountDownTimer myCountDownTimer;
	AssessmentType assessmentType;
	
	
	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	//private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	private JSONArray assessmentQuestionList;
	
	 public MiscAssessmentQueListFragment(JSONObject _assessmentObj) {
         // Empty constructor required for fragment subclasses
		 this.assessmentObj = _assessmentObj;
		/* totalQuestions = _ePollList.length();
		 this.pollId = _pollId;
		 */
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
    	 
         View rootView = inflater.inflate(R.layout.fragment_misc_assessment_que, container, false);
         optionListView = (ListView) rootView.findViewById(R.id.assessmentOptionList);
         questionTxtView = (TextView) rootView.findViewById(R.id.assessmentQuestion);
         questionNoTxtView = (TextView) rootView.findViewById(R.id.questionNo);
         timerTxtView = (TextView) rootView.findViewById(R.id.timeLeft);
         nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
         nextBtn.setOnClickListener(this); 
         TextView assessmentNameTxt = (TextView) rootView.findViewById(R.id.assessmentName);
         
      
         try {
        	
            assessmentNameTxt.setText(""+assessmentObj.getString("asst_name"));
			assessmentQuestionList = assessmentObj.getJSONArray("questions");
			totalQuestions = assessmentQuestionList.length();
			int time_interval = Integer.parseInt(assessmentObj.getString("time_interval"));
			myCountDownTimer = new MyCountDownTimer(time_interval*60*1000, 1000);
			myCountDownTimer.start();
			setValuesfor(0);
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
         
         return rootView;
     }

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("AssessmentQueListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
	
		
	}

	@Override
	public void onClick(View v) {
		
		currentQueNo++;
		
		if(optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION && question_type.equalsIgnoreCase("0"))
		{
			Toast.makeText(this.getActivity(), " Please select answer then next ", Toast.LENGTH_SHORT).show();
			return;
		}
		else if(optionListView.getCheckedItemCount() < 1  && question_type.equalsIgnoreCase("1"))
		{
			Toast.makeText(this.getActivity(), " Please select answer then next ", Toast.LENGTH_SHORT).show();
			return;
		}
		else
		{
			this.captureAns();
		}
			
		if(currentQueNo < totalQuestions)
		{
			setValuesfor(currentQueNo);
		}
		else
		{
			Toast.makeText(this.getActivity(), " asssessment finished ", Toast.LENGTH_SHORT).show();
			
			percentage = (correctAns * 100.0)/totalQuestions;
			submitePoll();
			
			
			
			
		}
	}

	private void submitePoll() {
		
	
			
			
			new ApplicationAsk(getActivity(), new ApplicationService() {
				
				 private String resp;
				 private JSONArray faqArray;
				
				@Override
				public void postExecute() {
										  
					  try {
						  
						  
						JSONObject wTResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + wTResponseObj.getString("d"));
						
						JSONObject wTResponseObj1 = new JSONObject(wTResponseObj.getString("d"));
						
						String status = wTResponseObj1.getString("Status");
						
						if(status.equalsIgnoreCase("success"))
						{
							
							Toast.makeText(getActivity(), "Thank You for attempting Assessment ", Toast.LENGTH_LONG).show();
						    
							String title = "";
							String message = "";
							
							int passing = Integer.parseInt(assessmentObj.getString("pass_percent"));
							
							if(passing > percentage)
							{
								title = "Sorry";
								message = "You have failed to make passing grade!\n Your score is "+Utils.round(percentage, 1) +"%";

							}	
							else
							{
								title = "Congratulations";
    							message = "You have Sucessed to make passing grade!\n Your score is "+Utils.round(percentage, 1)+"%";

							}
														
								new AlertDialog.Builder(getActivity())
							    .setTitle(title)
							    .setMessage(message)
							    .setPositiveButton("Back To Assessment", new DialogInterface.OnClickListener() {
							        public void onClick(DialogInterface dialog, int which) { 
							        	MiscAssessmentQueListFragment.this.getFragmentManager().popBackStack("AssesmentFunListFragment", 0);
							        	//MiscAssessmentQueListFragment.this.getFragmentManager().popBackStack();
							        	
							        }
							     })
							     .setNegativeButton("View Ans", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) { 

								MiscAssessmentAnsListFragment fragment = new MiscAssessmentAnsListFragment(MiscAssessmentQueListFragment.this.assessmentObj);  
								fragment.setAssessmentType(MiscAssessmentQueListFragment.this.assessmentType);
								FragmentManager fragmentManager = getFragmentManager();
								FragmentTransaction ft = fragmentManager.beginTransaction();
								ft.addToBackStack("AssessmentAnsListFragment");
								ft.replace(R.id.content_frame, fragment).commit();

							}
						})
							    .setIcon(android.R.drawable.ic_dialog_alert)
							     .show();
							
						}
						else
						{
							//failure
							Toast.makeText(getActivity(), "Assessment can't be submitted. Please try later ", Toast.LENGTH_LONG).show(); 
						}
						
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
						}
				
				@Override
				public void execute() {
					JSONObject assessmentSubmitRequest = new JSONObject();
					String assessmentType = null;
			        try {
			        	//ePollSubmitObj.put("employee_id", "8087");
			        	URLParams params = URLParams.getInstance();
			        	ePollSubmitObj.put("employee_id", params.getEmployee_id());
			        	ePollSubmitObj.put("UserId", params.getUser_id());
			        	ePollSubmitObj.put("group_id", outerAssessmentObj.getString("group_id"));
						ePollSubmitObj.put("asst_id", outerAssessmentObj.getString("asst_id"));
						//ePollSubmitObj.put("ind_div_id", outerAssessmentObj.getString("ind_div_id"));
						//assessmentType = outerAssessmentObj.getString("assessment_type");
						//ePollSubmitObj.put("AssignType", assessmentType);
						int attempt = Integer.parseInt(outerAssessmentObj.getString("user_attempt"));
						attempt++;
						ePollSubmitObj.put("Attempt", ""+attempt);
						ePollSubmitObj.put("min", ""+MiscAssessmentQueListFragment.this.myCountDownTimer.getMin());
						ePollSubmitObj.put("sec", ""+MiscAssessmentQueListFragment.this.myCountDownTimer.getSec());
						ePollSubmitObj.put("Marks", "" + (int)percentage);
						
						int passing = Integer.parseInt(assessmentObj.getString("pass_percent"));
						
						if(passing > percentage)
							ePollSubmitObj.put("passed", "0");
						else
							ePollSubmitObj.put("passed", "1");
						
						ePollSubmitObj.put("Answers", assessmentDataArray);
						
										
		    		
			        String assessmentSubmitURL = URLGenerator.submitMISCAssessmentResult();
			        		        
			        assessmentSubmitRequest.put("json_input", ePollSubmitObj.toString());
			        
			        
			        CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(assessmentSubmitURL, assessmentSubmitRequest.toString());
			        
			        } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 
					
				}
			}).execute();
					
	}

	private void setValuesfor(int questionNo)
	{
		//Toast.makeText(this.getActivity(), "Question No : "+(questionNo+1), Toast.LENGTH_SHORT).show();
		questionNoTxtView.setText("Question : "+(questionNo+1)+"/"+totalQuestions);
		try {
		if(questionNo !=0 )
		{
		/*String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		ansObj.put("quest_id",""+questionTxtView.getTag());
		ansObj.put("option", (optionListView.getCheckedItemPosition()+1));
		ePollDataArray.put(ansObj);*/
		
		}
		//Log.v("EPollListFragment", ""+answersHashMap);
		
		JSONObject questionJsonObj;
		
			questionJsonObj = assessmentQuestionList.getJSONObject(questionNo);
			String question = questionJsonObj.getString("question");
			String questionId = questionJsonObj.getString("quest_id");
			question_type = questionJsonObj.getString("type");
			String opt1 = questionJsonObj.getString("option1");
			String opt2 = questionJsonObj.getString("option2");
			String opt3 = questionJsonObj.getString("option3");
			String opt4 = questionJsonObj.getString("option4");
			String opt5 = questionJsonObj.getString("option5");
			String opt6 = questionJsonObj.getString("option6");
			String opt7 = questionJsonObj.getString("option7");
			
			//currentCorrectOption = Integer.parseInt(questionJsonObj.getString("correct_option"));
			currentCorrectAnswer = questionJsonObj.getString("CorrectAnswer");
			 optionList = new ArrayList<String>();
			if(!opt1.equals(""))
				optionList.add(opt1);
			if(!opt2.equals(""))
				optionList.add(opt2);
			if(!opt3.equals(""))
				optionList.add(opt3);
			if(!opt4.equals(""))
				optionList.add(opt4);
			if(!opt5.equals(""))
				optionList.add(opt5);
			if(!opt6.equals(""))
				optionList.add(opt6);
			
			//questionTxtView.setText(""+(questionNo+1)+")  "+question);
			questionTxtView.setText(""+question);
			questionTxtView.setTag(questionId);
			
			//arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_checked, optionList);
			arrayAdapter = new ArrayAdapter(getActivity(), R.layout.epoll_item, optionList);
			
			if(question_type.equalsIgnoreCase("0"))
			{
				optionListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
				optionListView.setAdapter(arrayAdapter);
			}
			else if(question_type.equalsIgnoreCase("1"))
			{
				optionListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				optionListView.setAdapter(arrayAdapter);
			}
			else if(question_type.equalsIgnoreCase("2"))
			{
				optionListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
				miscAnsAdapter = new MiscAnsAdapter(getActivity(), optionList);
				optionListView.setAdapter(miscAnsAdapter);
			}
			
	        
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void captureAns()
	{
		/*String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		*/
		JSONObject ansObj = new JSONObject();
		String selectedAnswer = null;
		try {
			
			ansObj.put("QuestionID",""+questionTxtView.getTag());
			ansObj.put("question_type",""+question_type);
			ansObj.put("CorrectAnswer", currentCorrectAnswer);
			
			if(question_type.equalsIgnoreCase("0"))
			{
				int currentSelection = (optionListView.getCheckedItemPosition()+1);
				/*selectedAnswer = ""+currentSelection;
				ansObj.put("selected_option", currentSelection);
				*/
                StringBuilder selectedOption = new StringBuilder();
				 
				 for (int i = 1; i <= 7; i++) {
			            
					 int selection = currentSelection == i ? 1 : 0;
					 
					   String subAns = ""+i+"-"+selection;
			           selectedOption.append(subAns);  
			           if(i != 7)
			           {
			        	   selectedOption.append(",");
			           }
			           
			        }
				 selectedAnswer = selectedOption.toString();
				 ansObj.put("selected_option", selectedOption);
				
			}
			else if(question_type.equalsIgnoreCase("1"))
			{
				 SparseBooleanArray checked = optionListView.getCheckedItemPositions();
				 
				 StringBuilder selectedOption = new StringBuilder();
				 
				 for (int i = 1; i <= 7; i++) {
			            
					 int selection = checked.get(i - 1) ? 1 : 0;
					 
					   String subAns = ""+i+"-"+selection;
			           selectedOption.append(subAns);  
			           if(i != 7)
			           {
			        	   selectedOption.append(",");
			           }
			           
			        }
				 selectedAnswer = selectedOption.toString();
				 ansObj.put("selected_option", selectedOption);
				
			}
			else if(question_type.equalsIgnoreCase("2"))
			{				
				selectedAnswer = miscAnsAdapter.getSelectedAns();
				ansObj.put("selected_option", miscAnsAdapter.getSelectedAns());
			}
			
		
			
			
			if(selectedAnswer.equalsIgnoreCase(currentCorrectAnswer))
			{
				correctAns++;
			}
			

			assessmentDataArray.put(ansObj);
			
			Log.v(TAG, ""+ assessmentDataArray);
			Log.v(TAG, "correctAns : "+ correctAns);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public class MyCountDownTimer extends CountDownTimer
	{
		long min , sec;

		public MyCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			new AlertDialog.Builder(getActivity())
		    .setTitle("Assessment")
		    .setMessage("Times Up")
		    .setPositiveButton("Back To Assessment", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		           
		        	MiscAssessmentQueListFragment.this.getFragmentManager().popBackStack();
		        	
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert)
		    .setCancelable(false)
		     .show();
			
		}

		@Override
		public void onTick(long milliUntilFinished) {
			
			long totalSecs = milliUntilFinished/1000;
			
			 min = totalSecs/60;
			 sec = totalSecs%60;
			
			timerTxtView.setText("Time Remaining : "+min+":"+sec);
			
		}

		public long getMin() {
			return min;
		}

		public long getSec() {
			return sec;
		}
		
	}

	public String getAsstId() {
		return asstId;
	}

	public void setAsstId(String asstId) {
		this.asstId = asstId;
	}

	public JSONObject getOuterAssessmentObj() {
		return outerAssessmentObj;
	}

	public void setOuterAssessmentObj(JSONObject outerAssessmentObj) {
		this.outerAssessmentObj = outerAssessmentObj;
	}

	public void setAssessmentType(AssessmentType assessmentType) {
		this.assessmentType = assessmentType;
	}
	
	
}
