package com.o2m.wockhard.assesment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class MiscAnsAdapter extends BaseAdapter
{

	Context context;

	ArrayList<String> ansOptions;
	List<String> filteredSelectionList;
	String orderedOptions[] = new String[]{"0","0","0","0","0","0","0"};
	public MiscAnsAdapter(Activity activity, ArrayList<String> _ansOptions) {
		
		this.context = activity;
		this.ansOptions = _ansOptions;
		ArrayList<String> selectionList =  new ArrayList<String>();
		 selectionList.add("select order");
		 selectionList.add("1");
		 selectionList.add("2");
		 selectionList.add("3");
		 selectionList.add("4");
		 selectionList.add("5");
		 selectionList.add("6");
		 selectionList.add("7");
		 filteredSelectionList = selectionList.subList(0, (this.ansOptions.size()+1));

	}

	@Override
	public int getCount()
	{
		return ansOptions.size();
	}

	@Override
	public Object getItem(int position)
	{

		
			return ansOptions.get(position);
		
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();
			view = inflater.inflate(R.layout.misc_ans_list_item, parent, false);
			holder.txtView_Title = (TextView) view.findViewById(R.id.miscAnsTitle);
			holder.optionSpinner = (Spinner) view.findViewById(R.id.optionSpinner);			 			 
			ArrayAdapter<String> selectionAdapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, filteredSelectionList);
			holder.optionSpinner.setAdapter(selectionAdapter);
	
			holder.optionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

	            @Override
	            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

	                String items = parent.getSelectedItem().toString();
	                Log.i("Selected item : ", items);
	                if(!items.equalsIgnoreCase("select order"))
	                orderedOptions[position] = items; 
	                Log.i("orderedOptions : ", ""+ Arrays.toString(orderedOptions));
	                //Validation for duplicate selection can be added.
	            }

	            @Override
	            public void onNothingSelected(AdapterView<?> arg0) {

	            }

	        });
			
			
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}

		
		holder.txtView_Title.setText(this.ansOptions.get(position));
		
		
		
		return view;

	}
	
	public String getSelectedAns()
	{
		StringBuilder selectedOption = new StringBuilder();
		for (int i = 0; i < orderedOptions.length; i++) {
			
			selectedOption.append(orderedOptions[i]);
			if(i != (orderedOptions.length - 1))
				selectedOption.append("-");
		}
		
		return selectedOption.toString();
	}
	

	private static class ViewHolder
	{

		public TextView txtView_Title;
		public Spinner optionSpinner;
		
	}

}
