package com.o2m.wockhard.assesment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class PrevCertificateAsessmentDetailsAdapter extends BaseExpandableListAdapter 
{

	Context context;

	ArrayList<String> arrayMessages;

	JSONArray assessmentArray;
	String unreadCount;

	
	public PrevCertificateAsessmentDetailsAdapter(Context context, JSONArray _arrayassessment)
	{

		this.context = context;
		this.assessmentArray = _arrayassessment;
	}

	private static class ChildViewHolder
	{

		public TextView txtView_courseTitle;
		//public TextView txtView_courseValue;
		public TextView txtView_marksTitle;
		//public TextView txtView_marksValue;
		public TextView txtView_statusTitle;
		//public TextView txtView_statusValue;
		
	}
	
	private static class GroupViewHolder
	{

		public TextView txtView_grpTitle;
		
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return assessmentArray.length();
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		
		return 1;
		
	}
	
	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		try {
			
			JSONObject performanceObj = (JSONObject) assessmentArray.get(groupPosition);
			
			return performanceObj;
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}



	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		GroupViewHolder holder = new GroupViewHolder();
		if (view == null)
		{

			holder = new GroupViewHolder();
			view = inflater.inflate(R.layout.assessment_grp_list_item, parent, false);
			holder.txtView_grpTitle = (TextView) view.findViewById(R.id.assessmentGrpTitle);
		
			view.setTag(holder);
		}
		else
		{
			holder = (GroupViewHolder) view.getTag();
		}
		
		try {
			
			JSONObject assessmentObj = (JSONObject) assessmentArray.get(groupPosition);
				
			holder.txtView_grpTitle.setText("" + assessmentObj.getString("certificate_name"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(groupPosition %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		
	
		
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ChildViewHolder holder = new ChildViewHolder();
		if (view == null)
		{

			holder = new ChildViewHolder();
			view = inflater.inflate(R.layout.assessment_child_list_item, parent, false);
			holder.txtView_courseTitle = (TextView) view.findViewById(R.id.assementCourseTitle);
			//holder.txtView_courseValue = (TextView) view.findViewById(R.id.assementCourseValue);
			holder.txtView_marksTitle = (TextView) view.findViewById(R.id.assementMarksTitle);
			//holder.txtView_marksValue = (TextView) view.findViewById(R.id.assementMarksValue);
			holder.txtView_statusTitle = (TextView) view.findViewById(R.id.assementStatusTitle);
			//holder.txtView_statusValue = (TextView) view.findViewById(R.id.assementStatusValue);
		
			view.setTag(holder);
		}
		else
		{
			holder = (ChildViewHolder) view.getTag();
		}
		
try {
			
			JSONObject assesmentObj = (JSONObject) assessmentArray.get(groupPosition);
				
			holder.txtView_courseTitle.setText("Assessment : "+assesmentObj.getString("certificate_name"));
			holder.txtView_marksTitle.setText("Status : "+assesmentObj.getString("status"));
			holder.txtView_statusTitle.setText("Expired On : "+assesmentObj.getString("expire_on"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
	

}
