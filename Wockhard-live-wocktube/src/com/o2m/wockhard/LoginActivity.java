package com.o2m.wockhard;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.wockhard.HomeActivity.Screen;
import com.o2m.wockhard.wocktube.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {

	private String TAG = "LoginActivity";

	private Button loginBtn, forgotPasswordBtn;
	private EditText userNameEditText, passwordEditText;

	String divCode, roleId, feedback;

	@Override
	public void onClick(View arg0) {

		if (arg0 == loginBtn) {
			// new LoginTask().execute("8081","1");
			validateLogin(userNameEditText.getText().toString(),
					passwordEditText.getText().toString());
			// Toast.makeText(this, "login", Toast.LENGTH_LONG).show();
		} else if (arg0 == forgotPasswordBtn) {
			// Toast.makeText(this, "Not yet implemented",
			// Toast.LENGTH_LONG).show();
			Intent homeIntent = new Intent(this, GridHomeActivity.class);
			startActivity(homeIntent);

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		userNameEditText = (EditText) findViewById(R.id.editTxt_UsrName);
		passwordEditText = (EditText) findViewById(R.id.editTxt_PWD);
		loginBtn = (Button) findViewById(R.id.btn_Login);
		// forgotPasswordBtn = (Button) findViewById(R.id.btn_forgot_pass);

		SharedPreferences sharedPref = this
				.getPreferences(Context.MODE_PRIVATE);
		String offlineName = sharedPref.getString("uname", "");
		String offlinePass = sharedPref.getString("upwd", "");

		if (!offlinePass.equals(""))
			userNameEditText.setText(offlineName);

		if (!offlineName.equals(""))
			passwordEditText.setText(offlinePass);

		loginBtn.setOnClickListener(this);
		// forgotPasswordBtn.setOnClickListener(this);
		
		int b = 11, c = 10;
		double a = 0.0;
		a = (b * 100)/c;
		Log.v(TAG, "a : " + a + "b : " + b + "C : " + c);
		String message = "";
		
		Log.v(TAG, "0-->> " + Utils.round(a, 1));
		
		if(a >= 100)
			message = "You have Sucessed to make passing grade!\n Your score is 100%";
		else
			message = "You have Sucessed to make passing grade!\n Your score is " + Utils.round(a, 1) + "%";
		
		Log.v(TAG, "message : "+ message);
		
		
	}

	void login() {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/*
	 * private class LoginTask extends AsyncTask<String, String, String> {
	 * 
	 * private String resp;
	 * 
	 * @Override protected String doInBackground(String... params) {
	 * 
	 * JSONObject loginObj = new JSONObject(); // JSONObject inboxObj = new
	 * JSONObject(); try { loginObj.put("p_username", params[0]);
	 * loginObj.put("p_pwd", params[1]);
	 * 
	 * 
	 * } catch (JSONException e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * String loginURL =
	 * "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user"
	 * ; //String loginURL =
	 * "http://wockhardt.oh22media.com/myservice/service1.asmx/GetDivision";
	 * //String inboxURL =
	 * "http://wockhardt.oh22media.com/myservice/service1.asmx/GetInboxMessage_new"
	 * ; //application/x-www-form-urlencoded CommunicationService commService =
	 * new CommunicationService(); resp = commService.sendSyncRequest(loginURL,
	 * loginObj.toString());
	 * 
	 * return resp; }
	 * 
	 * 
	 * @Override protected void onPostExecute(String result) {
	 * 
	 * Log.v(TAG," onPostExecute Login Response : " + result);
	 * 
	 * try { JSONObject loginResponseObj = new JSONObject(result); JSONObject
	 * loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
	 * String status = loginResponseObj1.getString("Status");
	 * 
	 * if(status.equalsIgnoreCase("success")) {
	 * 
	 * } else {
	 * 
	 * }
	 * 
	 * 
	 * } catch (JSONException e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * 
	 * 
	 * }
	 * 
	 * 
	 * 
	 * }
	 */
	private void validateLogin(final String username, final String password) {
		new ApplicationAsk(this, new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute Login Response : " + resp);

				try {

					SharedPreferences sharedPref = LoginActivity.this
							.getPreferences(Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putString("uname", username);
					editor.putString("upwd", password);
					editor.commit();
					JSONObject loginResponseObj = new JSONObject(resp);
					JSONObject loginResponseObj1 = new JSONObject(
							loginResponseObj.getString("d"));
					String status = loginResponseObj1.getString("Status");

					if (status.equalsIgnoreCase("success")) {

						JSONObject dataObj = loginResponseObj1
								.getJSONObject("data");
						JSONArray dataArray = dataObj.getJSONArray("Table");

						URLParams.getInstance().setLoginResponse(
								dataArray.getJSONObject(0));
						roleId = URLParams.getInstance().getRole_id();
						divCode = URLParams.getInstance().getDiv_code();
						String levelCode = URLParams.getInstance()
								.getLevel_code();

						Log.v(TAG, "roleId : " + roleId + "divCode : "
								+ divCode);

						if (roleId.equals("1") || roleId.equals("6")) {

							fetchDivCode();

						} else if (roleId.equals("3")) {
							fetchDivCodeForL4L5();
						} else {

							if (levelCode.equalsIgnoreCase("L1")
									|| levelCode.equalsIgnoreCase("L2")) {
								loadFeedbackScreen();

							} else {
								Intent homeIntent = new Intent(
										LoginActivity.this,
										GridHomeActivity.class);
								startActivity(homeIntent);
							}
						}

						// fetchUserList();
					} else {
						Toast.makeText(
								LoginActivity.this,
								"Error :"
										+ loginResponseObj1
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject loginObj = new JSONObject();
				try {
					loginObj.put("p_username", username);
					loginObj.put("p_pwd", password);
					// loginObj.put("", "");
				} catch (JSONException e) {

					e.printStackTrace();
				}

				// URLGenerator.getLoginUrl();

				String loginURL = URLGenerator.getLoginUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				// String loginURL =
				// "http://training.wockhardt.com/mobile/service1.asmx/validate_sql_user";

				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(loginURL,
						loginObj.toString());

				// return resp;

			}
		}).execute();
	}

	protected void fetchDivCodeForL4L5() {
		new ApplicationAsk(this, new ApplicationService() {

			private String resp, feedbackresp;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute fetchDivCodeForL4L5 Response : "
						+ resp);

				try {

					JSONObject divl4l5ResponseObj = new JSONObject(resp);
					JSONObject divl4l5ResponseObj1 = new JSONObject(
							divl4l5ResponseObj.getString("d"));
					String status = divl4l5ResponseObj1.getString("Status");

					if (status.equalsIgnoreCase("success")) {

						JSONObject dataObj = divl4l5ResponseObj1
								.getJSONObject("data");
						JSONArray divJsonArray = dataObj.getJSONArray("Table");

						// URLParams.getInstance().setLoginResponse(dataArray.getJSONObject(0));
						// save this json array
						URLParams.getInstance().setDivMap(
								convertJsonArrayToMap(divJsonArray));
						// Intent homeIntent = new Intent(LoginActivity.this,
						// GridHomeActivity.class);
						// startActivity(homeIntent);
						if (feedbackresp != null) {
							JSONObject fbResponseObj = new JSONObject(
									feedbackresp);
							JSONObject fbResponseObj1 = new JSONObject(
									fbResponseObj.getString("d"));
							JSONObject fbdataObj = fbResponseObj1
									.getJSONObject("data");
							JSONArray fbJsonArray = fbdataObj
									.getJSONArray("Table");
							JSONObject fbObj = (JSONObject) fbJsonArray.get(0);
							feedback = fbObj.getString("feedback");

							LoginActivity.this.loadFirstScreen();
						} else {
							Intent homeIntent = new Intent(LoginActivity.this,
									GridHomeActivity.class);
							startActivity(homeIntent);

						}

					} else {
						Toast.makeText(
								LoginActivity.this,
								"Error :"
										+ divl4l5ResponseObj1
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject divcodObj = new JSONObject();
				try {
					divcodObj.put("p_employee_id", URLParams.getInstance()
							.getEmployee_id());

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String divisionl4l5URL = URLGenerator.getDivision_L5_L4_User();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";

				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(divisionl4l5URL,
						divcodObj.toString());

				String feedbackURL = URLGenerator.WOW_Check_feedback_display();
				URLParams params = URLParams.getInstance();
				String levelCode = params.getLevel_code();

				if (levelCode.equalsIgnoreCase("L1")
						|| levelCode.equalsIgnoreCase("L2")) {
					JSONObject fbObj = new JSONObject();
					try {

						fbObj.put("p_employee_id", params.getEmployee_id());
						fbObj.put("p_level_code", params.getLevel_code());
						fbObj.put("p_doj", params.getDoj());
						feedbackresp = commService.sendSyncRequest(feedbackURL,
								fbObj.toString());

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}).execute();

	}

	private void fetchDivCode() {

		new ApplicationAsk(this, new ApplicationService() {

			private String resp, feedbackresp = null;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute fetchDivCodeForL4L5 Response : "
						+ resp);

				try {

					JSONObject divl4l5ResponseObj = new JSONObject(resp);
					JSONObject divl4l5ResponseObj1 = new JSONObject(
							divl4l5ResponseObj.getString("d"));
					String status = divl4l5ResponseObj1.getString("Status");

					if (status.equalsIgnoreCase("success")) {

						JSONObject dataObj = divl4l5ResponseObj1
								.getJSONObject("data");
						JSONArray divJsonArray = dataObj.getJSONArray("Table");

						// URLParams.getInstance().setLoginResponse(dataArray.getJSONObject(0));
						// save this json array
						URLParams.getInstance().setDivMap(
								convertJsonArrayToMap(divJsonArray));

						if (feedbackresp != null) {
							JSONObject fbResponseObj = new JSONObject(
									feedbackresp);
							JSONObject fbResponseObj1 = new JSONObject(
									fbResponseObj.getString("d"));
							JSONObject fbdataObj = fbResponseObj1
									.getJSONObject("data");
							JSONArray fbJsonArray = fbdataObj
									.getJSONArray("Table");
							JSONObject fbObj = (JSONObject) fbJsonArray.get(0);
							feedback = fbObj.getString("feedback");
							LoginActivity.this.loadFirstScreen();
						} else {
							Intent homeIntent = new Intent(LoginActivity.this,
									GridHomeActivity.class);
							startActivity(homeIntent);
						}

					} else {
						Toast.makeText(
								LoginActivity.this,
								"Error :"
										+ divl4l5ResponseObj1
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject divcodObj = new JSONObject();

				String divisionURL = URLGenerator.getAllDivision();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";

				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(divisionURL,
						divcodObj.toString());

				String feedbackURL = URLGenerator.WOW_Check_feedback_display();
				URLParams params = URLParams.getInstance();
				String levelCode = params.getLevel_code();

				if (levelCode.equalsIgnoreCase("L1")
						|| levelCode.equalsIgnoreCase("L2")) {
					JSONObject fbObj = new JSONObject();
					try {

						fbObj.put("p_employee_id", params.getEmployee_id());
						fbObj.put("p_level_code", params.getLevel_code());
						fbObj.put("p_doj", params.getDoj());
						feedbackresp = commService.sendSyncRequest(feedbackURL,
								fbObj.toString());

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}).execute();

	}

	private HashMap<String, String> convertJsonArrayToMap(JSONArray divJsonArray) {
		HashMap<String, String> divMap = new HashMap<String, String>();
		try {

			for (int i = 0; i < divJsonArray.length(); i++) {

				JSONObject divJsonObj;

				divJsonObj = divJsonArray.getJSONObject(i);
				divMap.put(divJsonObj.getString("division"),
						divJsonObj.getString("div_code"));

			}

		} catch (JSONException e) {

			e.printStackTrace();
		}
		return divMap;
	}

	public void loadFirstScreen() {

		if (feedback == null) {
			Intent homeIntent = new Intent(LoginActivity.this,
					GridHomeActivity.class);
			startActivity(homeIntent);
			return;

		}

		if (feedback.equalsIgnoreCase("2")) {
			Intent homeIntent = new Intent(LoginActivity.this,
					BaseActivity.class);
			homeIntent.putExtra("SCREEN", Screen.Wow.ordinal());
			startActivity(homeIntent);
		} else if (feedback.equalsIgnoreCase("1")) {
			Intent homeIntent = new Intent(LoginActivity.this,
					BaseActivity.class);
			homeIntent.putExtra("SCREEN", Screen.Feedback.ordinal());
			startActivity(homeIntent);
		} else if (feedback.equalsIgnoreCase("0")) {
			Intent homeIntent = new Intent(LoginActivity.this,
					GridHomeActivity.class);
			startActivity(homeIntent);
		} else {
			Intent homeIntent = new Intent(LoginActivity.this,
					GridHomeActivity.class);
			startActivity(homeIntent);
		}

	}

	public void loadFeedbackScreen() {

		new ApplicationAsk(this, new ApplicationService() {

			String feedbackresp;

			@Override
			public void postExecute() {

				try {

					JSONObject fbResponseObj = new JSONObject(feedbackresp);
					JSONObject fbResponseObj1 = new JSONObject(fbResponseObj
							.getString("d"));
					JSONObject fbdataObj = fbResponseObj1.getJSONObject("data");
					String status = fbResponseObj1.getString("Status");

					if (status.equalsIgnoreCase("success")) {
						JSONArray fbJsonArray = fbdataObj.getJSONArray("Table");
						JSONObject fbObj = (JSONObject) fbJsonArray.get(0);
						feedback = fbObj.getString("feedback");
						LoginActivity.this.loadFirstScreen();

					} else {
						Toast.makeText(
								LoginActivity.this,
								"Error :" + fbResponseObj1.getString("message"),
								Toast.LENGTH_LONG).show();

					}
				} catch (Exception e) {

				}

			}

			@Override
			public void execute() {

				String feedbackURL = URLGenerator.WOW_Check_feedback_display();
				URLParams params = URLParams.getInstance();
				String levelCode = params.getLevel_code();
				CommunicationService commService = new CommunicationService();

				JSONObject fbObj = new JSONObject();
				try {

					fbObj.put("p_employee_id", params.getEmployee_id());
					fbObj.put("p_level_code", params.getLevel_code());
					fbObj.put("p_doj", params.getDoj());
					feedbackresp = commService.sendSyncRequest(feedbackURL,
							fbObj.toString());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// }
		}).execute();
	}

	/*
	 * private void fetchUserList() { new ApplicationAsk(this, new
	 * ApplicationService() {
	 * 
	 * private String resp;
	 * 
	 * @Override public void postExecute() {
	 * Log.v(TAG," onPostExecute Login Response : " + resp);
	 * 
	 * try {
	 * 
	 * 
	 * JSONObject loginResponseObj = new JSONObject(resp); JSONObject
	 * loginResponseObj1 = new JSONObject(loginResponseObj.getString("d"));
	 * String status = loginResponseObj1.getString("Status");
	 * 
	 * if(status.equalsIgnoreCase("success")) {
	 * 
	 * JSONObject dataObj = loginResponseObj1.getJSONObject("data"); JSONArray
	 * dataArray = dataObj.getJSONArray("Table");
	 * 
	 * URLParams.getInstance().setUserResponse(dataArray.getJSONObject(0));
	 * 
	 * Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
	 * startActivity(homeIntent); } else { Toast.makeText(LoginActivity.this,
	 * "Error :"+loginResponseObj1.getString("message"),
	 * Toast.LENGTH_LONG).show(); }
	 * 
	 * 
	 * } catch (JSONException e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * }
	 * 
	 * @Override public void execute() { JSONObject userObj = new JSONObject();
	 * try {
	 * 
	 * URLParams params = URLParams.getInstance(); userObj.put("p_employee_id",
	 * params.getEmployee_id()); userObj.put("p_div_code",
	 * params.getDiv_code()); userObj.put("p_level_code",
	 * params.getLevel_code()); //p_level_code //loginObj.put("", ""); } catch
	 * (JSONException e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * //URLGenerator.getLoginUrl();
	 * 
	 * String loginURL = URLGenerator.getUserListForEmployeeUrl();//
	 * "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user"
	 * ; //String loginURL =
	 * "http://training.wockhardt.com/mobile/service1.asmx/validate_sql_user";
	 * 
	 * CommunicationService commService = new CommunicationService(); resp =
	 * commService.sendSyncRequest(loginURL, userObj.toString());
	 * 
	 * // return resp;
	 * 
	 * } }).execute(); }
	 */

}
