package com.o2m.wockhard;

import java.util.ArrayList;
import java.util.HashMap;

import com.o2m.wockhard.HomeActivity.Screen;
import com.o2m.wockhard.sharedlearning.SharedLearningActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class GridHomeActivity extends Activity implements OnClickListener {

	GridView gridView;
	Button saveBtn, closeBtn;
	AlertDialog alertDialog;
	URLParams params = URLParams.getInstance();
	ArrayList<String> lvlList, divList;
	HashMap<String, String> divmap;
	String selectedDiv, selectedLevel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		// ORMDroidApplication.initialize(this.getApplication());
		gridView = (GridView) findViewById(R.id.gvMenu);
		int optIcon[] = { R.drawable.hmenu_mylearning,
				R.drawable.hmenu_my_assessments, R.drawable.hmenu_epoll,
				R.drawable.hmenu_case_study, R.drawable.hmenu_wbook,
				R.drawable.hmenu_inbox, R.drawable.hmenu_wocktube,
				R.drawable.hmenu_faqs, R.drawable.hmenu_training_path,
				R.drawable.hmenu_performance_wall, R.drawable.hmenu_avatar,
				R.drawable.hmenu_library };
		Resources res = getResources();
		String optName[] = res.getStringArray(R.array.menu_title_array);
		
		String pselectedLevel = params.getLevel_code();
		if(pselectedLevel.equalsIgnoreCase("L4") || pselectedLevel.equalsIgnoreCase("L5") )
		{
			params.setLevel_code("L1");
		}
		
		
		gridView.setAdapter(new GridMenuAdapter(this, optIcon, optName));

		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				// position = position--;
				// if(position == Screen.BABY_INFO.ordinal())
				if (position == 0) {
					Intent assessmentIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					assessmentIntent.putExtra("SCREEN",
							Screen.MyLearning.ordinal());
					startActivity(assessmentIntent);
				}
				// else if(position == Screen.MESSAGES.ordinal())
				else if (position == 1) {
					Intent assessmentIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					assessmentIntent.putExtra("SCREEN",
							Screen.Assessment.ordinal());
					startActivity(assessmentIntent);
				}
				// else if(position == Screen.PLANNER.ordinal())
				else if (position == 2) {
					Intent inboxIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					inboxIntent.putExtra("SCREEN", Screen.ePoll.ordinal());
					startActivity(inboxIntent);
				}
				// else if(position == Screen.PREGNANCY_DIET.ordinal())
				else if (position == 3) {
					Intent wockTubeIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					wockTubeIntent.putExtra("SCREEN",
							Screen.CaseStudy.ordinal());
					startActivity(wockTubeIntent);
				}
				// else if(position == Screen.FREE_SHOPEE.ordinal())
				else if (position == 4) {
					Intent inboxIntent = new Intent(GridHomeActivity.this,
							SharedLearningActivity.class);
					startActivity(inboxIntent);
				}
				// else if(position == Screen.PREGNANCY_EXCERCISE.ordinal())
				else if (position == 5) {
					Intent inboxIntent = new Intent(GridHomeActivity.this,
							InboxActivity.class);
					startActivity(inboxIntent);
				}
				// else if(position == Screen.NEW_BORN_ESSENTIALS.ordinal())
				else if (position == 6) {
					Intent wockTubeIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					wockTubeIntent.putExtra("SCREEN", Screen.Wocktube.ordinal());
					startActivity(wockTubeIntent);
				}
				// else if(position == Screen.BABY_NAMES.ordinal())
				else if (position == 7) {
					Intent inboxIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					inboxIntent.putExtra("SCREEN", Screen.FAQ.ordinal());
					startActivity(inboxIntent);
				}
				// else if(position == Screen.HOSPITAL_CHECKLIST.ordinal())
				else if (position == 8)// training
				{
					Intent trainingIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					trainingIntent.putExtra("SCREEN", Screen.Training.ordinal());
					startActivity(trainingIntent);
				}
				// else if(position == Screen.VACCINATION.ordinal())
				else if (position == 9) {
					Intent inboxIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					inboxIntent.putExtra("SCREEN",
							Screen.PerformanceWall.ordinal());
					startActivity(inboxIntent);
				}
				// else if(position == Screen.QUERRIES.ordinal())
				else if (position == 10)// avatar
				{
					Intent avatarIntent = new Intent(GridHomeActivity.this,
							BaseActivity.class);
					avatarIntent.putExtra("SCREEN", Screen.Avatar.ordinal());
					startActivity(avatarIntent);
				}
				// else if(position == Screen.SETTING.ordinal())
				else if (position == 11)// classroom
				{
					 Intent intent = new
					 Intent(GridHomeActivity.this,BaseActivity.class);
					 intent.putExtra("SCREEN", Screen.Library.ordinal());
					 startActivity(intent);
				} else {
					Toast.makeText(GridHomeActivity.this, "Under development",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		params = URLParams.getInstance();
		String roleId = params.getRole_id();

		ImageButton settingBtn = (ImageButton) findViewById(R.id.setting);
		// levelSpinner = (Spinner) findViewById(R.id.levelSpinner);

		if (roleId.equals("1") || roleId.equals("6") || roleId.equals("3")) {
			settingBtn.setVisibility(View.VISIBLE);
		} else {
			settingBtn.setVisibility(View.INVISIBLE);
		}

	}

	public void settingButton(View v) {

		/*
		 * MyPreference.setMyIntPref(this,
		 * Constants.SharedPreference.USER_ACTIVE, 1); Intent intent = new
		 * Intent(this, LoginActivity.class);
		 * intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
		 * Intent.FLAG_ACTIVITY_NEW_TASK); startActivity(intent); finish();
		 */

		LayoutInflater li = LayoutInflater.from(this);

		View promptsView = li.inflate(R.layout.my_dialog_layout, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder.setView(promptsView);

		// set dialog message

		// alertDialogBuilder.setTitle("My Dialog..");
		// alertDialogBuilder.setIcon(R.drawable.ic_launcher);
		// create alert dialog
		alertDialog = alertDialogBuilder.create();

		final Spinner mdivSpinner = (Spinner) promptsView
				.findViewById(R.id.divisionSpinner);

		final Spinner mlevelspinner = (Spinner) promptsView
				.findViewById(R.id.levelSpinner);

		saveBtn = (Button) promptsView.findViewById(R.id.saveBtn);
		closeBtn = (Button) promptsView.findViewById(R.id.closeBtn);

		saveBtn.setOnClickListener(this);
		closeBtn.setOnClickListener(this);
		// reference UI elements from my_dialog_layout in similar fashion

		mdivSpinner.setOnItemSelectedListener(new OnSpinnerItemClicked());
		mlevelspinner.setOnItemSelectedListener(new OnSpinnerItemClicked());

		// show it
		alertDialog.show();
		alertDialog.setCanceledOnTouchOutside(false);

		divmap = params.getDivMap();
		divList = new ArrayList<String>(divmap.keySet());
		ArrayAdapter<String> divAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, divList);
		mdivSpinner.setAdapter(divAdapter);

		ArrayList<String> levelList = new ArrayList<String>();
		levelList.add("L1");
		levelList.add("L2");
		levelList.add("L3");

		lvlList = new ArrayList<String>(levelList);
		ArrayAdapter<String> lvlAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, lvlList);
		mlevelspinner.setAdapter(lvlAdapter);

		int divPosition = 0, lvlPosition = 0;

		String divName = HomeActivity.getKeyFromValue(divmap,
				params.getDiv_code());
        selectedDiv = divName;
		
		divPosition = divList.indexOf(divName);
		lvlPosition = lvlList.indexOf(params.getLevel_code());

		selectedLevel = params.getLevel_code();
		 
		mdivSpinner.setSelection(divPosition);
		mlevelspinner.setSelection(lvlPosition);
		
		/*
		if(selectedLevel.equalsIgnoreCase("L4") || selectedLevel.equalsIgnoreCase("L5") )
		{
			params.setLevel_code("L1");
			String pselectedDiv = divmap.get(divList.get(0));
			params.setDiv_code(pselectedDiv);
			//params.setDiv_code(params.getDiv_code());
		}
		*/
	}

	public class OnSpinnerItemClicked implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
//			Toast.makeText(parent.getContext(),
//					"Clicked : " + parent.getItemAtPosition(pos).toString(),
//					Toast.LENGTH_LONG).show();
			if (parent.getId() == R.id.divisionSpinner) {
				
				selectedDiv = divmap.get(divList.get(pos));
				//params.setDiv_code(divmap.get(divList.get(pos)));
			} else if (parent.getId() == R.id.levelSpinner) {
				//params.setLevel_code(lvlList.get(pos));
				selectedLevel = lvlList.get(pos);
				
			}

		}

		@Override
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	@Override
	public void onClick(View v) {

		if (v == saveBtn) {
			
			params.setDiv_code(selectedDiv);
			params.setLevel_code(selectedLevel);
			alertDialog.dismiss();
			// params.setLevel_code(lvlList.get(pos));
		} else if (v == closeBtn) {
			alertDialog.dismiss();
		}

	}

}
