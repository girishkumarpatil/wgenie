package com.o2m.wockhard;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.wochhard.casestudy.CaseStudyFragment;
import com.o2m.wockhard.HomeActivity.Screen;
import com.o2m.wockhard.assesment.AssesmentFunListFragment;
import com.o2m.wockhard.epoll.EPollListFragment;
import com.o2m.wockhard.epoll.EPollResultListFragment;
import com.o2m.wockhard.faqs.FaqListFragment;
import com.o2m.wockhard.inbox.MessageListFragment;
import com.o2m.wockhard.milestones.MilestoneListFragment;
import com.o2m.wockhard.performancewall.PerformanceWallListFragment;
import com.o2m.wockhard.sharedlearning.SharedLearningActivity;
import com.o2m.wockhard.sharedlearning.SharedLearningListFragment;
import com.o2m.wockhard.wocktube.WocktubeListFragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class BaseActivity extends Activity {

	private String TAG = "BaseActivity";
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private String[] mMenuTitles;
	private int i;
	int screenPosition;

	private URLParams urlParameter = URLParams.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		mMenuTitles = getResources().getStringArray(R.array.menu_array);
		// set up the drawer's list view with items and click listener

		NavigationDrawerListAdapter customAdapter = new NavigationDrawerListAdapter(
				this, mMenuTitles);
		// mDrawerList.setAdapter(new ArrayAdapter<String>(this,
		// R.layout.drawer_list_item, mMenuTitles));
		mDrawerList.setAdapter(customAdapter);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		Intent mIntent = getIntent();
		screenPosition = mIntent.getIntExtra("SCREEN", 10);
		selectItem(screenPosition);
	}

	public void menuButton(View v) {
		if (i == 0) {
			mDrawerLayout.openDrawer(mDrawerList);
			i = 1;
		} else {
			i = 0;
			mDrawerLayout.closeDrawer(mDrawerList);
		}

	}

	public void homeButton(View v) {
		Intent homeIntent = new Intent(this, GridHomeActivity.class);
		startActivity(homeIntent);
	}

	private class DrawerItemClickListener implements
	ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
			
			selectItem(position);
		}
	}

	private void selectItem(final int position) {

		// Toast.makeText(this, "---> "+position, Toast.LENGTH_SHORT).show();
		
		if(position >= 0 && position < 8)
			this.clearStack();
		

		switch (position) {
		// case
		case 0:
			this.loadePoll();

			break;
			// ePoll
		case 1:
			this.loadCaseStudy();
			break;
			// wocktube
		case 2:
			this.loadSL();

			break;
			// inbox
			
		case 3:
			this.loadAssessment();
			break;
			
		case 4:

			this.loadInbox();
			break;
			// sl
		case 5:
			//this.loadWocktube();
			this.loadNewWocktube();
			break;
			// faq
		case 6:

			this.loadFaq();
			break;
			// milestone
		case 7:

			this.loadMilestone();
			break;
			// performance wall
		case 8:

			this.loadPerformancewall();
		/*	Fragment fragment = new TestFragment();
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
			.replace(R.id.content_frame, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(screenPosition, true);
			setTitle(mMenuTitles[screenPosition]);
			mDrawerLayout.closeDrawer(mDrawerList);*/
			break;
			
		case 9:		
			this.loadMyLearning();
		break;
		
		case 10:
			this.loadWow();
		break;
		
		case 11:
			this.loadFeedback();
			break;
		case 12:
			this.loadTraining();
			break;
		case 13:
			this.loadAvatar();
		break;
		case 14:
			this.loadLibrary();
		break;
		

		default:

			Toast.makeText(this, "Invalid configuration ", Toast.LENGTH_SHORT)
			.show();
			break;
		}

	}

	private void loadLibrary() {
		// TODO Auto-generated method stub
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.Library.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
	}

	private void loadMyLearning() {
	
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.MyLearning.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
	}
	
	private void loadWow() {
		
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.Wow.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
	}
	
	private void loadFeedback() {
		
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.Feedback.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
	}
	
private void loadTraining() {
		
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.Training.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
	}

private void loadAvatar() {
	
	LoadWebPageFragment fragment = new LoadWebPageFragment();
	fragment.setScreenName(Screen.Avatar.ordinal());
	FragmentTransaction ft = getFragmentManager().beginTransaction();
	
	ft.addToBackStack("LoadWebPageFragment");
	ft.replace(R.id.content_frame, fragment).commit();
	
}

/*private void loadNewWocktube() {
	
	LoadWebPageFragment fragment = new LoadWebPageFragment();
	fragment.setScreenName(Screen.Wocktube.ordinal());
	FragmentTransaction ft = getFragmentManager().beginTransaction();
	
	ft.addToBackStack("LoadWebPageFragment");
	ft.replace(R.id.content_frame, fragment).commit();
	
}*/
	private void loadAssessment() {
		
		Fragment fragment = new AssesmentFunListFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("AssesmentFunListFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
	}

	private void loadCaseStudy() {

		new ApplicationAsk(this, new ApplicationService() {

			private String resp;
			private JSONArray faqArray;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute loadCaseStudy Response : " + resp);

				try {

					JSONObject csResponseObj = new JSONObject(resp);

					Log.v(TAG,
							" onPostExecute Status : "
									+ csResponseObj.getString("d"));

					JSONObject csResponseObj1 = new JSONObject(
							csResponseObj.getString("d"));

					Log.v(TAG,
							" onPostExecute message : "
									+ csResponseObj1.getString("Status"));

					JSONObject dataObj = csResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table");
					
					if(faqArray.length() <= 0)
					{
						Toast.makeText(BaseActivity.this, "No data found", Toast.LENGTH_SHORT).show();
						return;
					}
					
					JSONObject caseStudyObj = (JSONObject) faqArray.get(0);
					
					Fragment csF = new CaseStudyFragment(caseStudyObj);
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.addToBackStack("CaseStudyFragment");
					ft.replace(R.id.content_frame, csF).commit();
					
					// getFragmentManager().beginTransaction().commit();
					mDrawerList.setItemChecked(screenPosition, true);
					setTitle(mMenuTitles[screenPosition]);
					mDrawerLayout.closeDrawer(mDrawerList);
					

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(BaseActivity.this, "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject csRequestObj = new JSONObject();
				try {

					/*
					 * csRequestObj.putOpt("p_div_code", "E");
					 * csRequestObj.putOpt("p_level_code", "L1");
					 */
					csRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					csRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					csRequestObj.putOpt("p_level_code",
							urlParameter.getLevel_code());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String csURL =
				// "http://wockhardt.oh22media.com/myservice/service1.asmx/getCaseStudyByDivDesg";
				String csURL = URLGenerator.getCaseStudy();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(csURL,
						csRequestObj.toString());

			}
		}).execute();

	}

	private void loadePoll() {
		new ApplicationAsk(this, new ApplicationService() {

			private String resp;
			private JSONArray faqArray;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute inbox Response : " + resp);

				try {

					JSONObject epollResponseObj = new JSONObject(resp);

					Log.v(TAG,
							" onPostExecute Status : "
									+ epollResponseObj.getString("d"));

					JSONObject epollResponseObj1 = new JSONObject(
							epollResponseObj.getString("d"));

					Log.v(TAG,
							" onPostExecute message : "
									+ epollResponseObj1.getString("Status"));

					String epollResponseStatus = epollResponseObj1
							.getString("Status");
					
					if (!epollResponseStatus.equalsIgnoreCase("Success")) {
						Toast.makeText(BaseActivity.this,
								epollResponseObj1.getString("message"),
								Toast.LENGTH_LONG).show();
						return;
					} else {
						JSONObject dataObj = epollResponseObj1
								.getJSONObject("data");
						faqArray = dataObj.getJSONArray("Table");
						if (faqArray == null) {
							Toast.makeText(BaseActivity.this,
									"Data Parsing Error", Toast.LENGTH_SHORT)
									.show();
							return;
						}
						if (faqArray.length() <= 0) {
							Toast.makeText(BaseActivity.this,
									"No data to Display", Toast.LENGTH_SHORT)
									.show();
							return;
						}

						JSONObject ePollResponse = (JSONObject) faqArray.get(0);

						if (ePollResponse.get("status").equals("Pending")) {
							String pollId = ePollResponse.getString("Poll_id");
							JSONArray ePollQueArray = ePollResponse
									.getJSONArray("Questions");
							EPollListFragment fragment = new EPollListFragment(
									ePollQueArray, pollId);

							// JSONArray ePollResultArray =
							// ePollResponse.getJSONArray("Result");
							// EPollResultListFragment fragment = new
							// EPollResultListFragment(ePollResultArray);

							if (ePollResponse.getString("Poll_ans").equals("0")) {
								// 0 : hide ans 1 : shw ans

								fragment.setShowAns(false);
							} else {
								fragment.setShowAns(true);
							}

							FragmentTransaction ft = getFragmentManager().beginTransaction();
							ft.addToBackStack("EPollListFragment");
							
							ft.replace(R.id.content_frame, fragment)
							.commit();
						} else {// Result

							JSONArray ePollResultArray = ePollResponse
									.getJSONArray("Result");
							EPollResultListFragment fragment = new EPollResultListFragment(
									ePollResultArray);

							if (ePollResponse.getString("Poll_ans").equals("0")) {
								// 0 : hide ans 1 : shw ans

								fragment.setShowAns(false);
							} else {
								fragment.setShowAns(true);
							}

							FragmentTransaction ft = getFragmentManager().beginTransaction();
							ft.addToBackStack("EPollResultListFragment");
							ft.replace(R.id.content_frame, fragment).commit();

							/*
							 * String pollId =
							 * ePollResponse.getString("Poll_id"); JSONArray
							 * ePollQueArray =
							 * ePollResponse.getJSONArray("Questions");
							 * EPollListFragment fragment = new
							 * EPollListFragment(ePollQueArray, pollId);
							 * 
							 * // JSONArray ePollResultArray =
							 * ePollResponse.getJSONArray("Result"); //
							 * EPollResultListFragment fragment = new
							 * EPollResultListFragment(ePollResultArray);
							 * 
							 * if(ePollResponse.getString("Poll_ans").equals("0")
							 * ) { //0 : hide ans 1 : shw ans
							 * 
							 * fragment.setShowAns(false); } else {
							 * fragment.setShowAns(true); }
							 * 
							 * 
							 * FragmentManager fragmentManager =
							 * getFragmentManager();
							 * fragmentManager.beginTransaction
							 * ().replace(R.id.content_frame,
							 * fragment).commit();
							 */

						}

					}
				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(BaseActivity.this, "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(screenPosition, true);
				setTitle(mMenuTitles[screenPosition]);
				mDrawerLayout.closeDrawer(mDrawerList);

			}

			@Override
			public void execute() {
				JSONObject ePollRequestObj = new JSONObject();
				try {
					/*
					 * ePollRequestObj.putOpt("p_employee_id", "0");
					 * ePollRequestObj.putOpt("p_div_code", "E");
					 * ePollRequestObj.putOpt("p_level_code", "L1");
					 */

					ePollRequestObj.putOpt("p_employee_id",
							urlParameter.getEmployee_id());
					ePollRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					ePollRequestObj.putOpt("p_level_code",
							urlParameter.getLevel_code());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String ePollURL =
				// "http://wockhardt.oh22media.com/myservice/service1.asmx/GetEpollFrontend";
				String ePollURL = URLGenerator.getEPoll();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(ePollURL,
						ePollRequestObj.toString());

			}
		}).execute();
	}

	private void loadInbox() {
		Intent inboxIntent = new Intent(this, InboxActivity.class);
		startActivity(inboxIntent);
	}

	private void loadSL() {
		Intent inboxIntent = new Intent(this, SharedLearningActivity.class);
		startActivity(inboxIntent);
	}

	private void loadWocktube() {
		
		new ApplicationAsk(this, new ApplicationService() {

			private String resp;
			private JSONArray faqArray;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute inbox Response : " + resp);

				try {

					JSONObject wTResponseObj = new JSONObject(resp);

					Log.v(TAG,
							" onPostExecute Status : "
									+ wTResponseObj.getString("d"));

					JSONObject wTResponseObj1 = new JSONObject(
							wTResponseObj.getString("d"));

					Log.v(TAG,
							" onPostExecute message : "
									+ wTResponseObj1.getString("Status"));

					JSONObject dataObj = wTResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table");
					
					Fragment fragment = new WocktubeListFragment(faqArray);
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.addToBackStack("WocktubeListFragment");
					ft.replace(R.id.content_frame, fragment).commit();

					// update selected item and title, then close the drawer
					mDrawerList.setItemChecked(screenPosition, true);
					setTitle(mMenuTitles[screenPosition]);
					mDrawerLayout.closeDrawer(mDrawerList);

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(BaseActivity.this, "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void execute() {
				JSONObject wocktubeRequestObj = new JSONObject();
				try {
					/*
					 * wocktubeRequestObj.putOpt("p_div_code", "T1");
					 * wocktubeRequestObj.putOpt("p_desg_level", "L1");
					 */
					wocktubeRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());
					wocktubeRequestObj.putOpt("p_desg_level",
							urlParameter.getLevel_code());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String wocktubeURL =
				// "http://wockhardt.oh22media.com/myservice/service1.asmx/GetWocktubeForFrontend";
				String wocktubeURL = URLGenerator.getWockTube();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(wocktubeURL,
						wocktubeRequestObj.toString());

			}
		}).execute();
		
	}

	private void loadFaq() {
		new ApplicationAsk(this, new ApplicationService() {

			private String resp;
			private JSONArray faqArray;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute inbox Response : " + resp);

				try {

					JSONObject faqResponseObj = new JSONObject(resp);

					Log.v(TAG,
							" onPostExecute Status : "
									+ faqResponseObj.getString("d"));

					JSONObject faqResponseObj1 = new JSONObject(
							faqResponseObj.getString("d"));

					Log.v(TAG,
							" onPostExecute message : "
									+ faqResponseObj1.getString("Status"));

					JSONObject dataObj = faqResponseObj1.getJSONObject("data");
					faqArray = dataObj.getJSONArray("Table");

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(BaseActivity.this, "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

				Fragment fragment = new FaqListFragment(faqArray);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.addToBackStack("FaqListFragment");
				ft.replace(R.id.content_frame, fragment).commit();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(screenPosition, true);
				setTitle(mMenuTitles[screenPosition]);
				mDrawerLayout.closeDrawer(mDrawerList);

			}

			@Override
			public void execute() {
				JSONObject faqRequestObj = new JSONObject();
				try {
					// faqRequestObj.putOpt("p_div_code", "T1");
					faqRequestObj.putOpt("p_div_code",
							urlParameter.getDiv_code());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String faqURL =
				// "http://wockhardt.oh22media.com/myservice/service1.asmx/GetFAQForFrontendByDivision";
				String faqURL = URLGenerator.getFAQ();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(faqURL,
						faqRequestObj.toString());

			}
		}).execute();
	}

	private void loadMilestone() {
		Fragment fragment = new MilestoneListFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("MilestoneListFragment");
		ft.replace(R.id.content_frame, fragment).commit();

	}

	private void loadPerformancewall() {
		
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.PerformanceWall.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
   /*new ApplicationAsk(this, new ApplicationService() {

			private String resp;
			private JSONArray pwArray;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute inbox Response : " + resp);

				try {

					JSONObject pwResponseObj = new JSONObject(resp);

					Log.v(TAG,
							" onPostExecute Status : "
									+ pwResponseObj.getString("d"));

					JSONObject pwResponseObj1 = new JSONObject(
							pwResponseObj.getString("d"));

					Log.v(TAG,
							" onPostExecute message : "
									+ pwResponseObj1.getString("Status"));

					JSONObject dataObj = pwResponseObj1.getJSONObject("data");
					pwArray = dataObj.getJSONArray("Table");

				} catch (JSONException e) {

					e.printStackTrace();
					Toast.makeText(BaseActivity.this, "Data Parsing Error",
							Toast.LENGTH_LONG).show();
				}

				Fragment fragment = new PerformanceWallListFragment(pwArray);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.addToBackStack("PerformanceWallListFragment");
				ft.replace(R.id.content_frame, fragment).commit();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(screenPosition, true);
				setTitle(mMenuTitles[screenPosition]);
				mDrawerLayout.closeDrawer(mDrawerList);

			}

			@Override
			public void execute() {
				JSONObject pwRequestObj = new JSONObject();
				try {
					pwRequestObj.putOpt("p_performance_id", "0");
					pwRequestObj.putOpt("p_div_code", urlParameter.getDiv_code());
					

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String pwWallURL =
				// "http://wockhardt.oh22media.com/myservice/service1.asmx/App_GetPerformanceWall";
				String pwWallURL = URLGenerator.getPerformanceWall();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(pwWallURL,
						pwRequestObj.toString());

			}
		}).execute();
*/	}
	
	private void loadNewWocktube() {
		
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		fragment.setScreenName(Screen.Wocktube.ordinal());
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment).commit();
		
	}
	
	 @Override
		public boolean onKeyDown(int keyCode, KeyEvent event)
		{
			if (keyCode == KeyEvent.KEYCODE_BACK)
			{

				if (getFragmentManager().getBackStackEntryCount() == 1)
				{
					this.finish();
					//return false;
				}
				else
				{

					getFragmentManager().popBackStack();
					removeCurrentFragment();
					return false;
				}

			}
			return super.onKeyDown(keyCode, event);
		}
	 
		public void removeCurrentFragment()
		{
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			Fragment currentFrag = getFragmentManager().findFragmentById(R.id.content_frame);

			if (currentFrag != null)
				transaction.remove(currentFrag);

			transaction.commit();

		}
	
		public void clearStack()
		{
			/*
			FragmentManager manager = getFragmentManager();
			
			for(int i = 0 ; i < manager.getBackStackEntryCount(); i++)
			{
				manager.popBackStack();
			}
				*/	
		}

}
