package com.o2m.wockhard.epoll;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.BaseActivity;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.o2m.wockhard.wocktube.WocktubeListFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class EPollListFragment extends Fragment implements OnItemClickListener,OnClickListener {

	private String TAG = "EPollListFragment";
	private ListView optionListView;
	private TextView questionTxtView;
	private Button nextBtn;
	private JSONArray ePollList;
	private String pollId;
	
	private JSONObject ePollSubmitObj = new JSONObject();
	private JSONArray ePollDataArray = new JSONArray();
	
	int totalQuestions, currentQueNo = 0;
	ArrayAdapter<String> arrayAdapter;
	ArrayList<String> optionList;
	boolean showAns;
	
	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	
	 public EPollListFragment(JSONArray _ePollList, String _pollId) {
         // Empty constructor required for fragment subclasses
		 this.ePollList = _ePollList;
		 totalQuestions = _ePollList.length();
		 this.pollId = _pollId;
		 
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
    	 
         View rootView = inflater.inflate(R.layout.fragment_epoll, container, false);
         optionListView = (ListView) rootView.findViewById(R.id.ePollList);
         questionTxtView = (TextView) rootView.findViewById(R.id.ePollQuestion);
         nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
         nextBtn.setOnClickListener(this); 
         
         optionListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
      /*   String[] options = {"option1","option2","option3","option4","option5"};
         ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_single_choice, options);
         optionListView.setAdapter(arrayAdapter);*/
         setValuesfor(0);
         return rootView;
     }

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("EPollListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
	
		
	}

	@Override
	public void onClick(View v) {
		
		currentQueNo++;
		
		if(optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION)
		{
			Toast.makeText(this.getActivity(), " Please select answer then next ", Toast.LENGTH_SHORT).show();
			return;
		}
		else
		{
			this.captureAns();
		}
			
		if(currentQueNo < totalQuestions)
		{
			setValuesfor(currentQueNo);
		}
		else
		{
			Toast.makeText(this.getActivity(), " ePoll finished ", Toast.LENGTH_SHORT).show();
			
			
			submitePoll();
			
			
			
		}
	}

	private void submitePoll() {
		
	
			
			
			new ApplicationAsk(getActivity(), new ApplicationService() {
				
				 private String resp;
				 private JSONArray faqArray;
				
				@Override
				public void postExecute() {
					Log.v(TAG," onPostExecute inbox Response : " + resp);
					  
					  try {
						  
						  
						JSONObject wTResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + wTResponseObj.getString("d"));
						
						JSONObject wTResponseObj1 = new JSONObject(wTResponseObj.getString("d"));
						
						String status = wTResponseObj1.getString("Status");
						
						
						if(status.equalsIgnoreCase("success"))
						{
							JSONObject dataObj = wTResponseObj1.getJSONObject("data");
							faqArray = dataObj.getJSONArray("Table");
							
							EPollResultListFragment fragment = new EPollResultListFragment(faqArray);   
							fragment.setShowAns(showAns);
							FragmentManager fragmentManager = getFragmentManager();
						    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
						    Toast.makeText(getActivity(), "Thank You for attempting poll ", Toast.LENGTH_LONG).show();
						   
								new AlertDialog.Builder(getActivity())
							    .setTitle("ePoll")
							    .setMessage("Thank you for attempting poll")
							    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
							        public void onClick(DialogInterface dialog, int which) { 
							            // continue with delete
							        	
							        }
							     })
							    .setIcon(android.R.drawable.ic_dialog_alert)
							     .show();
							
						}
						else
						{
							//failure
							Toast.makeText(getActivity(), "Poll can't be submitted. Please try later ", Toast.LENGTH_LONG).show(); 
						}
						
						
					} catch (JSONException e) {
						
						e.printStackTrace();
						Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
					}
					  
					  
					  
					  

		
					
				}
				
				@Override
				public void execute() {
					JSONObject ePollRequestObj = new JSONObject();
			        try {
			        	//ePollSubmitObj.put("employee_id", "8087");
			        	URLParams params = URLParams.getInstance();
			        	ePollSubmitObj.put("employee_id", params.getEmployee_id() );
						ePollSubmitObj.put("poll_id", pollId);
						ePollSubmitObj.put("data", ePollDataArray);
						
						ePollRequestObj.put("json_input", ePollSubmitObj.toString());
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		//String ePollSubmitURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/GetEpollResult";
			        String ePollSubmitURL = URLGenerator.getsubmitEPollResult();
		    		CommunicationService commService = new CommunicationService();
		    		resp = commService.sendSyncRequest(ePollSubmitURL, ePollRequestObj.toString());
			  
			 
					
				}
			}).execute();
			
			
			
			
			
	
		
		
	}

	private void setValuesfor(int questionNo)
	{
		Toast.makeText(this.getActivity(), "Question No : "+(questionNo+1), Toast.LENGTH_SHORT).show();
		try {
		if(questionNo !=0 )
		{
		/*String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		ansObj.put("quest_id",""+questionTxtView.getTag());
		ansObj.put("option", (optionListView.getCheckedItemPosition()+1));
		ePollDataArray.put(ansObj);*/
		
		}
		Log.v("EPollListFragment", ""+answersHashMap);
		
		JSONObject ePollJsonObj;
		
			ePollJsonObj = ePollList.getJSONObject(questionNo);
			String question = ePollJsonObj.getString("quest_title");
			String questionId = ePollJsonObj.getString("quest_id");
			/*String opt1 = ePollJsonObj.getString("answer_opt1");
			String opt2 = ePollJsonObj.getString("answer_opt2");
			String opt3 = ePollJsonObj.getString("answer_opt3");
			String opt4 = ePollJsonObj.getString("answer_opt4");
			String opt5 = ePollJsonObj.getString("answer_opt5");
			String opt6 = ePollJsonObj.getString("answer_opt6");
			*
			*/
			String opt1 = ePollJsonObj.getString("opt1");
			String opt2 = ePollJsonObj.getString("opt2");
			String opt3 = ePollJsonObj.getString("opt3");
			String opt4 = ePollJsonObj.getString("opt4");
			String opt5 = ePollJsonObj.getString("opt5");
			String opt6 = ePollJsonObj.getString("opt6");
			
			 optionList = new ArrayList<String>();
			if(!opt1.equals(""))
				optionList.add(opt1);
			if(!opt2.equals(""))
				optionList.add(opt2);
			if(!opt3.equals(""))
				optionList.add(opt3);
			if(!opt4.equals(""))
				optionList.add(opt4);
			if(!opt5.equals(""))
				optionList.add(opt5);
			if(!opt6.equals(""))
				optionList.add(opt6);
			
			//questionTxtView.setText(""+(questionNo+1)+")  "+question);
			questionTxtView.setText(""+question);
			questionTxtView.setTag(questionId);
			
			//arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_checked, optionList);
			arrayAdapter = new ArrayAdapter(getActivity(), R.layout.epoll_item, optionList);
			
	        optionListView.setAdapter(arrayAdapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void captureAns()
	{
		String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		
		JSONObject ansObj = new JSONObject();
		try {
			ansObj.put("quest_id",""+questionTxtView.getTag());
			ansObj.put("option", (optionListView.getCheckedItemPosition()+1));
			ePollDataArray.put(ansObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
