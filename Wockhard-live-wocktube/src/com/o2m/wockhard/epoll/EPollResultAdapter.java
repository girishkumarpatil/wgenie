package com.o2m.wockhard.epoll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.http.cookie.SetCookie2;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class EPollResultAdapter extends BaseAdapter
{

	Context context;
	//private HashMap<String, String> answersHashMap;
	List<String> list, percentList;
	

	public EPollResultAdapter(Context context, ArrayList<String> optionList, ArrayList<String> percentList)
	{

		this.context = context;
	/*	this.answersHashMap = answersHashMap;
		Set<String> keys = this.answersHashMap.keySet();*/
		this.list = optionList;
		this.percentList = percentList;
	}

	@Override
	public int getCount()
	{

		return list.size();
	}

	@Override
	public Object getItem(int position)
	{

		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();

			view = inflater.inflate(R.layout.epoll_result_list_item, parent, false);
			holder.txtView_epResT = (TextView) view.findViewById(R.id.epollResultansTitle);
			holder.txtView_epResPer = (TextView)view.findViewById(R.id.epollResultansPercent);
			
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
				
		
		
		try {
			String opt = this.list.get(position);
			
			holder.txtView_epResT.setText("" + this.list.get(position));
			holder.txtView_epResPer.setText("" + this.percentList.get(position));
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_epResT;
		
		public TextView txtView_epResPer;

	}

}
