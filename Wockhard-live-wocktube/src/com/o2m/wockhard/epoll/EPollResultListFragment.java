package com.o2m.wockhard.epoll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.o2m.wockhard.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class EPollResultListFragment extends Fragment implements OnItemClickListener,OnClickListener {

	private ListView optionListView;
	private TextView questionTxtView;
	private Button nextBtn;
	private JSONArray ePollList;
	
	int totalQuestions, currentQueNo = 0;
	ArrayAdapter<String> arrayAdapter;
	ArrayList<String> optionList , percentList;
	boolean showAns;
	
	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	
	
	 public EPollResultListFragment(JSONArray _ePollList) {
         // Empty constructor required for fragment subclasses
		 this.ePollList = _ePollList;
		 totalQuestions = _ePollList.length();
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
    	 
         View rootView = inflater.inflate(R.layout.fragment_epoll_result, container, false);
         optionListView = (ListView) rootView.findViewById(R.id.ePollResultList);
         questionTxtView = (TextView) rootView.findViewById(R.id.ePollResultQuestion);
         nextBtn = (Button) rootView.findViewById(R.id.ernextBtn);
         nextBtn.setOnClickListener(this); 
         
         
         if(!showAns)
         {
        	 optionListView.setVisibility(View.INVISIBLE);
        	 nextBtn.setVisibility(View.INVISIBLE);
        	 questionTxtView.setText("Thank You for Attempting Poll");
         }
         else
         {
        	 setValuesfor(0);
         }
         
      /*   String[] options = {"option1","option2","option3","option4","option5"};
         ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_single_choice, options);
         optionListView.setAdapter(arrayAdapter);*/
         
         return rootView;
     }

    
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v("EPollListFragment", "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
	
		
	}

	@Override
	public void onClick(View v) {
		
		currentQueNo++;
		
//		if(optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION)
//		{
//			Toast.makeText(this.getActivity(), " Please select answer then next ", Toast.LENGTH_SHORT).show();
//			return;
//		}
			
		if(currentQueNo < totalQuestions)
		{
			setValuesfor(currentQueNo);
		}
		else
		{
			//Toast.makeText(this.getActivity(), " ePoll finished ", Toast.LENGTH_SHORT).show();
			
			
				new AlertDialog.Builder(getActivity())
			    .setTitle("ePoll Result")
			    .setMessage("Do you want to view result again")
			    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // continue with delete
			        	currentQueNo = 0;
			        	setValuesfor(currentQueNo);
			        }
			     })
			    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // do nothing
			        	
			        }
			     })
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();
			}
			
		
	}

	private void setValuesfor(int questionNo)
	{
		Toast.makeText(this.getActivity(), "Question No : "+(questionNo+1), Toast.LENGTH_SHORT).show();
		
		if((questionNo + 1) == ePollList.length())
		{
			nextBtn.setVisibility(View.GONE);
		}
//		if(questionNo !=0 )
//		{
//		String selectedAns = optionList.get(optionListView.getCheckedItemPosition());
//		answersHashMap.put((String) questionTxtView.getText(), selectedAns);
//		}
		Log.v("EPollListFragment", ""+answersHashMap);
		
		JSONObject ePollJsonObj;
		try {
			ePollJsonObj = ePollList.getJSONObject(questionNo);
			String question = ePollJsonObj.getString("title");
			
			String opt1 = ePollJsonObj.getString("answer_opt1");
			String opt2 = ePollJsonObj.getString("answer_opt2");
			String opt3 = ePollJsonObj.getString("answer_opt3");
			String opt4 = ePollJsonObj.getString("answer_opt4");
			String opt5 = ePollJsonObj.getString("answer_opt5");
			String opt6 = ePollJsonObj.getString("answer_opt6");
			
			/*String opt1 = ePollJsonObj.getString("opt1");
			String opt2 = ePollJsonObj.getString("opt2");
			String opt3 = ePollJsonObj.getString("opt3");
			String opt4 = ePollJsonObj.getString("opt4");
			String opt5 = ePollJsonObj.getString("opt5");
			String opt6 = ePollJsonObj.getString("opt6");*/
			
			int total = Integer.parseInt(ePollJsonObj.getString("total"));
			
			 optionList = new ArrayList<String>();
			 percentList = new ArrayList<String>();
			if(!opt1.equals(""))
			{
				int opt1Count = Integer.parseInt(ePollJsonObj.getString("option1_count"));	
				optionList.add(opt1);
				percentList.add(getPercent(opt1Count, total) + "%");
				answersHashMap.put(opt1, getPercent(opt1Count, total) + "%");
			}
			if(!opt2.equals(""))
			{
				int opt2Count = Integer.parseInt(ePollJsonObj.getString("option2_count"));	
				optionList.add(opt2);
				percentList.add(getPercent(opt2Count, total) + "%");
				
			}
			if(!opt3.equals(""))
			{
				int opt3Count = Integer.parseInt(ePollJsonObj.getString("option3_count"));	
				optionList.add(opt3);
				percentList.add(getPercent(opt3Count, total) + "%");
				
			}
			if(!opt4.equals(""))
			{
				int opt4Count = Integer.parseInt(ePollJsonObj.getString("option4_count"));	
				optionList.add(opt4);
				percentList.add(getPercent(opt4Count, total) + "%");
				
			}
			if(!opt5.equals(""))
			{
				int opt5Count = Integer.parseInt(ePollJsonObj.getString("option5_count"));	
				optionList.add(opt5);
				percentList.add(getPercent(opt5Count, total) + "%");
				
			}
			if(!opt6.equals(""))
			{
				int opt6Count = Integer.parseInt(ePollJsonObj.getString("option6_count"));	
				optionList.add(opt6);
				percentList.add(getPercent(opt6Count, total) + "%");
				
			}
			
			//questionTxtView.setText(""+(questionNo+1)+")  "+question);
			questionTxtView.setText(""+question);
			// arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, optionList);
			//arrayAdapter = new ArrayAdapter(getActivity(), R.layout.epoll_item, optionList);
			EPollResultAdapter adapter = new EPollResultAdapter(getActivity(), optionList, percentList);
			
	        optionListView.setAdapter(adapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private String getPercent(int opt, int total)
	{
		   Float calValue = (float) (100* (1.0*opt)/total) ;
		   
		   BigDecimal bd = new BigDecimal(Float.toString(calValue));
		    bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		   
		   return String.valueOf(bd.floatValue());
	}
		
}
