package com.o2m.wockhard;



import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GridMenuAdapter extends BaseAdapter {
	
	String result[];
	Context context;
	int[] optIconId;
	private static LayoutInflater inflater=null;
	public GridMenuAdapter(GridHomeActivity activity,int[]optIcon, String []optName) {
		result=optName;
		context=activity;
		optIconId=optIcon;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return result.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
    public class Holder
    {
        TextView tv;
        ImageView img;
    }
	@Override
	public View getView(final int position, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		Holder holder= new Holder();
		View rowView;
		rowView = inflater.inflate(R.layout.bkup_single_view_gv,null);
        holder.tv=(TextView) rowView.findViewById(R.id.tv_optName);
        holder.img=(ImageView) rowView.findViewById(R.id.iv_optIcon);

    if(!result[position].equalsIgnoreCase("Avatar"))
        	holder.tv.setText(result[position]);
    holder.img.setImageResource(optIconId[position]);
    Resources res = context.getResources();
        
    
    switch (position) {
	case 0:
		rowView.setBackgroundColor(res.getColor(R.color.my_learning_bg));
		break;
	case 1:
		rowView.setBackgroundColor(res.getColor(R.color.my_assment_bg));
		break;
	case 2:
		rowView.setBackgroundColor(res.getColor(R.color.epoll_bg));
		break;
	case 3:
		rowView.setBackgroundColor(res.getColor(R.color.casestudy_bg));
		break;
	case 4:
		rowView.setBackgroundColor(res.getColor(R.color.shared_learning_bg));
		break;
	case 5:
		rowView.setBackgroundColor(res.getColor(R.color.inbox_bg));
		break;
	case 6:
		rowView.setBackgroundColor(res.getColor(R.color.wocktube_bg));
		break;
	case 7:
		rowView.setBackgroundColor(res.getColor(R.color.faq_bg));
		break;
	case 8:
		rowView.setBackgroundColor(res.getColor(R.color.training_path_bg));
		break;
	case 9:
		rowView.setBackgroundColor(res.getColor(R.color.performance_bg));
		break;
	case 10:
		rowView.setBackgroundColor(res.getColor(R.color.avtar_bg));
		break;
	case 11:
		rowView.setBackgroundColor(res.getColor(R.color.classroom_bg));
		break;
	default:
		break;
	}
//    rowView.setOnClickListener(new OnClickListener() {
//
//       @Override
//       public void onClick(View v) {
//           // TODO Auto-generated method stub
//           Toast.makeText(context, "You Clicked "+result[position]+",position:"+position, Toast.LENGTH_LONG).show();
//       }
//   });
		return rowView;


}
}
