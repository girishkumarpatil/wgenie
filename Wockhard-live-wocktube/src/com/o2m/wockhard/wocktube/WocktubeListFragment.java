package com.o2m.wockhard.wocktube;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.o2m.wockhard.R;
import com.o2m.wockhard.sharedlearning.SharedLearningAdapter;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class WocktubeListFragment extends Fragment implements OnItemClickListener, OnItemSelectedListener {

	private ListView wocktubeListView;
	private JSONArray wocktubeList;
	private String TAG = "WocktubeListFragment";
	
	
	 public WocktubeListFragment(JSONArray _wTList) {
         // Empty constructor required for fragment subclasses
		 this.wocktubeList = _wTList;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.fragment_wocktube, container, false);
         wocktubeListView = (ListView) rootView.findViewById(R.id.wocktubeList);
         
       /*  WocktubeAdapter wocktubeAdapter = new WocktubeAdapter(this.getActivity(), wocktubeList);
         wocktubeListView.setAdapter(wocktubeAdapter);*/
                  
     	Spinner spinner = (Spinner) rootView
				.findViewById(R.id.wocktubeSpinner);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this.getActivity(), R.array.wocktube_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);
         
         
         
         wocktubeListView.setOnItemClickListener(this);
         spinner.setOnItemSelectedListener(this);
       
         spinner.setSelection(0);
         return rootView;
     }

	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v(TAG , "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		
		try {
			JSONObject wTObj = (JSONObject) wocktubeList.get(position);
			
			String videoUrl = wTObj.getString("video_link");
			
			Intent intent=new Intent(Intent.ACTION_VIEW, 
		             Uri.parse(videoUrl));
		     startActivity(intent);
			
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
	
			JSONArray filteredwocktubeList = new JSONArray();

			for (int i = 0; i < this.wocktubeList.length(); i++) {

				try {

					JSONObject wTObj = (JSONObject) this.wocktubeList.get(i);
					String category = wTObj.getString("wt_type");
					if (category.equalsIgnoreCase(String.valueOf(pos))) {
						filteredwocktubeList.put(wTObj);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
			if (filteredwocktubeList.length() == 0) {
				
				Toast.makeText(getActivity(), "No Data Found ",
						Toast.LENGTH_SHORT).show();
				wocktubeListView.setAdapter(null);
				wocktubeListView.invalidateViews();
				
			} else {

				wocktubeListView.setAdapter(null);
				wocktubeListView.invalidateViews();
				WocktubeAdapter inboxMsgAdapter = new WocktubeAdapter(
						this.getActivity(), filteredwocktubeList);
				wocktubeListView.setAdapter(inboxMsgAdapter);
			}

		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	
		
	
}
