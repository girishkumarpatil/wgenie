package com.o2m.wockhard.wocktube;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class WocktubeAdapter extends BaseAdapter
{

	Context context;
	JSONArray wTJsonArray;
	ImageLoader imageLoader;

	public WocktubeAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.wTJsonArray = _arrayMessages;
		imageLoader = new  ImageLoader(context);
	}

	@Override
	public int getCount()
	{

		return wTJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return wTJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();

			view = inflater.inflate(R.layout.wocktube_list_item, parent, false);
			holder.txtView_wtTitle = (TextView) view.findViewById(R.id.wTTitle);
			holder.txtView_synopsis = (TextView)view.findViewById(R.id.wTSynopsis);
			holder.imgView_thumbnailview = (ImageView)view.findViewById(R.id.thmbnail);
			
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
				
		
		
		try {
			JSONObject wTObj = (JSONObject) wTJsonArray.get(position);
			
			holder.txtView_wtTitle.setText("" + wTObj.getString("wt_title"));
			holder.txtView_synopsis.setText("" + wTObj.getString("wt_synopsis"));
			ImageView imgV = holder.imgView_thumbnailview;
			imageLoader.DisplayImage(wTObj.getString("video_image"), imgV);
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(position %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_3)));
		}
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_wtTitle;
		public ImageView imgView_thumbnailview ;
		public TextView txtView_synopsis;

	}

}
