package com.o2m.wockhard;

public class URLGenerator {
	
	//static final String BASE_URL = "http://wockhardt.oh22media.com/myservice/service1.asmx";
	//http://training.wockhardt.com/mobile/service1.asmx
	//Go LIVE
	//static final String BASE_URL = "http://training.wockhardt.com/mobile/service1.asmx";
	// TEST ENVIRONMENT
	static final String BASE_URL = "http://wock.oh22works.com/wck/Service1.asmx";
	public static String getCaseStudyCommentsUrl() {
		
		return BASE_URL + "/" + "getCSCommentsForUsers";
		
	}
	
	public static String getLoginUrl() {
		
		return BASE_URL + "/" + "validate_sql_user";
		
	}
	
   public static String WOW_Check_feedback_display() {
		
		return BASE_URL + "/" + "WOW_Check_feedback_display";
		
	}
	
	
	
public static String getAllDivision() {
		
		return BASE_URL + "/" + "GetDivision";
		
	}

public static String getDivision_L5_L4_User() {
	
	return BASE_URL + "/" + "getdivision_L5_L4_User";
	
}
	
	//GetUserListForEmployeeID {�p_employee_id� : �8081�, �p_div_code� : �E�, �p_level_code�: �L1�}
	public static String getUserListForEmployeeUrl() {
		
		return BASE_URL + "/" + "GetUserListForEmployeeID";
		
	}
	
	
	//getCommentsBySLID
	public static String getCommentsForSharedLearning() {
		
		return BASE_URL + "/" + "getCommentsBySLID";
		
	}
//AddSLComments
	
   public static String getAddCommentsForSharedLearning() {
		
		return BASE_URL + "/" + "AddSLComments";
		
	}
   
   public static String getAddSharedLearningUrl() {
		
		return BASE_URL + "/" + "AddSharedLearning";
		
	}
   
   //GetInboxMessage_new, GetOtherMessage

   public static String getInboxMessages() {
		
		return BASE_URL + "/" + "GetInboxMessage_new";
		
	}
   
   public static String getOtherMessages() {
		
		return BASE_URL + "/" + "GetOtherMessage";
		
	}
   //Update_Message_flag
   public static String getUpdateMessageStatus() {
		
		return BASE_URL + "/" + "Update_Message_flag";
		
	}
   //getCaseStudyByDivDesg
   
   public static String getCaseStudy() {
		
		return BASE_URL + "/" + "getCaseStudyByDivDesg";
		
	}
   
 //AddStarToCaseStudy 
   public static String AddStarToCaseStudy() {
		
		return BASE_URL + "/" + "AddStarToCaseStudy";
		
	}
   
   //CS_AddLikeCountForComment
   
   public static String AddLikeToCaseStudyComment() {
		
		return BASE_URL + "/" + "CS_AddLikeCountForComment";
		
	}
   
   //AddCSComment
   public static String AddCSComment() {
		
		return BASE_URL + "/" + "AddCSComment";
		
	}
  
   
   //GetEpollFrontend
   
   public static String getEPoll() {
		
		return BASE_URL + "/" + "GetEpollFrontend";
		
	}
   
   //
   public static String getsubmitEPollResult() {
		
		return BASE_URL + "/" + "GetEpollResult";
		
	}
   
   //GetWocktubeForFrontend
   
   public static String getWockTube() {
		
		return BASE_URL + "/" + "GetWocktubeForFrontend";
		
	}
   
   //GetFAQForFrontendByDivision
   
   public static String getFAQ() {
		
		return BASE_URL + "/" + "GetFAQForFrontendByDivision";
		
	}
   
   //App_GetPerformanceWall
   
   public static String getPerformanceWall() {
		
		return BASE_URL + "/" + "App_GetPerformanceWall";
		
	}
   
   //GetSharedLearningForFrontend
   
   public static String getSharedLearning() {
		
		return BASE_URL + "/" + "GetSharedLearningForFrontend";
		
	}
   //AddLikeCountForSL
   
   public static String AddLikeCountForSL() {
		
		return BASE_URL + "/" + "AddLikeCountForSL";
		
	}
   
   //App_MilestoneAllCertificate
   
   public static String getCurriculumAssistance() {
		
		return BASE_URL + "/" + "App_MilestoneAllCertificate";
		
	}
   
   //Milestone_PendingIndividualAsst
   public static String getPendingAssistance() {
		
		return BASE_URL + "/" + "Milestone_PendingIndividualAsst";
		
	}
   
   //Milestone_PendingMISCAsst
   
   public static String getMilestone_PendingMISCAsst() {
		
		return BASE_URL + "/" + "Milestone_PendingMISCAsst";
		
	}
   
   //Milestone_PendingCourseAsst
   
   public static String getMilestone_PendingCourseAsst() {
		
		return BASE_URL + "/" + "Milestone_PendingCourseAsst";
		
	}
   
   //Milestone_PendingWocktubeAsst
   public static String getMilestone_PendingWocktubeAsst() {
		
		return BASE_URL + "/" + "Milestone_PendingWocktubeAsst";
		
	}
   
   public static String getInductionAssessment() {
		
		return BASE_URL + "/" + "mobile_Induction_GetAssessment";
		
	}
   
   public static String getInductionAssessmentQuestion() {
		
		return BASE_URL + "/" + "mobile_Induction_Get_Assessment_questions";
		
	}
   
   public static String submitInductionAssessmentResult() {
		
		return BASE_URL + "/" + "mobile_Induction_Insert_Assessment_Result";
		
	}
   
   public static String getIndividualAssessment() {
		
		return BASE_URL + "/" + "mobile_individual_GetAssessment";
		
	}
   
   public static String getPREPOSTAssessment() {
		
		return BASE_URL + "/" + "mobile_PREPOST_GetAssessment";
		
	}
   
   public static String getLatestAssessment() {
		
		return BASE_URL + "/" + "UP_get_Assessment_Interactive";
		
	}
   
   public static String getIndividualAssessmentQuestion() {
		
		return BASE_URL + "/" + "mobile_Induction_Get_Assessment_questions";
		
	}
   
   public static String submitIndividualAssessmentResult() {
		
		return BASE_URL + "/" + "mobile_individual_Insert_Assessment_Result";
		
	}
   
   public static String getWocktubeAssessment() {
		
		return BASE_URL + "/" + "mobile_wocktube_GetAssessment";
		
	}
  
  public static String getWocktubeAssessmentQuestion() {
		
		return BASE_URL + "/" + "mobile_Induction_Get_Assessment_questions";
		
	}
  
  public static String submitWocktubeAssessmentResult() {
		
		return BASE_URL + "/" + "mobile_wocktube_Assessment_Result";
		
	}
  
  public static String getCertificationAssessment() {
		
		return BASE_URL + "/" + "mobile_certification_assessment";
		
	}
  
  public static String submitCertificateAssessmentResult() {
		
		return BASE_URL + "/" + "mobile_certification_Insert_Assessment_Result";
		
	}
  
  public static String getCourseAssessment() {
		
		return BASE_URL + "/" + "mobile_My_Learning_GetAssessment";
		
	}

public static String submitCourseAssessmentResult() {
		
		return BASE_URL + "/" + "mobile_My_Learning_Insert_Assessment_Result";
		
	}
  
  public static String getMISCAssessment() {
		
		return BASE_URL + "/" + "mobile_Misc_GetAssessment";
		
	}
//mobile_Misc_questions_by_assessment_id
  
  public static String getMISCQue() {
		
		return BASE_URL + "/" + "mobile_Misc_questions_by_assessment_id";
		
	}


  
public static String submitMISCAssessmentResult() {
		
		return BASE_URL + "/" + "mobile_Misc_Insert_Assessment_Result";
		
	}
//http://training.wockhardt.com/Webservice.asmx/SaveSLImageFile_New

public static String imageUpload() {
	
	return "http://training.wockhardt.com/Webservice.asmx/SaveSLImageFile_New";
	
}
}
