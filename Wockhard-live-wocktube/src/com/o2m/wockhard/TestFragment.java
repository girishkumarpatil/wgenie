package com.o2m.wockhard;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import com.twotoasters.android.horizontalimagescroller.image.ImageToLoad;
import com.twotoasters.android.horizontalimagescroller.image.ImageToLoadUrl;
import com.twotoasters.android.horizontalimagescroller.widget.HorizontalImageScroller;
import com.twotoasters.android.horizontalimagescroller.widget.HorizontalImageScrollerAdapter;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class TestFragment extends Fragment implements OnClickListener {

	private JSONObject slObj;
	private Button slPostComm, slLikeBtn, slPlayBtn;
	private EditText slCommDesc; 
	private ListView commentListView;
	private TextView likeCount, slVideoLink;
	private VideoView videoView;
	HorizontalImageScroller scroller;
	ProgressDialog pDialog;
	String videoLinkToBePlayed = "";
	private String TAG = "SharedLearningDetailsFragment";
	
	
	 public TestFragment() {
         // Empty constructor required for fragment subclasses
		 this.slObj = null;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         
    	 View rootView = inflater.inflate(R.layout.fragment_test, container, false);
    	 
             
         
       
        // scroller = (HorizontalImageScroller) rootView.findViewById(R.id.my_horizontal_image_scroller2);
         HorizontalImageScroller testScroller = (HorizontalImageScroller) rootView.findViewById(R.id.my_horizontal_image_scroller2);
         
         
         ArrayList<ImageToLoad> images1 = new ArrayList<ImageToLoad>();
         for (int i=0; i <  5; i++)
         {
        	 /*JSONObject imgObj = imageArray.getJSONObject(i);
        	 String imageTitle = imgObj.getString("img_title");
        	 String url = imgObj.getString("img_file");
        	 Log.v(TAG, "Title : " + imageTitle + "" + " \nimageLink : " + url);*/
        	 ImageToLoadUrl downloadImg = new ImageToLoadUrl("http://www.diporental.com/wp-content/gallery/listcar/toyota-innova2012.jpg");
             downloadImg.setCanCacheFile(true);
             images1.add(downloadImg); 
             // substitute some pretty picture you can stand to see 20 times in a list
           //  images.add(new ImageToLoadDrawableResource(R.drawable.some_drawable)); // plug in some of your own drawables
         }
         HorizontalImageScrollerAdapter adapter1 = new HorizontalImageScrollerAdapter(this.getActivity(), images1);
         adapter1.setImageSize(200);
         testScroller.setAdapter(adapter1);   

         
        // scroller.setAdapter(new Ho)
         
       
         return rootView;
     }

	private void loadAndPlayVideoFrom(String VideoURL) {
		// Create a progressbar
				pDialog = new ProgressDialog(getActivity());
				// Set progressbar title
				pDialog.setTitle(" Video Streaming ");
				// Set progressbar message
				pDialog.setMessage("Buffering..."+ VideoURL);
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				// Show progressbar
				pDialog.show();
		 
				try {
					// Start the MediaController
					MediaController mediacontroller = new MediaController(
							getActivity());
					mediacontroller.setAnchorView(videoView);
					// Get the URL from String VideoURL
					Uri video = Uri.parse(VideoURL);
					videoView.setMediaController(mediacontroller);
					videoView.setVideoURI(video);
		 
				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
		 
				videoView.requestFocus();
				videoView.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						pDialog.dismiss();
						videoView.start();
					}
				});
		 
			
		
	}

	@Override
	public void onClick(View view) {
		
		if(view == slPostComm)
		{
			if(slCommDesc.getText().toString().trim().equalsIgnoreCase(""))
			{
				Toast.makeText(getActivity(), "Please enter the comment", Toast.LENGTH_LONG).show();
			}
			else
			{
				//Toast.makeText(getActivity(), "Submission not implemented yet", Toast.LENGTH_LONG).show();
				//this.submitComment(slCommDesc.getText().toString());
			}
		}
		else if(view == slLikeBtn)
		{
			this.submitLikeSL();
		}
		else if(view == slPlayBtn)
		{
			//Uri video = Uri.parse(videoLinkToBePlayed);
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(videoLinkToBePlayed));
			startActivity(i);
		}
		
	}
	
	
	public void submitLikeSL()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csLikeResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csLikeResponseObj.getString("d"));
						
						JSONObject csLikeResponseObj1 = new JSONObject(csLikeResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csLikeResponseObj1.getString("Status"));
						
					// csF = new CaseStudyFragment(csResponseObj);
					 	if(csLikeResponseObj1.getString("Status").equalsIgnoreCase("Success")){
					 		slCommDesc.setText("");
					 	Toast.makeText(getActivity(), "Like Posted Successfully..", Toast.LENGTH_LONG).show();
					 		slLikeBtn.setEnabled(false);
					 		slLikeBtn.setClickable(false);
					 		
					 		increasetLikeCount();
					 	}
					 	else{
					 	Toast.makeText(getActivity(), "Error during Like posting", Toast.LENGTH_LONG).show();
					 	}
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject csAddLikeRequestObj = new JSONObject();
		        try {
					
		        	URLParams urlParameter = URLParams.getInstance();
					csAddLikeRequestObj.putOpt("p_sl_id",slObj.getString("sl_id"));
					csAddLikeRequestObj.putOpt("p_user_id",urlParameter.getUser_id());
							
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String csURL = URLGenerator.AddLikeCountForSL();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(csURL, csAddLikeRequestObj.toString());
		  
		 
				
			}
		}).execute();

		
	}
	
	private void increasetLikeCount()
	{
		String currentCount = (String) likeCount.getText();
		int cnt = Integer.parseInt(currentCount);
		cnt++;
		likeCount.setText(""+cnt);
	}
	

	
	
	
	
}
