package com.o2m.wockhard;

public enum AssessmentType {
	Induction, Certificate, Course, Individual, Wocktube, MISC, PREPOST, INTERACTIVE
}
