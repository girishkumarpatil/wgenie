package com.o2m.wockhard.faqs;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.me.JSONException;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.wockhard.R;

public class FaqAdapter extends BaseExpandableListAdapter 
{

	Context context;

	ArrayList<String> arrayMessages;

	JSONArray faqJsonArray;
	String unreadCount;

	//private ChannelDetails channelDetails;

	//public MessageAdapter(Context context, ArrayList<String> _arrayMessages)
	public FaqAdapter(Context context, JSONArray _arrayFaq)
	{

		this.context = context;
		this.faqJsonArray = _arrayFaq;
	}

	/*
	@Override
	public int getCount()
	{

		return sLJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return sLJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();
//			view = inflater.inflate(R.layout.channel_list, parent, false);
			view = inflater.inflate(R.layout.shared_learn_list_item, parent, false);
			holder.txtView_slTitle = (TextView) view.findViewById(R.id.slTitle);
			holder.txtView_slDesc = (TextView)view.findViewById(R.id.slDesc);
			holder.txtView_slBy = (TextView)view.findViewById(R.id.slBy);
			holder.txtView_slLikes = (TextView)view.findViewById(R.id.slLikes);
			holder.txtView_slComments = (TextView)view.findViewById(R.id.slComments);
			//holder.attachment = (View)view.findViewById(R.id.) ;
			//holder.msgDate = (TextView)view.findViewById(R.id.msgDaTE) ;
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		
		holder.txtView_slTitle.setText("sl Title : " + position);
		holder.txtView_slDesc.setText("desc : " + position);
		holder.txtView_slBy.setText("Girish Patil "+ position);
		holder.txtView_slLikes.setText("Likes "+ position);
		holder.txtView_slComments.setText("Comments "+ position);
		
		
		try {
		
			JSONObject slObj = (JSONObject) sLJsonArray.get(position);
				
			holder.txtView_slTitle.setText(" " + slObj.getString("sl_sub"));
			holder.txtView_slDesc.setText(" " + slObj.getString("sl_desc"));
			holder.txtView_slBy.setText(" " + slObj.getString("name"));
			holder.txtView_slLikes.setText("Likes " + slObj.getString("like_count"));
			holder.txtView_slComments.setText("Comments "+  slObj.getString("comment_count"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
		
		return view;

	}
*/
	private static class ChildViewHolder
	{

		public TextView txtView_que;
		public TextView txtView_ans;
		
	}
	
	private static class GroupViewHolder
	{

		public TextView txtView_grpTitle;
		
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return faqJsonArray.length();
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		try {
			JSONObject brandObj = (JSONObject) faqJsonArray.get(groupPosition);
			JSONArray questionArray = brandObj.getJSONArray("Questions");
			/*String noOfEmp = brandObj.getString("no_of_emp");
			int noEmp = Integer.parseInt(noOfEmp);*/
			int questionCnt = questionArray.length();
			return questionCnt;
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		try {
			
			JSONObject performanceObj = (JSONObject) faqJsonArray.get(groupPosition);
			
			return performanceObj;
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}



	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		GroupViewHolder holder = new GroupViewHolder();
		if (view == null)
		{

			holder = new GroupViewHolder();
			view = inflater.inflate(R.layout.faq_grp_list_item, parent, false);
			holder.txtView_grpTitle = (TextView) view.findViewById(R.id.faqGrpTitle);
		
			view.setTag(holder);
		}
		else
		{
			holder = (GroupViewHolder) view.getTag();
		}
		
		try {
			
			JSONObject performanceObj = (JSONObject) faqJsonArray.get(groupPosition);
				
			holder.txtView_grpTitle.setText("" + performanceObj.getString("brand_name"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(groupPosition %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		
	
		
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ChildViewHolder holder = new ChildViewHolder();
		if (view == null)
		{

			holder = new ChildViewHolder();
			view = inflater.inflate(R.layout.faq_child_list_item, parent, false);
			holder.txtView_que = (TextView) view.findViewById(R.id.faqQue);
			holder.txtView_ans = (TextView) view.findViewById(R.id.faqAns);
		
			view.setTag(holder);
		}
		else
		{
			holder = (ChildViewHolder) view.getTag();
		}
		
try {
			
			JSONObject brandObj = (JSONObject) faqJsonArray.get(groupPosition);
			JSONArray questionArray = brandObj.getJSONArray("Questions");
			JSONObject queObj = (JSONObject) questionArray.get(childPosition);
				
			holder.txtView_que.setText(""+queObj.getString("question_title"));
			holder.txtView_ans.setText(""+queObj.getString("answer"));
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String getName(int childPosition, JSONObject performanceObj) throws org.json.JSONException {
		switch (childPosition) {
		case 0:

			return performanceObj.getString("emp1");
		case 1:

			return performanceObj.getString("emp2");
		case 2:

			return performanceObj.getString("emp3");
		case 3:

			return performanceObj.getString("emp4");
		case 4:

			return performanceObj.getString("emp5");
		case 5:

			return performanceObj.getString("emp6");
		case 6:

			return performanceObj.getString("emp7");
		case 7:

			return performanceObj.getString("emp8");
		case 8:

			return performanceObj.getString("emp9");
		case 9:

			return performanceObj.getString("emp10");

		default:
			break;
		}
		return "";
	}
	
	public String getHeadqtr(int childPosition, JSONObject performanceObj) throws org.json.JSONException {
		switch (childPosition) {
		case 0:

			return performanceObj.getString("hq1");
		case 1:

			return performanceObj.getString("hq2");
		case 2:

			return performanceObj.getString("hq3");
		case 3:

			return performanceObj.getString("hq4");
		case 4:

			return performanceObj.getString("hq5");
		case 5:

			return performanceObj.getString("hq6");
		case 6:

			return performanceObj.getString("hq7");
		case 7:

			return performanceObj.getString("hq8");
		case 8:

			return performanceObj.getString("hq9");
		case 9:

			return performanceObj.getString("hq10");

		default:
			break;
		}
		return "";
	}
	
	

}
