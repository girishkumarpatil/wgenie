package com.o2m.wockhard.jshandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;

import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

public class JsHandler {

	Activity activity;
	String TAG = "JsHandler";
	WebView webView;
	public static final String FILE_EXISTS = "File already Exists !!";

	public JsHandler(Activity _contxt, WebView _webView) {
		activity = _contxt;
		webView = _webView;
	}

	/**
	 * This function handles call from JS
	 */
	public void jsFnCall(String jsString) {
		showDialog(jsString);
	}

	/**
	 * This function handles call from Android-Java
	 */
	public void javaFnCall(String jsString) {

		final String webUrl = "javascript:diplayJavaMsg('" + jsString + "')";
		// Add this to avoid android.view.windowmanager$badtokenexception unable
		// to add window
		if (!activity.isFinishing())
			// loadurl on UI main thread
			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					webView.loadUrl(webUrl);
				}
			});
	}

	/**
	 * function shows Android-Native Alert Dialog
	 */
	public void showDialog(String msg) {

		// AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
		// alertDialog.setTitle(activity.getString(R.string.app_dialog_title));
		// alertDialog.setMessage(msg);
		// alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,activity.getString(R.string.ok_text),
		// new DialogInterface.OnClickListener()
		// {
		// public void onClick(DialogInterface dialog, int which)
		// {
		// dialog.dismiss();
		// }
		// });
		// alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,activity.getString(R.string.cancel_text),
		// new DialogInterface.OnClickListener()
		// {
		// @Override
		// public void onClick(DialogInterface dialog, int which)
		// {
		// dialog.dismiss();
		// }
		// });
		// alertDialog.show();
	}


	@JavascriptInterface
	public String filePath(String fileName) {

		Log.v(TAG, "filePath(fileName) : "+fileName);
		String status = HttpCommunicationResponse.FAILURE, path = "";
		File parentFile = activity.getFilesDir();
		File downloadFile = new File(parentFile.getAbsolutePath()
				+ File.separator + fileName);

		if (downloadFile.exists()) {
			status = HttpCommunicationResponse.SUCCESS;
			Log.e("", FILE_EXISTS + " : " + downloadFile.getAbsolutePath());
			path = downloadFile.getAbsolutePath();
		}

		return path;
		/*
		final String webUrl = "javascript:filePathCallback('" + status + "','"
				+ path + "')";
		// Add this to avoid android.view.windowmanager$badtokenexception unable
		// to add window
		if (!activity.isFinishing())
			// loadurl on UI main thread
			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					webView.loadUrl(webUrl);
				}
			});
		 */
	}

	@JavascriptInterface
	public void deleteFileWithName(String fileName) {

		Log.v(TAG, "deleteFileWithName(fileName) : "+fileName);

		String status = HttpCommunicationResponse.FAILURE, path = null;
		File parentFile = activity.getFilesDir();
		File downloadFile = new File(parentFile.getAbsolutePath()
				+ File.separator + fileName);

		if (downloadFile.exists()) {
			status = HttpCommunicationResponse.SUCCESS;
			Log.e("", FILE_EXISTS + " : " + downloadFile.getAbsolutePath());
			path = downloadFile.getAbsolutePath();
			new File(path).delete();
			Log.e("", "FILE DELETED" + " : " + downloadFile.getAbsolutePath());
		}

		final String webUrl = "javascript:deleteFileWithNameCallback('"
				+ status + "','" + path + "')";
		if (!activity.isFinishing())
			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					webView.loadUrl(webUrl);
				}
			});

	}


	@JavascriptInterface
	public void deleteAll() {

		Log.v(TAG, "deleteAll");
		File dir = activity.getFilesDir();
		if (dir.isDirectory()) {
			File[] subDir = dir.listFiles();
			for (int i = 0; i < subDir.length; i++) {
				if (subDir[i].isDirectory()) {
					String[] contents = subDir[i].list();
					for (int j = 0; j < contents.length; j++) {

						new File(subDir[i], contents[j]).delete();

					}

				}
				subDir[i].delete();
			}

		}
		final String webUrl = "javascript:deleteAllCallback('"+HttpCommunicationResponse.SUCCESS+"')";

		if(!activity.isFinishing()) 

			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					webView.loadUrl(webUrl); 
				}
			});

	}

	@JavascriptInterface
	public void downloadFile(final String contentUrl, final String fileName,
			final boolean isLogEnabled) {

		Log.v(TAG, "downloadFile : url " + contentUrl +"fileName : "+ fileName );
		new ApplicationAsk(activity, new ApplicationService() {

			String status, reason;
			File downloadFile = null;
			int responseCode = 0;

			HashMap<String, String> headerFields = null;

			InputStream inputStream = null;
			FileOutputStream fileOutputStream;

			@Override
			public void postExecute() {
				final String webUrl = "javascript:downloadCallback('" + status + "','"
						+ reason + "','" + downloadFile.getAbsolutePath() + "')";
				// Add this to avoid android.view.windowmanager$badtokenexception unable
				// to add window
				if (!activity.isFinishing())
					// loadurl on UI main thread
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							webView.loadUrl(webUrl);
						}
					});

			}

			@Override
			public void execute() {
				if (isLogEnabled) {
					Log.d("url :", contentUrl);
					// Log.d("filePath :", filePath);
					Log.d("fileName : ", fileName);
					// Log.d("timeout :", "" + timeout);

				}

				try {
					// if (reqHeader != null) {
					// headerFields = reqHeader.getHeaderFields();
					// }
					// File parentFile = new File(filePath);
					File parentFile = activity.getFilesDir();
					if (!parentFile.exists()) {
						parentFile.mkdirs();
					}
					downloadFile = new File(parentFile.getAbsolutePath()
							+ File.separator + fileName);

					if (downloadFile.exists()) {
						if (isLogEnabled) {
							Log.e("", FILE_EXISTS);
						}
					}

					URL url = new URL(contentUrl);
					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();

					if (headerFields != null) {
						if (headerFields.size() != 0) {
							for (String headerKey : headerFields.keySet()) {
								connection.setRequestProperty(headerKey,
										headerFields.get(headerKey));
								if (isLogEnabled) {
									Log.d("Header Key : ", headerKey);
									Log.d("Header Value : ",
											headerFields.get(headerKey));
								}
							}
						}
					}
					connection.setDoInput(true);
					connection.setConnectTimeout(1000 * 60 * 3);
					connection.connect();
					responseCode = connection.getResponseCode();

					if (responseCode == HttpURLConnection.HTTP_OK) {

						inputStream = connection.getInputStream();

						fileOutputStream = new FileOutputStream(downloadFile.getPath());

						byte[] buffer = new byte[4096];
						int readBytes;
						while ((readBytes = inputStream.read(buffer)) != -1) {
							fileOutputStream.write(buffer, 0, readBytes);

						}

						// responseObject = getResponse("", HttpURLConnection.HTTP_OK,
						// "",
						// HttpCommunicationResponse.SUCCESS);
						status = HttpCommunicationResponse.SUCCESS;
						reason = HttpCommunicationResponse.SUCCESS;

						if (inputStream != null) {
							inputStream.close();
						}

						if (fileOutputStream != null) {
							fileOutputStream.close();
						}
					}

					else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
						downloadFile.delete();
						// responseObject = getResponse("",
						// HttpURLConnection.HTTP_NOT_FOUND,
						// HttpCommunicationResponse.NO_SERVER_ACCESS,
						// HttpCommunicationResponse.FAILURE);
						status = HttpCommunicationResponse.FAILURE;
						reason = HttpCommunicationResponse.NO_SERVER_ACCESS;
						// return responseObject;

					} else if (responseCode == HttpCommunicationResponse.STATUS_CODE_ZERO) {
						downloadFile.delete();
						// responseObject = getResponse("", responseCode,
						// HttpCommunicationResponse.NO_INTERNET_ACCESS,
						// HttpCommunicationResponse.FAILURE);
						status = HttpCommunicationResponse.FAILURE;
						reason = HttpCommunicationResponse.NO_INTERNET_ACCESS;
						// return responseObject;

					} else {
						downloadFile.delete();
						// responseObject = getResponse("", responseCode,
						// HttpCommunicationResponse.NETWORK_FAIL,
						// HttpCommunicationResponse.FAILURE);
						// return responseObject;
						status = HttpCommunicationResponse.FAILURE;
						reason = HttpCommunicationResponse.NETWORK_FAIL;

					}

				} catch (HttpHostConnectException httpHostConnectException) {
					// This Exception happens when you attempt to open a TCP connection
					// from an IP address where there is nothing currently listening for
					// connection.
					downloadFile.delete();
					// responseObject = getResponse("", responseCode,
					// HttpCommunicationResponse.NETWORK_FAIL,
					// HttpCommunicationResponse.FAILURE);
					if (isLogEnabled) {
						httpHostConnectException.printStackTrace();
						;
					}
					status = HttpCommunicationResponse.FAILURE;
					reason = HttpCommunicationResponse.NETWORK_FAIL;
					// return responseObject;

				} catch (UnknownHostException unknownHostException) {
					// This Exception occurs when you are trying to connect to a remote
					// host using Host name but IP address of that Host can not be
					// resolved.
					downloadFile.delete();
					// responseObject = getResponse("", responseCode,
					// HttpCommunicationResponse.NO_INTERNET_ACCESS,
					// HttpCommunicationResponse.FAILURE);
					if (isLogEnabled) {
						unknownHostException.printStackTrace();
					}
					// return responseObject;
					status = HttpCommunicationResponse.FAILURE;
					reason = HttpCommunicationResponse.NETWORK_FAIL;

				} catch (ConnectTimeoutException connectTimeoutException) {
					downloadFile.delete();
					// responseObject = getResponse("", responseCode,
					// HttpCommunicationResponse.TIMEOUT_ERROR,
					// HttpCommunicationResponse.FAILURE);

					if (isLogEnabled) {
						connectTimeoutException.printStackTrace();
					}
					// return responseObject;
					status = HttpCommunicationResponse.FAILURE;
					reason = HttpCommunicationResponse.TIMEOUT_ERROR;

				} catch (SocketTimeoutException socketTimeoutException) {
					downloadFile.delete();
					// responseObject = getResponse("", responseCode,
					// HttpCommunicationResponse.SOCKET_TIMEOUT,
					// HttpCommunicationResponse.FAILURE);

					if (isLogEnabled) {
						socketTimeoutException.printStackTrace();
					}
					// return responseObject;
					status = HttpCommunicationResponse.FAILURE;
					reason = HttpCommunicationResponse.SOCKET_TIMEOUT;

				} catch (ConnectException connectException) {
					downloadFile.delete();
					// responseObject = getResponse("", responseCode,
					// HttpCommunicationResponse.NO_INTERNET_ACCESS,
					// HttpCommunicationResponse.FAILURE);

					if (isLogEnabled) {
						connectException.printStackTrace();
						;
					}
					status = HttpCommunicationResponse.FAILURE;
					reason = HttpCommunicationResponse.NO_INTERNET_ACCESS;
					// return responseObject;

				} catch (IOException ioexception) {
					downloadFile.delete();
					if (ioexception.getMessage() == null
							|| ioexception.getMessage().length() == 0) {
						// responseObject = getResponse("", responseCode,
						// HttpCommunicationResponse.NETWORK_FAIL,
						// HttpCommunicationResponse.FAILURE);
						status = HttpCommunicationResponse.FAILURE;
						reason = HttpCommunicationResponse.NETWORK_FAIL;

					} else {
						// responseObject = getResponse("", responseCode,
						// ioexception.getMessage(),
						// HttpCommunicationResponse.FAILURE);
						status = HttpCommunicationResponse.FAILURE;
						reason = ioexception.getMessage();
					}

					if (isLogEnabled) {
						ioexception.printStackTrace();
					}
					// return responseObject;
				} catch (Exception exception) {
					downloadFile.delete();
					if (exception.getMessage() == null
							|| exception.getMessage().length() == 0) {
						// responseObject = getResponse("", responseCode,
						// HttpCommunicationResponse.NETWORK_FAIL,
						// HttpCommunicationResponse.FAILURE);
						status = HttpCommunicationResponse.FAILURE;
						reason = HttpCommunicationResponse.NETWORK_FAIL;
					} else {
						// responseObject = getResponse("", responseCode,
						// exception.getMessage(),
						// HttpCommunicationResponse.FAILURE);
						status = HttpCommunicationResponse.FAILURE;
						reason = exception.getMessage();
					}

					if (isLogEnabled) {
						exception.printStackTrace();
						;
					}

				}
			};

		}).execute();
	}


	@JavascriptInterface
	public void openBrowser(String url) {

		Log.v(TAG, "Open PDF : " + url);
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		activity.startActivity(browserIntent);
	}



	public static interface HttpCommunicationResponse {
		public static final String NO_SERVER_ACCESS = "Server Not Found";
		public static final String NO_INTERNET_ACCESS = "Connection could not be established either because of device's or server's network connectivity";
		public static final String NETWORK_FAIL = "Unknown Error Occurred";
		public static final String TIMEOUT_ERROR = "Connection Timeout";
		public static final String SOCKET_TIMEOUT = "Socket Timeout";
		public static final String INVALID_URL = "Invalid Url";
		public static final String FAILURE = "FAILURE";
		public static final String SUCCESS = "SUCCESS";
		public static final String CONTENT_TYPE = "Content-Type";

		public static final int STATUS_CODE_ZERO = 0;

	}

}
