package com.o2m.wochhard.casestudy;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.o2m.wockhard.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CaseStudyCommentListFragment extends Fragment implements OnItemClickListener {

	private ListView csCommentListView;
	private JSONArray csCommentList;
	private String TAG = "CaseStudyCommentListFragment";
	
	
	 public CaseStudyCommentListFragment(JSONArray _csCommentList) {
         // Empty constructor required for fragment subclasses
		 this.csCommentList = _csCommentList;
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.fragment_casestudy_comment_list, container, false);
         csCommentListView = (ListView) rootView.findViewById(R.id.csCommentList);
         
         CsCommentAdapter csCommentAdapter = new CsCommentAdapter(this.getActivity(), csCommentList);
         csCommentListView.setAdapter(csCommentAdapter);
                  
         csCommentListView.setOnItemClickListener(this);
       
         return rootView;
     }

	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
		Log.v(TAG , "position"+ position);
		
		//Toast.makeText(this.getActivity(), ""+position, Toast.LENGTH_SHORT).show();
		
	/*	try {
			JSONObject wTObj = (JSONObject) csCommentList.get(position);
			
			String videoUrl = wTObj.getString("video_link");
			
			Intent intent=new Intent(Intent.ACTION_VIEW, 
		             Uri.parse(videoUrl));
		     startActivity(intent);
			
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		
	}

	
		
	
}
