package com.o2m.wochhard.casestudy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.sax.RootElement;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.BaseActivity;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;
import android.graphics.PorterDuff;
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CaseStudyFragment extends Fragment implements OnClickListener {
	Button csPostComm, csViewAllComments, csRateIt;
	EditText csCommDesc;
	JSONObject caseStudyObj;
	String commMsg;
	Dialog rankDialog;
	RatingBar ratingBar;
	
	public CaseStudyFragment(JSONObject _caseStudyObj) {
		// TODO Auto-generated constructor stub
		caseStudyObj = _caseStudyObj;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_casestudy, container, false);
		csPostComm = (Button) rootView.findViewById(R.id.csPostComm);
		csRateIt = (Button) rootView.findViewById(R.id.csRateIt);
		RatingBar showratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);
		csViewAllComments = (Button) rootView.findViewById(R.id.csViewAllComm);
        TextView csTitle = (TextView) rootView.findViewById(R.id.csTitle);
        TextView csAssignDate = (TextView) rootView.findViewById(R.id.csAssignedDate);
        WebView csDesc = (WebView) rootView.findViewById(R.id.csDescWebView);
        csDesc.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        csDesc.getSettings().setJavaScriptEnabled(true);
        TextView csCreatedName = (TextView) rootView.findViewById(R.id.csCreatedName);
        TextView csCommentDesc = (TextView) rootView.findViewById(R.id.csCommentDesc);
        TextView csCommentBy = (TextView) rootView.findViewById(R.id.csCommentBy);
        csCommDesc =(EditText) rootView.findViewById(R.id.csCommDesc);
        LayerDrawable stars = (LayerDrawable) showratingBar.getProgressDrawable();
		stars.getDrawable(2).setColorFilter(Color.parseColor("#ff8000"), PorterDuff.Mode.SRC_ATOP);
		
		try {
			csTitle.setText(caseStudyObj.getString("cs_title"));
			csAssignDate.setText(caseStudyObj.getString("display_date"));
			csDesc.loadData(caseStudyObj.getString("cs_desc"), "text/html", "UTF-8");
			csCreatedName.setText("Posted By : " + caseStudyObj.getString("name"));
			csCommentDesc.setText(caseStudyObj.getString("Comment_desc"));
			csCommentBy.setText(caseStudyObj.getString("comment_by_name"));
			int totalStar = Integer.parseInt(caseStudyObj.getString("total_star"));
			int totalEmp = Integer.parseInt(caseStudyObj.getString("total_emp"));
			
			
			if(totalEmp != 0)
				showratingBar.setRating(totalStar/totalEmp);
			else
				showratingBar.setRating(0);
			//user_star
			if(!caseStudyObj.getString("user_star").equals(""))
				csRateIt.setVisibility(View.GONE);
			JSONArray attachmentArray = caseStudyObj.getJSONArray("Attachments");
			if(attachmentArray.length() <= 0)
			{
				
				ImageButton atcBtn = (ImageButton) rootView.findViewById(R.id.csAttchment);
				atcBtn.setVisibility(View.GONE);
			}
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		csPostComm.setOnClickListener(this);
		csViewAllComments.setOnClickListener(this);
		csRateIt.setOnClickListener(this);
		return rootView;
	}
	
    
	public void viewAllComment()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			 private JSONArray commentArray;
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csCommentResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csCommentResponseObj.getString("d"));
						
						JSONObject csCommentResponseObj1 = new JSONObject(csCommentResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csCommentResponseObj1.getString("Status"));
						
					
					 	if(csCommentResponseObj1.getString("Status").equalsIgnoreCase("Success"))
					 	{
					 		//Toast.makeText(getActivity(), "Comment Posted Successfully..", Toast.LENGTH_LONG).show();
					 		// Comments 
					 		JSONObject dataObj = csCommentResponseObj1.getJSONObject("data");
							commentArray = dataObj.getJSONArray("Table");
					 		
							CaseStudyCommentListFragment fragment = new CaseStudyCommentListFragment(commentArray);
					 		FragmentTransaction ft = getFragmentManager().beginTransaction();
							ft.addToBackStack("CaseStudyCommentListFragment");
							ft.replace(R.id.content_frame, fragment, "CaseStudyFragment");
							ft.commit();
					 	}
					 	else
					 	{
					 		Toast.makeText(getActivity(), "Error during Comment posting", Toast.LENGTH_LONG).show();
					 	}
				} 
				  catch (JSONException e) 
				  {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject fetchCSCommetsRB = new JSONObject();
		        try {
		        	URLParams params = URLParams.getInstance();
					fetchCSCommetsRB.putOpt("_CSAssignID",caseStudyObj.getString("assign_id"));
					fetchCSCommetsRB.putOpt("_CSID",caseStudyObj.getString("cs_id"));
					fetchCSCommetsRB.putOpt("_EmployeeID", params.getEmployee_id());
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String fetchCSCommetsURL = URLGenerator.getCaseStudyCommentsUrl();//"http://wockhardt.oh22media.com/myservice/service1.asmx/getCSCommentsForUsers";
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(fetchCSCommetsURL, fetchCSCommetsRB.toString());
		  
		 
				
			}
		}).execute();
		
	}
	
	public void postComment()
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csResponseObj.getString("d"));
						
						JSONObject csResponseObj1 = new JSONObject(csResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csResponseObj1.getString("Status"));
						
					// csF = new CaseStudyFragment(csResponseObj);
					 	if(csResponseObj1.getString("Status").equalsIgnoreCase("Success")){
					 	Toast.makeText(getActivity(), "Comment Posted Successfully..", Toast.LENGTH_LONG).show();
					 	}
					 	else{
					 	Toast.makeText(getActivity(), "Error during Comment posting", Toast.LENGTH_LONG).show();
					 	}
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject csRequestObj = new JSONObject();
		        try {
					
					csRequestObj.putOpt("p_assign_id",caseStudyObj.getString("assign_id"));
					csRequestObj.putOpt("p_comment_desc",commMsg);
					//csRequestObj.putOpt("p_comment_by",caseStudyObj.getString("comment_by_name"));
					csRequestObj.putOpt("p_comment_by", URLParams.getInstance().getEmployee_id());
					csRequestObj.putOpt("p_file_display_name","");
					csRequestObj.putOpt("p_file_uname","");		
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		//String csURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/AddCSComment";
		        String csURL = URLGenerator.AddCSComment();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(csURL, csRequestObj.toString());
		  
		 
				
			}
		}).execute();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
	
		if (arg0==csPostComm)
		{
			commMsg = csCommDesc.getText().toString();
			
			if(commMsg.trim().equalsIgnoreCase(""))
			{
				Toast.makeText(getActivity(), "Please Enter the comments", Toast.LENGTH_LONG).show();
			}
			else
			{
				this.postComment();
			}
			
			
		}
		else if(arg0 == csViewAllComments)
		{
			//Toast.makeText(getActivity(), "Not Implemented Yet", Toast.LENGTH_LONG).show();
			this.viewAllComment();
		}
		else if(arg0 == csRateIt)
		{
			    rankDialog = new Dialog(getActivity(), R.style.FullHeightDialog);
		        rankDialog.setContentView(R.layout.rating_dialog);
		        rankDialog.setCancelable(true);
		        ratingBar = (RatingBar)rankDialog.findViewById(R.id.dialog_ratingbar);
		        ratingBar.setRating(0);
		        
		        TextView text = (TextView) rankDialog.findViewById(R.id.rank_dialog_text1);
		        text.setText("Rate Now");
		 
		        Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
		        updateButton.setOnClickListener(new View.OnClickListener() {
		            @Override
		            public void onClick(View v) {
		                rankDialog.dismiss();
		                submitRatings((int)ratingBar.getRating());
		            }
		        });
		        //now that the dialog is set up, it's time to show it    
		        rankDialog.show(); 
		}
		
	}
	
	private void submitRatings(final int noOfStars)
	{
		new ApplicationAsk(getActivity(), new ApplicationService() {
			 private String TAG;
			 private String resp;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute postComment Response : " + resp);
				  
				  try {
					  
						JSONObject csResponseObj = new JSONObject(resp);
						
						Log.v(TAG," onPostExecute Status : " + csResponseObj.getString("d"));
						
						JSONObject csResponseObj1 = new JSONObject(csResponseObj.getString("d"));
						
						Log.v(TAG," onPostExecute message : " +  csResponseObj1.getString("Status"));
						
					// csF = new CaseStudyFragment(csResponseObj);
					 	if(csResponseObj1.getString("Status").equalsIgnoreCase("Success")){
					 	Toast.makeText(getActivity(), "Rating Posted Successfully..", Toast.LENGTH_LONG).show();
					 	csRateIt.setVisibility(View.GONE);
					 	}
					 	else{
					 	Toast.makeText(getActivity(), "Error during Rating posting", Toast.LENGTH_LONG).show();
					 	}
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(getActivity(), "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject csAddStarRequestObj = new JSONObject();
		        try {
					URLParams params = URLParams.getInstance();
					csAddStarRequestObj.putOpt("p_assign_id",caseStudyObj.getString("assign_id"));
					csAddStarRequestObj.putOpt("p_employee_id",params.getEmployee_id());
					csAddStarRequestObj.putOpt("p_user_id", params.getUser_id());
					csAddStarRequestObj.putOpt("p_star","" + noOfStars);
					csAddStarRequestObj.putOpt("p_div_code", params.getDiv_code());
					csAddStarRequestObj.putOpt("p_level_code",params.getLevel_code());
					//p_level_code
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		String csURL = URLGenerator.AddStarToCaseStudy();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(csURL, csAddStarRequestObj.toString());
		  
		 
				
			}
		}).execute();
	}
	
}
