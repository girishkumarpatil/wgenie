package com.o2m.wochhard.casestudy;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.o2m.wockhard.ApplicationAsk;
import com.o2m.wockhard.ApplicationService;
import com.o2m.wockhard.CommunicationService;
import com.o2m.wockhard.R;
import com.o2m.wockhard.URLGenerator;
import com.o2m.wockhard.URLParams;

public class CsCommentAdapter extends BaseAdapter
{

	Context context;
	JSONArray wTJsonArray;

	public CsCommentAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.wTJsonArray = _arrayMessages;
	}

	@Override
	public int getCount()
	{

		return wTJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return wTJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();

			view = inflater.inflate(R.layout.cs_comment_list_item, parent, false);
			holder.txtView_csCommentBy = (TextView) view.findViewById(R.id.csCommentBy);
			holder.txtView_csCommentDesc = (TextView)view.findViewById(R.id.csCommentDesc);
			holder.txtView_csCommentDate = (TextView) view.findViewById(R.id.csCommentdate);
			holder.txtView_csCommentLikeCount = (TextView) view.findViewById(R.id.csCommentLikeCount);
			//holder.button_csCommentlikeDislike = (ToggleButton) view.findViewById(R.id.toggle);
			holder.button_csCommentlikeDislike = (Button) view.findViewById(R.id.btn_like);
			
			
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
				
		try {
			JSONObject wTObj = (JSONObject) wTJsonArray.get(position);
			
			holder.txtView_csCommentBy.setText("" + wTObj.getString("name"));
			holder.txtView_csCommentDesc.setText("" + wTObj.getString("comment_desc"));
			holder.txtView_csCommentDate.setText("" + wTObj.getString("display_date"));
			holder.txtView_csCommentLikeCount.setText("" + wTObj.getString("total_like"));
			
			if(wTObj.getString("cm_like_id").trim().equals(""))
			{
				//holder.button_csCommentlikeDislike.setChecked(false);
				holder.button_csCommentlikeDislike.setTag(1);
				holder.button_csCommentlikeDislike.setBackgroundResource(R.drawable.like);
			}
			else
			{
				//holder.button_csCommentlikeDislike.setChecked(true);
			if(wTObj.getString("like_flag").trim().equals("0"))
			{
				holder.button_csCommentlikeDislike.setTag(1);
				holder.button_csCommentlikeDislike.setBackgroundResource(R.drawable.like);
			}
			else{
				holder.button_csCommentlikeDislike.setTag(0);
				holder.button_csCommentlikeDislike.setBackgroundResource(R.drawable.dislike);
			}
			}
holder.button_csCommentlikeDislike.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					int tag = (Integer) v.getTag();
					
					
					
					try {
			
							JSONObject wTObj = (JSONObject) wTJsonArray.get(position);
							String commentId = wTObj.getString("comment_id");
							
						if(tag == 1)
						{
							//Toast.makeText(context, "like -- " + commentId, Toast.LENGTH_SHORT).show();
							v.setTag(0);
							v.setBackgroundResource(R.drawable.dislike);
							submitLikeDislike("1", commentId, position);
							
						}
						else
						{
							//Toast.makeText(context, "dis like -- " + commentId, Toast.LENGTH_SHORT).show();
							v.setTag(1);
							v.setBackgroundResource(R.drawable.like);
							submitLikeDislike("0", commentId, position);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
/*holder.button_csCommentlikeDislike.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
					try {
						
						JSONObject wTObj = (JSONObject) wTJsonArray.get(position);
						String commentId = wTObj.getString("comment_id");
						if(isChecked)
						{
							Toast.makeText(context, "like -- " + position, Toast.LENGTH_SHORT).show();
							submitLikeDislike("1", commentId);
						}
						else
						{
							Toast.makeText(context, "dis like -- " + position, Toast.LENGTH_SHORT).show();
							submitLikeDislike("0", commentId);
						}
						
						
					} catch (org.json.JSONException e) {
						
						e.printStackTrace();
					}
					
					
					
				}
			});
*/			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return view;

	}

	private static class ViewHolder
	{		
		public TextView txtView_csCommentDesc;
		public TextView txtView_csCommentBy;
		public TextView txtView_csCommentDate;
		//public ToggleButton button_csCommentlikeDislike;
		public Button button_csCommentlikeDislike;
		public TextView txtView_csCommentLikeCount;

	}
	
	private void submitLikeDislike(final String likeFlag, final String commentId, final int position) {
		
		new ApplicationAsk(context, new ApplicationService() {
			 private String TAG;
			 private String resp;
			
			@Override
			public void postExecute() {
				Log.v(TAG," onPostExecute Like Response : " + resp);
				  
				  try {
					  
						JSONObject csCommentLikeResponseObj = new JSONObject(resp);
						Log.v(TAG," onPostExecute Status : " + csCommentLikeResponseObj.getString("d"));
						JSONObject csCommentLikeResponseObj1 = new JSONObject(csCommentLikeResponseObj.getString("d"));
											
						
					// csF = new CaseStudyFragment(csResponseObj);
					 	if(csCommentLikeResponseObj1.getString("Status").equalsIgnoreCase("Success")){
					 	
					 	
					 	JSONObject wTObj = (JSONObject) wTJsonArray.get(position);
					 	String totalLike = wTObj.getString("total_like");
					 	int likeCnt = Integer.parseInt(totalLike);
					 	
					 		if(likeFlag.equalsIgnoreCase("1"))
					 		{
					 			Toast.makeText(context, "Liked", Toast.LENGTH_LONG).show();
					 			likeCnt++;
					 			wTObj.put("total_like", ""+likeCnt);//
							 	wTObj.put("cm_like_id", "23");
							 	wTObj.put("like_flag", "1");
							 	
					 		}
					 		else
					 		{
					 			Toast.makeText(context, "Disliked", Toast.LENGTH_LONG).show();
					 			likeCnt--;
					 			wTObj.put("total_like", ""+likeCnt);//
							 	wTObj.put("cm_like_id", "");
							 	wTObj.put("like_flag", "0");
					 		}
					 		notifyDataSetChanged();				 		
					 		
					 	
					 	}
					 	else{
					 	Toast.makeText(context, "Error : " + csCommentLikeResponseObj1.getString("message"), Toast.LENGTH_LONG).show();
					 	}
				} catch (JSONException e) {
					
					e.printStackTrace();
					Toast.makeText(context, "Data Parsing Error", Toast.LENGTH_LONG).show(); 
				}	
			}
			
			@Override
			public void execute() {
				JSONObject commentlikeRequestObj = new JSONObject();
		        try {
					URLParams params = URLParams.getInstance();
					commentlikeRequestObj.putOpt("p_comment_id",commentId);
					commentlikeRequestObj.putOpt("p_user_id", params.getUser_id());
					commentlikeRequestObj.putOpt("p_employee_id", params.getEmployee_id());
					commentlikeRequestObj.putOpt("p_like_flag", likeFlag);
							
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
	    		//String csURL = "http://wockhardt.oh22media.com/myservice/service1.asmx/AddCSComment";
		        String csCommentLikeURL = URLGenerator.AddLikeToCaseStudyComment();
	    		CommunicationService commService = new CommunicationService();
	    		resp = commService.sendSyncRequest(csCommentLikeURL, commentlikeRequestObj.toString());
		  
		 
				
			}
		}).execute();

	}
	
	

}
