// JavaScript Document



wockhardt.controller("library", function($scope, $http, $compile, $timeout){
	
$scope.init = function(){

	        var divCode = $("#div_code").val();
			var userId = $("#user_id").val();
			var levelCode = $("#level_code").val();
			var params = {divcode:divCode, desg_level:levelCode, userid:userId};
			console.log("In scope init div : " + divCode + "userId : " + userId + "levelCode : " + levelCode  );
			console.log(params);
			$http.get("http://training.wockhardt.com/Library/service1.asmx/" + "GetMyLibrary_MobileView", {params:params}).success(function(data, status, header, config){
				console.log(data);
				$("#preloader").fadeOut(500);
				var obj=JSON.parse($(data).text());
				console.log(obj);
				if (obj.Status=="success"){
					$scope.libraries= obj.data;
					window.setTimeout(function(){
						$(".inline").colorbox({inline:true, width:"300px", fixed:true});
						$('#navigation').accordion({
								header: '.head', 
								autoheight: false,
								alwaysOpen:false
							});		
					}, 300);
				}
			})

}
$scope.init();
//$timeout($scope.init(), 2000);
		
$scope.rateArticle = function($event, rating){
	var butObj = $event.currentTarget;
	var articleId = $(butObj).parents("ul.user_rating").attr("data-article-id");
	var id = $(butObj).parents("ul.user_rating").attr("data-name");
	var divCode = $("#div_code").val();
	var userId = $("#user_id").val();
	var levelCode = $("#level_code").val();
	var empId = $("#emp_id").val();
	var params = {userid:userId, emp_id:empId, article_id:articleId, star:rating, rating:rating};
	console.log(params);
	$http.get("http://training.wockhardt.com/Library/service1.asmx/" + "AddStarRating_ToLibraryArticle", {params:params}).success(function(data, status, header, config){
				//console.log(data);
				var obj=JSON.parse($(data).text());
				
				if (obj.Status=="success"){
						var rateCount = parseInt(obj.data[0].Count);
						var star = parseInt(obj.data[0].Rating);
						var prevRating = $("#"+id).attr("data-rating-count");
						//console.log($("#"+id));
						$("#"+id).find(".rate_count").text(rateCount);
						$("#"+id).find(".rate_it a").remove();
						var ratingTotal = (prevRating + rating) / rateCount;
						var str="";
						//console.log(ratingTotal);
						for (i=0;i<=4;i++){
							if (i>ratingTotal-1){
								str+="<li class='blank'></li>";
							}else{
								str+="<li></li>";
							}
						}
						console.log(str);
						$("#"+id).find(".rating_box .rating").html(str);
						
						
					
						$.colorbox.close();
				}
			})
	
	
	

}
	
})

