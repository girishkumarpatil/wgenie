// JavaScript Document
const serverUrl = "http://training.wockhardt.com/mobile/service1.asmx/";
var charLimit = 120;

var wockhardt = angular.module('wockhardt', []).filter('filter_trusted_html', ['$sce', function($sce) {
	return function(text) {
		//console.log(text);
		return $sce.trustAsHtml(unEntity(text));
	};
	
}]);

wockhardt.run(function($rootScope, $http, $sce){
	$rootScope.trustUrl = function(src){
		return $sce.trustAsResourceUrl(src);
	}
		//$("#preloader").hide();
	
	
})



function getDataByKey(key){
	var obj = JSON.parse(localStorage.getItem("user"));
	return obj[key];
}
function errorShow(obj, msg){
	if (obj.parents(".elem").find(".error").length<1){
		obj.focus();
		obj.parents(".elem").append("<span class='error'></span>");
		obj.parents(".elem").find(".error").text(msg);
		obj.parents(".elem").find(".error").show();
		window.setTimeout(function(){
			obj.parents(".elem").find(".error").remove();
		}, 2000);
	}
}
function showDialog(msg, type, url){
	$(".dialog").text(msg);
	$(".dialog").fadeIn();
	window.setTimeout(function(){
			$(".dialog, .overlay").hide();
			if (type=="refresh"){
				location.reload();
			}else if(type=="redirect"){
				location.href = url;
			}
		}, 4000);

}

	function waitDialog(msg){
	$(".dialog").text(msg);
	$(".dialog, .overlay").fadeIn();
}




$.easing.myease = function(x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
			return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
};

function unEntity(str){
	
   //return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&nbsp;/g,'');
   return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

function entity(str){
	
   return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}


$(window).load(function(e) {
    	
$(document).on("click", ".menu", function(){
	var navLeft = parseInt($("nav").css("left"));
	if (navLeft<0){
		$("nav").stop().animate({left:0}, 700, "myease");
		$(".overlay2").fadeIn();
	}else{
		$("nav").stop().animate({left:-150}, 700, "myease");
		$(".overlay2").fadeOut();
	}
	
	return false;
	
});

//$("#preloader").fadeOut(500);

$(document).mouseup(function(e){
		var container = $("nav");
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			hideMenu();
		}
	});

});

function hideMenu(){
	$("nav").stop().animate({left:-150}, 700, "myease");
	$(".overlay2").fadeOut();
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function validateUrl(url){
var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
if (!re.test(url)) { 

    return false;
}else{
	return true;
}

}