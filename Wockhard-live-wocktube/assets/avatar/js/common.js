// JavaScript Document

$(window).load(function(e) {
    $('#preloader').fadeOut();
	$(".box").css("height", $(window).height()-110);
});
$(document).ready(function() {
    $('input[type=radio]').on('click', function(){
       if($(this).attr("checked")){
            $('input[type=radio]').parent('label').removeClass('checked'); 
            $(this).parent('label').addClass('checked'); 
        }
    });
    
});

function error_box(txt){
	$(".error_box_center").text(txt)
	$(".error_box_center").show();
	window.setTimeout(function(){
		$(".error_box_center").fadeOut();
	}, 1500);
	
}

function show_feedback(id, status){
    if(status=="true"){
        $("#"+id).fadeIn();
    }else{
        $("#"+id).fadeOut();
    }
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}