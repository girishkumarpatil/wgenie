// JavaScript Document

$(window).load(function(e) {
    $('#preloader').fadeOut();
	$(".box").css("height", $(window).height()-110);
});
$(document).ready(function() {
    $('input[type=radio]').on('click', function(){
       if($(this).attr("checked")){
            $('input[type=radio]').parent('label').removeClass('checked'); 
            $(this).parent('label').addClass('checked'); 
        }
    });
    
	
	//$(document).on("click", "#full", function(){
		//show_box("#pop_video")
	//})
	//$(document).on("click", ".overlay", function(){
		//hide_box();
	//})
});

function show_box(id){
	$(".lightbox, .overlay").show();
	$(".windows").hide();
	$(id).show();
	var pop_videocontainer = document.getElementById('pop_player');
	var pop_videosource = document.getElementById('pop_vsource');
	pop_videocontainer.play();
}

function hide_box(){
	$(".lightbox, .overlay").hide();
	$(".windows").hide();
	var pop_videocontainer = document.getElementById('pop_player');
	var pop_videosource = document.getElementById('pop_vsource');
	pop_videocontainer.pause();
}

function error_box(txt){
	$(".error_box_center").text(txt)
	$(".error_box_center").show();
	window.setTimeout(function(){
		$(".error_box_center").fadeOut();
	}, 1500);
	
}

function show_feedback(id, status){
    if(status=="true"){
        $("#"+id).fadeIn();
    }else{
        $("#"+id).fadeOut();
    }
}
