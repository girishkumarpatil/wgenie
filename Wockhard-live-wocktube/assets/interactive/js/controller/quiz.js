app.controller("quiz", function ($scope, $http, $interval){

$scope.questions = []; [{id:5, type:"5", order:["1", "2", "3", "4", "5"], ans:"1", questions:"Rearrange the following"}, {id:5, type:"4", images:["img/1.png", "img/2.jpg", "img/3.png", "img/4.jpg", "img/5.jpg"], ans:"1", questions:"Select one image?"}, {id:4, type:"1", questions:"Match the column", options:["Q1", "Q2", "Q3", "Q4"], answers:["1", "2", "3", "4"]}, {id:1, type:"0", questions:"Prime minister of India", ans:"1", options:["Narendra Modi", "Manmohan Singh", "Amit Shah"]}, {id:2, type:"3", questions:"Tell me about india?"},  {id:3, type:"2", questions:"Please solve the puzzle", imageUrl:"img/wockhardt621.jpg"}];


$scope.index = -1;
$scope.marks = 0;
$scope.total = 	0;
$scope.valid = false;
$scope.answers = [];
$scope.completed = false;
$scope.duration = 0;
$scope.timer = 0;
$scope.displayTimer = "";
$scope.attempts = 0;
$scope.passed = 1;
$scope.singleMarks = 0;
var interval;
$scope.getAssessment = function(){

			var divCode = $("#div_code").val();
			var userId = $("#user_id").val();
			var levelCode = $("#level_code").val();
			var asstId = $("#asst_id").val();
			var asstDivId = $("#asst_div_id").val();
			var duration = $("#duration").val();
			var attempts = $("#attempts").val();
			var marks = $("#marks").val();
			
			
			
			$scope.duration = duration;

			var params = {div_code:divCode, levelcode:levelCode, asst_id:asstId}
			console.log(params);

 			$http.get(serverUrl + "UP_Get_Interactive_Assessment_question_by_user", {params:params}).success(function(data, status, header, config){
								var obj=JSON.parse($(data).text());
								console.log(obj);
								if (obj.data.Table.length>0){
									for (i in obj.data.Table){
										var ques = obj.data.Table[i];
										$scope.questions.push({
											id :ques.IQ_id,
											type:ques.IQ_type,
											questions:ques.IQ_question,
											imageUrl:imageUrl+ques.IQ_image,
											options:[ques.IQ_ans1, ques.IQ_ans2, ques.IQ_ans3, ques.IQ_ans4],
											answers:[ques.IM_ans1, ques.IM_ans2, ques.IM_ans3, ques.IM_ans4],
											correct_answers2:[ques.IM_ans1, ques.IM_ans2, ques.IM_ans3, ques.IM_ans4],
											correct_answers:[ques.IQ_ans1, ques.IQ_ans2, ques.IQ_ans3, ques.IQ_ans4],
											order:[ques.IQ_ans1, ques.IQ_ans2, ques.IQ_ans3, ques.IQ_ans4],
											rorder:[ques.IQ_ans1, ques.IQ_ans2, ques.IQ_ans3, ques.IQ_ans4],
											images:[imageUrl+ques.IQ_ans1, imageUrl+ques.IQ_ans2, imageUrl+ques.IQ_ans3, imageUrl+ques.IQ_ans4],
											ans:parseInt(ques.IQ_corrans),
											attempts:ques.attempts,
											pass_percent:ques.pass_percent,
											IQ_asst_id:ques.IQ_asst_id,
											asst_div_id:ques.asst_div_id
										});
									};
									//console.log($scope.questions);
									$scope.total = 	$scope.questions.length;
									$scope.singleMarks = marks / $scope.total;
									console.log($scope.singleMarks);
									if ($scope.answers.length==$scope.total){
										document.write(JSON.stringify($scope.answers));
									}else{		
										$scope.showQues();	
										$scope.displayTimer= $scope.duration * 60;
										
										$scope.attempts = $scope.attempts+1;
										interval = $interval(function(){
											
											$scope.displayTimer--;
											//console.log($scope.displayTimer);	
											$scope.timer = changeSecondToMinute($scope.displayTimer);
											
											if ($scope.displayTimer<1){
												
												//alert("Quiz over");
												$scope.completed = true;
												 $interval.cancel(interval);
												
											}
											
											
										}, 1000)

										
									}
															}
						
	})	
	
	
}

//$scope.getAssessment();



$scope.showQues = function($event){
	$scope.index++;
	
	$scope.question = $scope.questions[$scope.index];

	if ($scope.question.type=="5"){
		$scope.question.rorder = $scope.question.rorder;
		$scope.question.order = shuffle($scope.question.order);
		
		
	}
	
	if ($scope.question.type=="1"){
		
		$scope.question.ranswer = $scope.question.correct_answers2;
		
		$scope.question.answers = shuffle($scope.question.answers);
	}

	
	if ($scope.question.type=="2"){
			$scope.valid = true;
			window.setTimeout(function(){
				$('#pile').height($('#source_image').height());
            $('#source_image').snapPuzzle({
                rows: 3, columns: 3,
                pile: '#pile',
                containment: '#puzzle-containment',
                onComplete: function(){
					$scope.valid = true;
                    $('#source_image').fadeOut(150).fadeIn();
                    $('#puzzle_solved').show();
                }
            });}, 1000);	


            $(window).resize(function(){
                $('#pile').height($('#source_image').height());
                $('#source_image').snapPuzzle('refresh');
            });
		
	}
	
	if ($scope.question.type=="5"){
		window.setTimeout(function(){rankSort();}, 1000);
		
	}
}
$scope.checkAns = function($event){
			$scope.userValue = "";


			
			
			if ($scope.question.type=="0" && $("input[type=radio]:checked").val()==""){
				alert("Select atleast one");				
			}else if ($scope.question.type=="3" && $("textarea").val()==""){
				alert("Enter value")
			}else if ($scope.question.type=="2" && $scope.valid==false){
				alert("Please solve the puzzle")
			}else if ($scope.question.type=="1" && $scope.valid== false){
				alert("Please match the column");
			}else if ($scope.question.type=="4" && $(".image_selection li.active").length<1){
				alert("Please select atleast one");
			}else{
				
				
					$scope.marks = $scope.singleMarks;
					$scope.subans = "";
					console.log($scope.marks);
					$scope.userValue = "0";
					if ($scope.index<=$scope.total){
						if ($scope.question.type=="0"){
							$scope.userValue = $("input[type=radio]:checked").val();
							console.log($scope.question.ans + ":" + $scope.userValue);
							if ($scope.userValue!=$scope.question.ans){
								$scope.marks = 0;
								$scope.passed = 0;
							}
						}
						if ($scope.question.type=="3"){
							
							$scope.subans = $("textarea").val();
						}
						if ($scope.question.type=="4"){
								$scope.userValue = $(".image_selection li.active").attr("data-value");
							if ($scope.userValue!=$scope.question.ans){
								$scope.marks = 0;
								$scope.passed = 0;
							}
							
							
						}
						//alert($scope.userValue);
						if ($scope.question.type=="5"){
							var sel = [];
							$scope.question.ans = "0,1,2,3";
							$(".ranking_list li").each(function(index, element){
								
								for(j in $scope.question.correct_answers){
									console.log($scope.question.correct_answers[j] + ":" + $(element).attr("data-value"));
									if ($scope.question.correct_answers[j]==$(element).attr("data-value")){
										sel.push(j);
									}
									
								}
								
							})
							
							$scope.userValue = sel.toString();
							
							if ($scope.userValue!=$scope.question.ans){
								$scope.marks= 0;
								$scope.passed= 0;
							}
							
							
						}
					
					
			
					if ($scope.question.type=="1"){
						var sel = [];
						var corAns = [];
						//console.log($scope.question.ranswer);
						//console.log($scope.question.answers);
							$(".ans li.completed").each(function(index, element){
								
								for(j in $scope.question.ranswer){
									//console.log($scope.question.ranswer[j] + ":" + $(element).attr("data-value"));
									if ($scope.question.ranswer[j]==$(element).attr("data-value")){
										corAns.push(j);
										sel.push($(element).attr("data-match-id"))
									}
									
								}
								
							});
							console.log(sel);
							console.log(corAns);
							
							$scope.userValue = sel.toString();
							$scope.question.ans = corAns.toString();
							if ($scope.userValue!=corAns){
								$scope.marks= 0;
								$scope.passed= 0;
							}
						}
					
					
					
					
					var qtimer = ($scope.duration * 60) - $scope.displayTimer;
					var min = changeSecondToMinute(qtimer).split(":")[1];
					var sec = changeSecondToMinute(qtimer).split(":")[2];
					$scope.answers.push({id:$scope.question.id, ans:$scope.userValue, correct_answers:$scope.question.ans, minute:min, seconds:sec, pass_percent:$scope.question.pass_percent, asst_id:$scope.question.IQ_asst_id, attempts:$scope.question.attempts, asst_div_id:$scope.question.asst_div_id, marks:$scope.marks, type:$scope.question.type, passed:$scope.passed, subans:$scope.subans});
					console.log($scope.answers);
					 
					
					console.log($scope.index + ":" + $scope.total);
					
					
						//alert($scope.question.type);
						$scope.saveData();
						//$scope.showQues();
						
					
					
				}
				
			}
	
}



var matchSelected;
$(document).on("click", ".options li", function(){
	
	var getMatch = $(this).attr("data-index");
	if ($(this).hasClass("completed")==true){
		$(this).removeClass("completed");
		$(".ans li[data-match-id='"+getMatch+"']").removeClass("completed");
	}
	if ($(".options li.selected").length<1){
		$(this).addClass("selected");
	}else{
		$(".options li").removeClass("selected");
	}
	
	
	
})

$(document).on("click", ".ans li", function(){
	
	
	if ($(".options li.selected").length>0){
			if ($(this).hasClass("completed")==false){
				var getMatch = $(".options li.selected").attr("data-match-id");
				$(this).attr("data-match-id", getMatch);
				$(this).addClass("completed")
				$(".options li.selected").addClass("completed");
				$(".options li").removeClass("selected");
			}
			
	}
	
		if ($(".ans li.completed").length==$(".ans li").length){			
				$scope.valid= true;
			}
		//console.log($scope.valid);
	
	
})

$scope.insertMarks = function(){
	var userId = $("#user_id").val();
	var asstId = $("#asst_id").val();
	var asstDivId = $("#asst_div_id").val();
	var marks = 0;

	var totalSecond = 0;
	
	console.log($scope.answers);
	
	for (i in $scope.answers){
		marks = marks + Math.round($scope.answers[i].marks);
		
		totalSecond = totalSecond+Math.round(parseInt($scope.answers[i].seconds))
	}
	var passed = 0;
	var pass_percent= parseInt($("#pass_percent").val());
	if (marks>pass_percent){
		passed=1;
	}
	
	
	var minute = parseInt(changeSecondToMinute(totalSecond).split(":")[1]);
	var second = parseInt(changeSecondToMinute(totalSecond).split(":")[2]);
	
	var params  = {interactiveMarks:{employee_id:userId,attempts:$scope.attempts,marks:marks,pass_percent:pass_percent,asst_div_id:asstDivId,interactive_type:3,passed:passed,asst_id:asstId,min:minute,sec:second}}  
	
	console.log(JSON.stringify(params));

	$http.get(serverUrl + "Interactive_Assessment_marks", {params:params}).success(function(data, status, header, config){
			console.log(data);
						$scope.completed = true;
						$interval.cancel(interval);
			
	})
	
	
}

$scope.saveData = function(){
	var userId = $("#user_id").val();
	var jsonObj  = "";
	var jsonString = "";
	var jsonArray = [];
	
	if ($scope.answers[$scope.index].type=="0" || $scope.answers[$scope.index].type=="3" || $scope.answers[$scope.index].type=="2" || $scope.answers[$scope.index].type=="4"){
	
	jsonObj = {questId:$scope.answers[$scope.index].id,employee_id:userId,asst_type:$scope.answers[$scope.index].type,option_selected:$scope.answers[$scope.index].ans, correct_option:$scope.answers[$scope.index].correct_answers, attempts:$scope.attempts, asst_div_id:$scope.answers[$scope.index].asst_div_id, subans:$scope.answers[$scope.index].subans};
	
}else{
	var answerArray = $scope.answers[$scope.index].ans.split(",");
	var correctArray = $scope.answers[$scope.index].correct_answers.split(",")
 for(i in answerArray){
	 
	 jsonArray.push({questId:$scope.answers[$scope.index].id,employee_id:userId,asst_type:$scope.answers[$scope.index].type, option_id:i, option_selected:answerArray[i], correct_option:correctArray[i], attempts:$scope.attempts, asst_div_id:$scope.answers[$scope.index].asst_div_id, subans:$scope.answers[$scope.index].subans});
	
 }
jsonObj =  {"questId":$scope.answers[$scope.index].id,"employee_id":userId,"asst_type":$scope.answers[$scope.index].type,"option_selected":0,"correct_option":0,"attempts":$scope.attempts,"asst_div_id":$scope.answers[$scope.index].asst_div_id,"subans":"", list:jsonArray};


	
	
}





//console.log(JSON.stringify(jsonObj));
	
	//return false;
	//localStorage.setItem("assessment", JSON.stringify($scope.answers));	
	var params = {ques:JSON.stringify(jsonObj)};
	//console.log(params);
	
	$("#next").text("Submitting..")
	$("#next").prop("disabled", true);
	$http.get(serverUrl + "Insert_interactive_assessment_answers", {params:params}).success(function(data, status, header, config){
		console.log(data);
		var obj = JSON.parse($(data).text())
		console.log(obj);
			if (obj.Status=="success"){
					
					console.log($scope.index + ":::::::::::::::" + $scope.total);
					if ($scope.index>=$scope.total-1){
						//alert("Quiz over");
						
						$scope.insertMarks();
						//document.write(JSON.stringify($scope.answers));
								
					}else{
						localStorage.setItem("assessment", JSON.stringify($scope.answers));	
						$scope.showQues();
					}
					
					
			}
			
			$("#next").text("Next")
			$("#next").prop("disabled", false);
			
		
	})
	
	
}

$scope.tryAgain = function($event){
	localStorage.setItem("assessment", "");	
	$scope.index=-1;
	console.log($scope.index);
	
	$scope.completed = false;
	$scope.getAssessment();
	
}


$(document).on("click", ".image_selection li a", function(){
		$(".image_selection li").removeClass("active")	
		$(this).parent("li").addClass("active");	
})





if (localStorage.getItem("assessment")!="" && localStorage.getItem("assessment")!=null){
	$scope.index = JSON.parse(localStorage.getItem("assessment")).length-1;	
	$scope.answers = JSON.parse(localStorage.getItem("assessment"));
}

//$scope.showQues();


})



function changeSecondToMinute(sec){

            var hrs = Math.floor(sec / 3600);
            var min = Math.floor((sec - (hrs * 3600)) / 60);
            var seconds = sec - (hrs * 3600) - (min * 60);
            seconds = Math.round(seconds * 100) / 100
           
            var result = (hrs < 10 ? "0" + hrs : hrs);
            result += ":" + (min < 10 ? "0" + min : min);
            result += ":" + (seconds < 10 ? "0" + seconds : seconds);
            return result;
         
	
}




function rankSort(){
	$(".slides").sortable({
     placeholder: 'slide-placeholder',
    axis: "y",
    revert: 150,
    start: function(e, ui){
        
        placeholderHeight = ui.item.outerHeight();
        ui.placeholder.height(placeholderHeight + 15);
        $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
    
    },
    change: function(event, ui) {
        
        ui.placeholder.stop().height(0).animate({
            height: ui.item.outerHeight() + 15
        }, 300);
        
        placeholderAnimatorHeight = parseInt($(".slide-placeholder-animator").attr("data-height"));
        
        $(".slide-placeholder-animator").stop().height(placeholderAnimatorHeight + 15).animate({
            height: 0
        }, 300, function() {
            $(this).remove();
            placeholderHeight = ui.item.outerHeight();
            $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
        });
        
    },
    stop: function(e, ui) {
        
        $(".slide-placeholder-animator").remove();
        
    },
});
	
	
}

