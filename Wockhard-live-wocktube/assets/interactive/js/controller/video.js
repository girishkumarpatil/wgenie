app.controller("video", function ($scope, $http){

$scope.questions = [{id:4, type:"1", questions:"Match the column", options:["Q1", "Q2", "Q3", "Q4"], answers:["1", "2", "3", "4"], timing:10}, {id:1, type:"0", questions:"Prime minister of India", ans:"1", options:["Narendra Modi", "Manmohan Singh", "Amit Shah"], timing:60}, {id:2, type:"3", questions:"Tell me about india?", timing:95},  {id:3, type:"2", questions:"Please solve the puzzle", imageUrl:"img/wockhardt621.jpg", timing:150}];


$scope.index = 0;
$scope.marks = 0;
$scope.total = 	0;
$scope.valid = false;
$scope.answers = [];
$scope.timing = 0;;
$scope.question = [];
var player, time_update_interval;
//$(".lightbox").hide();
function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        width: 600,
        height: 400,
        videoId: 'Xa0Q0J5tOP0',
		playerVars: { 'autoplay': 1, 'controls': 0, 'rel':0 },
        events: {
            onReady: $scope.initialize
        }
    });
}

window.setTimeout(onYouTubeIframeAPIReady, 1000);


$scope.initialize = function(){

    // Update the controls on load
 
    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    // the elapsed part of the progress bar every second.
    time_update_interval = setInterval(function () {
		$scope.videoTimer = player.getCurrentTime();

		if ($scope.videoTimer>=$scope.questions[$scope.index].timing){
			player.pauseVideo();
			
			clearInterval(time_update_interval);
			showPop("#question_box");
			
			
			$scope.showQues();
			console.log($scope.question.type);
			

			
			//$("#question_box").show();
			
		}
 
    }, 1000)

}


$scope.getAssessment = function(){

var params = {div_code:"E", levelcode:"L1", asst_id:17}


			/*$http.get(serverUrl + "UP_Get_Interactive_Assessment_question_by_user", {params:params}).success(function(data, status, header, config){
								var obj=JSON.parse($(data).text());
								console.log(obj);
								if (obj.data.Table.length>0){
									for (i in obj.data.Table){
										var ques = obj.data.Table[i];
										$scope.questions.push({
											id :ques.IQ_id,
											type:ques.IQ_type,
											questions:ques.IQ_question,
											imageUrl:imageUrl+ques.IQ_image,
											options:[ques.IQ_ans1, ques.IQ_ans2, ques.IQ_ans3, ques.IQ_ans4],
											answers:[ques.IM_ans1, ques.IM_ans2, ques.IM_ans3, ques.IM_ans4],
											ans:parseInt(ques.IQ_corrans)
										});
									};
									//console.log($scope.questions);
									$scope.total = 	$scope.questions.length;
	if ($scope.answers.length==$scope.total){
		document.write(JSON.stringify($scope.answers));
	}else{		
		$scope.showQues();		
	}
								}
						
	})	*/
	
	
}

$scope.getAssessment();



$scope.showQues = function($event){
	
	
	$scope.question = $scope.questions[$scope.index];
	console.log($scope.question);
	if ($scope.question.type=="2"){
			
			window.setTimeout(function(){
				$('#pile').height($('#source_image').height());
            $('#source_image').snapPuzzle({
                rows: 3, columns: 3,
                pile: '#pile',
                containment: '#puzzle-containment',
                onComplete: function(){
					$scope.valid= true;
                    $('#source_image').fadeOut(150).fadeIn();
                    $('#puzzle_solved').show();
                }
            });}, 1000);	


            $(window).resize(function(){
                $('#pile').height($('#source_image').height());
                $('#source_image').snapPuzzle('refresh');
            });
		
	}
	
	
}
$scope.checkAns = function($event){
			$scope.userValue = "";

			
			
			if ($scope.question.type=="0" && $("input[type=radio]:checked").val()==""){
				alert("Select atleast one");				
			}else if ($scope.question.type=="3" && $("textarea").val()==""){
				alert("Enter value")
			}else if ($scope.question.type=="2" && $scope.valid== false){
				alert("Please solve the puzzle")
			}else if ($scope.question.type=="1" && $scope.valid== false){
				alert("Please match the column");
			}else{
				
				
				
				if ($scope.index<=$scope.total){
					if ($scope.question.type=="0"){
						$scope.userValue = $("input[type=radio]:checked").val();
					}
					if ($scope.question.type=="3"){
						$scope.userValue = $("textarea").val();
					}
					if ($scope.question.type=="1"){
						var sel = [];
						$(".ans li.completed").each(function(index, element){
							sel.push($(element).attr("data-match-id"))
						})
						$scope.userValue = sel.toString();
					}
					
					$scope.answers.push({id:$scope.question.id, ans:$scope.userValue});
					console.log($scope.answers);
					 
					localStorage.setItem("assessment", JSON.stringify($scope.answers));
					console.log($scope.index + ":" + $scope.total);
					
					if ($scope.index>=$scope.total-1){
						alert("Quiz over");
						document.write(JSON.stringify($scope.answers));
					}else{
						player.playVideo();
						$scope.index++;
						$scope.showQues();
						
					}
					
				}
				
			}
	
}



var matchSelected;
$(document).on("click", ".options li", function(){
	
	var getMatch = $(this).attr("data-index");
	if ($(this).hasClass("completed")==true){
		$(this).removeClass("completed");
		$(".ans li[data-match-id='"+getMatch+"']").removeClass("completed");
	}
	if ($(".options li.selected").length<1){
		$(this).addClass("selected");
	}else{
		$(".options li").removeClass("selected");
	}
	
	
	
})

$(document).on("click", ".ans li", function(){
	
	
	if ($(".options li.selected").length>0){
			if ($(this).hasClass("completed")==false){
				var getMatch = $(".options li.selected").attr("data-match-id");
				$(this).attr("data-match-id", getMatch);
				$(this).addClass("completed")
				$(".options li.selected").addClass("completed");
				$(".options li").removeClass("selected");
			}
			
	}
	
		if ($(".ans li.completed").length==$(".ans li").length){			
				$scope.valid= true;
			}
		console.log($scope.valid);
	
	
})


if (localStorage.getItem("assessment")!="" && localStorage.getItem("assessment")!=null){
	$scope.index = JSON.parse(localStorage.getItem("assessment")).length-1;	
	$scope.answers = JSON.parse(localStorage.getItem("assessment"));
}

	
$scope.showQues();

})





